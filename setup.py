from setuptools import setup, find_packages

with open('VERSION.txt', 'r') as version_file:
    version = version_file.read().strip()

requires = ['jinja2', 'python-Levenshtein', 'nose']

setup(
    name='dips',
    version=version,
    author='Ophir Tal, Rotem Barzilay',
    author_email='ophir.tal@weizmann.ac.il, rotem.barzilay@weizmann.ac.il',
    packages=find_packages(),
    scripts=[
        'scripts/clean-peptides.py',
        'scripts/assemble-peptides.py',
        'scripts/alignments-to-csv.py',
        'scripts/output-alignments.py',
        'scripts/output-kmer-alignments.py',
        'scripts/sequences-to-csv.py',
        'scripts/kmer-assemble.py',
        'scripts/kmer-reporter.py',
        'scripts/dips-downsample.py',
        'scripts/kmer-assemble-gui.py',
        'scripts/align-peptides-to-sequence.py'
    ],
    url='https://bitbucket.org/incpm/ngps',
    description='pTA - Peptide Tag Assembler',
    long_description=open('README.md').read(),
    tests_require=requires,
    install_requires=requires,
    test_suite='nose.collector',
    include_package_data=True
)
