#!/usr/bin/env python

import argparse
import logging.config
import os
import json
from dips.align import AlignmentSwapScorer, NaiveAligner
from dips.assemble import PROTEINS_DICT
from dips.jsonutils import json_load_object_hook

"""
logging
"""
from dips.settings.logging_settings import LOGGING, LOGGER_NAME
logging.config.dictConfig(LOGGING)
logger = logging.getLogger(LOGGER_NAME)


def output_alignments(alignments):
    assert alignments
    # TODO: handle case of multiple proteins (currently only single protein)
    assert all([alignments[0].protein == alignment.protein for alignment in alignments])
    alignments = sorted(alignments, key=lambda x: x.position)
    output = alignments[0].protein.sequence + '\n'
    line_length = len(output)
    for alignment in alignments:
        current_line = '.' * alignment.position + alignment.sequence.sequence
        output += current_line + '.' * (line_length - len(current_line) - 1) + '\n'
    return output


def main():
    parser = argparse.ArgumentParser(
        description='Display all aligned contigs (and peptides, if specified) '
                    'under the protein to which they are aligned')
    parser.add_argument('--input', required=True, type=str, help='peptides json file from kmer-assemble')
    parser.add_argument('--protein-name', help='the protein you are trying to assemble from this set: [MYOGLOBIN, FETUIN, BSA], default=None',default=None)
    parser.add_argument('--output', required=True, type=argparse.FileType('w'), dest='output_file_handler',
                        help='Output file')
    args = parser.parse_args()
    try:
        input_json_file = args.input
        protein_name = args.protein_name
        protein = PROTEINS_DICT[protein_name].clean()
        cleaned_peptides = json.load(fp=open(input_json_file, 'r'), object_hook=json_load_object_hook)
        #cleaned_peptides = [peptide for peptide in cleaned_peptides if peptide.sequence=='VSPLPG']
        logger.info("finished loading %s cleaned peptides from the json file." %len(cleaned_peptides))
        swap_scorer = AlignmentSwapScorer(accuracy_threshold=0.8, min_confidence=0)
        swap_aligner = NaiveAligner(proteins=[], scorer=swap_scorer, stop_on_first_alignment=False)
        alignments = []
        for peptide in cleaned_peptides:
            alignment = swap_aligner.align_to_protein(protein, peptide)
            if alignment:
                alignments.append(alignment)
        logger.info("%s cleaned peptides were aligned." %len(alignments))
        s = output_alignments(alignments)
        args.output_file_handler.write(s)
    finally:
        args.output_file_handler.close()

if __name__ == '__main__':
    main()