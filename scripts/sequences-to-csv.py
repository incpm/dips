#!/usr/bin/env python

import argparse
import csv
import json

from dips.data import Peptide, Contig
from dips.jsonutils import json_load_object_hook

def convert_to_csv(input_file_handler, output_file_handler, reverse):
    sequences = json.load(fp=input_file_handler, object_hook=json_load_object_hook)
    assert all([type(sequence) in (Peptide, Contig) for sequence in sequences])
    csv_writer = csv.writer(output_file_handler, delimiter=',')
    csv_writer.writerow(['Scan', 'Peptide', 'local confidence (%)'])
    for sequence in sequences:
        seq = sequence.sequence
        confidence = sequence.confidence
        if reverse:
            seq = seq[::-1]
            confidence = confidence[::-1]
        csv_writer.writerow([sequence.single_identifier, seq, ' '.join([str(v) for v in confidence])])
    output_file_handler.close()

def load_assembly_results():
    parser = argparse.ArgumentParser(description='Read sequences in JSON format and convert to CSV format')
    parser.add_argument('--input', required=True, type=argparse.FileType('r'), help='JSON file containing the sequences', dest='input_file_handler')
    parser.add_argument('--output', required=True, type=argparse.FileType('w'), help='Output file', dest='output_file_handler')
    parser.add_argument('--reverse', action='store_true', default=False, help='whether to reverse the sequences')
    convert_to_csv(**vars(parser.parse_args()))

if __name__ == '__main__':
    load_assembly_results()
