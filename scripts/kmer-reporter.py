#!/usr/bin/env python

import argparse

from dips.assemble import KmerReporter


def main(args):
    # get arguments and execute:
    report_title = args.report_title
    run_folder = args.run_folder
    input_sequences = args.sequences
    # helper args:
    reporter = KmerReporter(report_title, run_folder, input_sequences)
    reporter.create_report()


def get_options():
    parser = argparse.ArgumentParser(description='Creates a report for specific sequences, Note: needs the run folder of the assembly process that was done.')
    parser.add_argument('--report_title', type=str, required=True, help='Title for the report and the report name. Required')
    parser.add_argument('--run-folder', required=True, help='The run folder that the assembly process was done in. Required')
    parser.add_argument('--sequences', type=str, nargs='+', required=True, help='The sequences for the report, if there is more than one, separate them by space . Required')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = get_options()
    main(args)
