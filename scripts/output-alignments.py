#!/usr/bin/env python

import argparse
import logging.config
import os

from dips.runner import AssemblyResultLoader

logging.config.fileConfig(os.path.join('config', 'dev', 'logging.conf'))

logger = logging.getLogger('dips.' + __name__)


def load_assembly_results(output_directory):
    r = AssemblyResultLoader(output_dir=output_directory)
    r.print_summary()
    return r


def output_alignments(alignments):
    assert alignments
    # TODO: handle case of multiple proteins (currently only single protein)
    assert all([alignments[0].protein == alignment.protein for alignment in alignments])
    alignments = sorted(alignments, key=lambda x: x.position)
    output = alignments[0].protein.sequence + '\n'
    line_length = len(output)
    for alignment in alignments:
        current_line = '.' * alignment.position + alignment.sequence.sequence
        output += current_line + '.' * (line_length - len(current_line) - 1) + '\n'
    return output


def main():
    parser = argparse.ArgumentParser(
        description='Display all aligned contigs (and peptides, if specified) '
                    'under the protein to which they are aligned')
    parser.add_argument('--input', required=True, type=str, dest='input_directory',
                        help='Input directory - should contain assembly output')
    parser.add_argument('--output', required=True, type=argparse.FileType('w'), dest='output_file_handler',
                        help='Output file')
    parser.add_argument('--peptides', action='store_true', default=False, help='output aligned peptides')
    args = parser.parse_args()
    try:
        r = load_assembly_results(args.input_directory)
        logger.info('number of aligned contigs: %s' % len(r.contig_alignments))
        alignments = r.contig_alignments
        if args.peptides:
            logger.info('number of aligned peptides: %s' % len(r.peptide_alignments))
            alignments += r.peptide_alignments
        s = output_alignments(alignments)
        args.output_file_handler.write(s)
    finally:
        args.output_file_handler.close()

if __name__ == '__main__':
    main()
