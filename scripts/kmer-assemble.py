#!/usr/bin/env python

import argparse
from dips.assemble import KmerAssembler, ASSEMBLE_STEPS


def main(args):
    # get arguments and execute:
    starting_step = args.starting_step
    acid_input_files_paths = args.acid_input_file
    trypsin_input_files_paths = args.trypsin_input_file
    output_dir = args.output_dir
    kmer_size = int(args.kmer_size)
    kmer_min_overlap = int(args.kmer_min_overlap)
    unite_min_overlap = int(args.unite_min_overlap)
    unite_min_extension = int(args.unite_min_extension)
    protein_name = args.protein_name
    report_title = args.report_title
    # helper args:
    contigs_ids_file = args.contigs_ids_file
    merge_min_quality = float(args.merge_min_quality)
    assert starting_step in ASSEMBLE_STEPS, "there's no starting step: "+starting_step+" in "+", ".join(ASSEMBLE_STEPS)
    assert acid_input_files_paths is not None or trypsin_input_files_paths is not None, "You need to enter at least one input file, acid or trypsin or both"
    KmerAssembler(starting_step, acid_input_files_paths, trypsin_input_files_paths, output_dir, kmer_size, kmer_min_overlap, unite_min_overlap,
                  unite_min_extension, merge_min_quality, report_title=report_title, protein_name=protein_name, contigs_ids_file=contigs_ids_file).assemble()


def get_options():
    parser = argparse.ArgumentParser(description='assemble peptides with kmer assembler')
    parser.add_argument('--acid-input-file', type=str, nargs='+',
                        help='Peptides input file/s (acid). Required')
    parser.add_argument('--trypsin-input-file', type=str, nargs='+',
                        help='Peptides input file/s (Trypsin). Optional')
    parser.add_argument('--output-dir', required=True, help='Output directory. Required')
    parser.add_argument('--starting-step', help='Starting step: ' + ", ".join(ASSEMBLE_STEPS) + ', default=all',
                        default='all')
    parser.add_argument('--kmer-size', help='K-mer size, default=7', default=7)
    parser.add_argument('--kmer-min-overlap', help='Minimal overlap for assembling k-mers (not uniting), default=5',
                        default=5)
    parser.add_argument('--unite-min-overlap', help='Minimal overlap for uniting contigs, default=5', default=5)
    parser.add_argument('--unite-min-extension', help='Minimal extension for uniting two contigs, default=7',
                        default=7)
    parser.add_argument('--protein-name', help='''DEBUG PARAMETER (optional):
    The protein you are trying to assemble from this set: [MYOGLOBIN, FETUIN, BSA], default=None''', default=None)
    parser.add_argument('--contigs-ids-file', help='''DEBUG PARAMETER (optional):
    A file containing the contig ID's per line. The filtration occurs before the merge contigs step''', default=None)
    parser.add_argument('--merge-min-quality',
                        help='Minimal quality for similarity between two k-mer contigs, value between 0 to 1, default=0.7',
                        default=0.7)
    parser.add_argument('--report-title', help='Report title name, default=\'Final Contigs Report\'',
                        default='Final Contigs Report')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = get_options()
    main(args)
