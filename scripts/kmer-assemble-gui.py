#!/usr/bin/env python
import multiprocessing
import traceback
import os
import sys

import re
import wx
from threading import Thread
from dips.assemble import STEP_ALL, ASSEMBLE_STEPS, KmerAssembler, KmerReporter
import logging
import abc
"""logging:"""
from dips.settings.logging_settings import LOGGING, LOGGER_NAME

logging.config.dictConfig(LOGGING)
logger = logging.getLogger(LOGGER_NAME)

#
# """
# start of fix for multiprocessing with PyInstaller(windows)
# """
# # Module multiprocessing is organized differently in Python 3.4+
# try:
#     # Python 3.4+
#     if sys.platform.startswith('win'):
#         import multiprocessing.popen_spawn_win32 as forking
#     else:
#         import multiprocessing.popen_fork as forking
# except ImportError:
#     import multiprocessing.forking as forking
#
# if sys.platform.startswith('win'):
#     # First define a modified version of Popen.
#     class _Popen(forking.Popen):
#         def __init__(self, *args, **kw):
#             if hasattr(sys, 'frozen'):
#                 # We have to set original _MEIPASS2 value from sys._MEIPASS
#                 # to get --onefile mode working.
#                 os.putenv('_MEIPASS2', sys._MEIPASS)
#             try:
#                 super(_Popen, self).__init__(*args, **kw)
#             finally:
#                 if hasattr(sys, 'frozen'):
#                     # On some platforms (e.g. AIX) 'os.unsetenv()' is not
#                     # available. In those cases we cannot delete the variable
#                     # but only set it to the empty string. The bootloader
#                     # can handle this case.
#                     if hasattr(os, 'unsetenv'):
#                         os.unsetenv('_MEIPASS2')
#                     else:
#                         os.putenv('_MEIPASS2', '')
#
#     # Second override 'Popen' class with our modified version.
#     forking.Popen = _Popen
# """
# end of fix for multiprocessing with PyInstaller(windows)
# """

END_PROCESS = "END!"


class QueueLoggerHandler(logging.Handler):
    def __init__(self, queue):
        logging.Handler.__init__(self)
        self.queue = queue

    def emit(self, record):
        s = self.format(record) + '\n'
        self.queue.put(s)


def run_assemble_process(queue, *args):
    try:
        # set logger:
        handler = QueueLoggerHandler(queue)
        FORMAT = "%(asctime)s %(levelname)s %(message)s"
        handler.setFormatter(logging.Formatter(FORMAT))
        handler.setLevel(logging.INFO)
        logger.addHandler(handler)
        # start assembling:
        kmer_assembler = KmerAssembler(*args)
        kmer_assembler.assemble()
    except Exception, e:
        tb = traceback.format_exc()
        queue.put(tb)
    finally:
        queue.put(END_PROCESS)


def run_reporter_process(queue, *args):
    try:
        # set logger:
        handler = QueueLoggerHandler(queue)
        FORMAT = "%(asctime)s %(levelname)s %(message)s"
        handler.setFormatter(logging.Formatter(FORMAT))
        handler.setLevel(logging.INFO)
        logger.addHandler(handler)
        # start assembling:
        reporter = KmerReporter(*args)
        reporter.create_report()
    except Exception, e:
        tb = traceback.format_exc()
        queue.put(tb)
    finally:
        queue.put(END_PROCESS)


FILES_DELIMITER = ';'


class ProcessGUIPanel(wx.Panel):

    def __init__(self, parent, process_func, run_btn_label='Run', logger_size=(500, 100)):
        super(ProcessGUIPanel, self).__init__(parent)
        self.SetBackgroundColour(wx.WHITE)

        # set processing params:
        self.process_func = process_func
        self.running = False
        self.run_thread = None
        self.p = None

        # run button and logger text area:
        self.run_btn_label = run_btn_label
        self.run_button = wx.Button(self, label=self.run_btn_label)
        self.Bind(wx.EVT_BUTTON, self.run_on_click, self.run_button)
        self.logger_size = logger_size
        self.logger_txt_area = wx.TextCtrl(self, -1, style=wx.TE_MULTILINE | wx.BORDER_SUNKEN | wx.TE_READONLY |
                                                            wx.TE_RICH2, size=self.logger_size)

        # init UI
        self.init_ui()

    @abc.abstractmethod
    def init_ui(self):
        pass

    @abc.abstractmethod
    def get_process_args(self):
        pass

    @staticmethod
    def _is_int(s):
        try:
            int(s)
            return True
        except ValueError:
            return False

    @staticmethod
    def _is_float(s):
        try:
            float(s)
            return True
        except ValueError:
            return False

    def _run_process(self, *args):
        manager = multiprocessing.Manager()
        self.queue = manager.Queue()
        self.p = multiprocessing.Process(target=self.process_func, args=tuple([self.queue]+list(args)))
        self.p.daemon = True
        self.p.start()
        while(True):
            # get logger message:
            line = self.queue.get()
            if(line==END_PROCESS):
                break
            else:
                self.logger_txt_area.AppendText(line)
        self.p.join()
        self.running = False
        self.run_thread = None
        self.p = None
        self.run_button.SetLabel(self.run_btn_label)

    def on_close(self, event):
        if(self.p!=None):
            self.p.terminate()
            self.queue.put(END_PROCESS)
            self.p.join()
        self.Destroy()

    def run_on_click(self, event):
        if(self.running==False):
            process_args = self.get_process_args()
            if process_args is None:
                return
            self.running = True
            self.run_button.SetLabel("Stop")
            self.run_thread = Thread(target=self._run_process, args=process_args)
            self.run_thread.daemon = True
            self.run_thread.start()
        else:
            self.queue.put(END_PROCESS)
            self.p.terminate()


class MainGUI(wx.Frame):
    def __init__(self, parent, title):
        super(MainGUI, self).__init__(parent, title=title,
            size=(545, 665))
        # Create a panel and notebook (tabs holder)
        p = wx.Panel(self)
        nb = wx.Notebook(p)
        # Create the tab windows
        self.assembler_panel = KmerAssemblePanel(nb)
        self.reporter_panel = KmerReporterPanel(nb)

        # Add the windows to tabs and name them.
        nb.AddPage(self.assembler_panel, "Assembler")
        nb.AddPage(self.reporter_panel, "Reporter")
        # nb.AddPage(tab2, "Tab 2")

        # Set noteboook in a sizer to create the layout
        sizer = wx.BoxSizer()
        sizer.Add(nb, 1, wx.EXPAND)
        p.SetSizer(sizer)

        self.Centre()
        self.Show()
        # TODO: handle close
        self.Bind(wx.EVT_CLOSE, self.on_close)

    def on_close(self, event):
        self.assembler_panel.on_close(event)
        self.reporter_panel.on_close(event)
        self.Destroy()


class KmerAssemblePanel(ProcessGUIPanel):
    def __init__(self, parent):
        super(KmerAssemblePanel, self).__init__(parent, run_assemble_process, 'Assemble')

    def init_ui(self):
        # title:
        title_sizer = wx.BoxSizer(wx.HORIZONTAL)
        font = wx.Font(12, wx.SWISS, wx.NORMAL, wx.BOLD)
        title = wx.StaticText(self, wx.ID_ANY, 'Peptide Tag Assembler')
        title.SetFont(font)
        title_sizer.Add(title, 0, wx.ALL, 10)


        # new buttons:
        report_title_sizer = wx.BoxSizer(wx.HORIZONTAL)
        file_select_acid_sizer = wx.BoxSizer(wx.HORIZONTAL)
        file_select_trypsin_sizer = wx.BoxSizer(wx.HORIZONTAL)

        # report title:
        report_title_label = wx.StaticText(self, wx.ID_ANY, 'Report title:')
        self.report_title = wx.TextCtrl(self, wx.ID_ANY, '', size=(-1, 23))
        report_title_sizer.Add(report_title_label, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        report_title_sizer.Add(self.report_title, 1, wx.BOTTOM|wx.TOP|wx.LEFT, 5)

        # acid file/s input:
        acid_input_file_label = wx.StaticText(self, wx.ID_ANY, 'Input peptide tags file after hydrolysis from PEAKs (.csv)')
        self.acid_input_file_button = wx.Button(self, label='Add file/s', size=(85, 30))
        self.acid_input_file_button.Bind(wx.EVT_BUTTON, lambda event: self.on_open(event, self.acid_input_file, self.acid_num_selected))
        self.acid_input_file = wx.TextCtrl(self, wx.ID_ANY, '', size=(-1, 23))
        self.acid_input_file_clear_button = wx.Button(self, label='Clear', size=(85, 30))
        self.acid_num_selected = wx.StaticText(self, wx.ID_ANY,'0 files selected')
        self.acid_input_file_clear_button.Bind(wx.EVT_BUTTON, lambda event: self.clear_text(event, self.acid_input_file, self.acid_num_selected))
        file_select_acid_sizer.Add(self.acid_input_file, 1, wx.RIGHT | wx.TOP, 5)
        file_select_acid_sizer.Add(self.acid_input_file_button, 0, wx.ALIGN_BOTTOM, 0)
        file_select_acid_sizer.Add(self.acid_input_file_clear_button, 0, wx.ALIGN_BOTTOM, 0)

        #trypsin file/s input:
        trypsin_input_file_label = wx.StaticText(self, wx.ID_ANY, '(Optional) Input peptide tags file after proteolytic digestion from PEAKs (.csv)')
        self.trypsin_input_file_button = wx.Button(self, label='Add file/s', size=(85, 30))
        self.trypsin_input_file_button.Bind(wx.EVT_BUTTON, lambda event: self.on_open(event, self.trypsin_input_file, self.trypsin_num_selected))
        self.trypsin_input_file = wx.TextCtrl(self, wx.ID_ANY|wx.ALL|wx.EXPAND, '', size=(-1, 23))
        self.trypsin_input_file_clear_button = wx.Button(self, label='Clear', size=(85, 30))
        self.trypsin_num_selected = wx.StaticText(self, wx.ID_ANY, '0 files selected')
        self.trypsin_input_file_clear_button.Bind(wx.EVT_BUTTON, lambda event: self.clear_text(event, self.trypsin_input_file, self.trypsin_num_selected))
        file_select_trypsin_sizer.Add(self.trypsin_input_file, 1, wx.RIGHT | wx.TOP, 5)
        file_select_trypsin_sizer.Add(self.trypsin_input_file_button, 0, wx.LEFT, 0)
        file_select_trypsin_sizer.Add(self.trypsin_input_file_clear_button, 0, wx.LEFT, 0)

        # output dir:
        output_dir_label = wx.StaticText(self, wx.ID_ANY, 'Output Directory:')
        self.output_dir = wx.DirPickerCtrl(self, wx.ID_ANY, message='Please select an output directory',  size=(-1, 30), path="")
        self.output_dir.SetBackgroundColour(wx.WHITE)

        files_sizer = wx.BoxSizer(wx.VERTICAL)
        files_sizer.Add(report_title_sizer, 0, wx.ALL|wx.EXPAND, 5)
        files_sizer.Add(acid_input_file_label, 0, wx.ALL|wx.EXPAND| wx.ALIGN_BOTTOM, 5)
        files_sizer.Add(file_select_acid_sizer, 0, wx.ALL|wx.EXPAND, 5)
        files_sizer.Add(self.acid_num_selected, 0, wx.ALIGN_TOP | wx.LEFT, 5)
        files_sizer.AddSpacer(10)
        files_sizer.Add(trypsin_input_file_label, 0, wx.ALL|wx.EXPAND| wx.ALIGN_BOTTOM, 5)
        files_sizer.Add(file_select_trypsin_sizer, 0, wx.ALL|wx.EXPAND, 5)
        files_sizer.Add(self.trypsin_num_selected, 0, wx.ALIGN_TOP | wx.LEFT, 5)
        files_sizer.AddSpacer(10)
        files_sizer.Add(output_dir_label, 0, wx.ALL|wx.EXPAND| wx.ALIGN_BOTTOM, 5)
        files_sizer.Add(self.output_dir, 0, wx.ALL|wx.EXPAND, 5)

        # parameters:
        parameters_sizer = wx.GridBagSizer(hgap=5, vgap=5)
        kmer_size_label = wx.StaticText(self, wx.ID_ANY, 'kmer size:')
        self.kmer_size_text_box = wx.TextCtrl(self, wx.ID_ANY,'7')
        kmer_min_overlap_label = wx.StaticText(self, wx.ID_ANY, 'kmer min overlap:')
        self.kmer_min_overlap_text_box = wx.TextCtrl(self, wx.ID_ANY,'5')
        unite_min_overlap_label = wx.StaticText(self, wx.ID_ANY, 'unite overlap size:')
        self.unite_min_overlap_text_box = wx.TextCtrl(self, wx.ID_ANY,'5')
        unite_min_extension_label = wx.StaticText(self, wx.ID_ANY, 'unite min extension:')
        self.unite_min_extension_text_box = wx.TextCtrl(self, wx.ID_ANY,'7')
        merge_min_quality_label = wx.StaticText(self, wx.ID_ANY, 'merge min quality:')
        self.merge_min_quality_text_box = wx.TextCtrl(self, wx.ID_ANY,'0.7')
        # starting_step_label = wx.StaticText(self, wx.ID_ANY, 'starting step:')
        # self.starting_step_combo_box = wx.ComboBox(self, wx.ID_ANY, STEP_ALL, wx.DefaultPosition,
        #                     (100,-1), ASSEMBLE_STEPS, wx.CB_DROPDOWN)

        parameters_sizer.Add(kmer_size_label, pos=(0,0), flag=wx.ALL|wx.ALIGN_CENTER_VERTICAL, border=5)
        parameters_sizer.Add(self.kmer_size_text_box, pos=(0,1), flag=wx.ALL|wx.ALIGN_CENTER_VERTICAL, border=5)
        parameters_sizer.Add(kmer_min_overlap_label, pos=(1,0), flag=wx.ALL|wx.ALIGN_CENTER_VERTICAL, border=5)
        parameters_sizer.Add(self.kmer_min_overlap_text_box, pos=(1,1), flag=wx.ALL|wx.ALIGN_CENTER_VERTICAL, border=5)
        parameters_sizer.Add(unite_min_overlap_label, pos=(2,0), flag=wx.ALL|wx.ALIGN_CENTER_VERTICAL, border=5)
        parameters_sizer.Add(self.unite_min_overlap_text_box, pos=(2,1), flag=wx.ALL|wx.ALIGN_CENTER_VERTICAL, border=5)
        parameters_sizer.Add(unite_min_extension_label, pos=(0,2), flag=wx.ALL|wx.ALIGN_CENTER_VERTICAL, border=5)
        parameters_sizer.Add(self.unite_min_extension_text_box, pos=(0,3), flag=wx.ALL|wx.ALIGN_CENTER_VERTICAL, border=5)
        parameters_sizer.Add(merge_min_quality_label, pos=(1,2), flag=wx.ALL|wx.ALIGN_CENTER_VERTICAL, border=5)
        parameters_sizer.Add(self.merge_min_quality_text_box, pos=(1,3), flag=wx.ALL|wx.ALIGN_CENTER_VERTICAL, border=5)
        # parameters_sizer.Add(starting_step_label, pos=(2,2), flag=wx.ALL|wx.ALIGN_CENTER_VERTICAL, border=5)
        # parameters_sizer.Add(self.starting_step_combo_box, pos=(2,3), flag=wx.ALL|wx.ALIGN_CENTER_VERTICAL, border=5)

        top_sizer = wx.BoxSizer(wx.VERTICAL)
        top_sizer.Add(title_sizer, 0, wx.CENTER, 5)
        top_sizer.Add(files_sizer,0, wx.EXPAND, 5)
        top_sizer.Add(parameters_sizer,0, wx.CENTER, 5)
        top_sizer.Add(self.logger_txt_area, 1, wx.CENTER|wx.TOP|wx.RIGHT|wx.LEFT|wx.EXPAND, 10)
        top_sizer.Add(self.run_button, 0, wx.CENTER|wx.BOTTOM|wx.TOP, 15)

        self.SetSizerAndFit(top_sizer)


    def _check_input(self, report_title, acid_input_file, trypsin_input_file, output_dir, kmer_size, kmer_min_overlap, unite_min_overlap,
                         unite_min_extension, merge_min_quality):
        # check empty values:
        if(report_title=='' or (acid_input_file=='' and trypsin_input_file=='') or output_dir=='' or kmer_size=='' or kmer_min_overlap=='' or unite_min_overlap==''
           or unite_min_extension=='' or merge_min_quality==''):
            msgBox = wx.MessageDialog(self, "Some of the fields are empty, please fill them all.", caption="Empty fields Error", style=wx.OK|wx.STAY_ON_TOP|wx.ICON_ERROR)
            msgBox.ShowModal()
            return False
        # check numbers:
        if(not(self._is_int(kmer_size) and self._is_int(kmer_min_overlap) and self._is_int(unite_min_overlap)
           and self._is_int(unite_min_extension))):
            msgBox = wx.MessageDialog(self, "Some of the integers are not integers.", caption="Integer fields Error", style=wx.OK|wx.STAY_ON_TOP|wx.ICON_ERROR)
            msgBox.ShowModal()
            return False
        # checking floats:
        if(not(self._is_float(merge_min_quality))):
            msgBox = wx.MessageDialog(self, "Some of the numbers are not numbers.", caption="Number fields Error", style=wx.OK|wx.STAY_ON_TOP|wx.ICON_ERROR)
            msgBox.ShowModal()
            return False
        # # check starting step
        # if(starting_step not in ASSEMBLE_STEPS):
        #     msgBox = wx.MessageDialog(self, "There is no starting step: '"+starting_step+"'.", caption="Starting step Error", style=wx.OK|wx.STAY_ON_TOP|wx.ICON_ERROR)
        #     msgBox.ShowModal()
        #     return False
        return True

    def get_process_args(self):
        report_title = self.report_title.GetValue()
        acid_input_file = self.acid_input_file.GetValue()
        trypsin_input_file = self.trypsin_input_file.GetValue()
        output_dir = self.output_dir.GetPath()
        kmer_size = self.kmer_size_text_box.GetValue()
        kmer_min_overlap = self.kmer_min_overlap_text_box.GetValue()
        unite_min_overlap = self.unite_min_overlap_text_box.GetValue()
        unite_min_extension = self.unite_min_extension_text_box.GetValue()
        merge_min_quality = self.merge_min_quality_text_box.GetValue()
        # starting_step = self.starting_step_combo_box.GetValue()
        # checking input:
        if (self._check_input(report_title, acid_input_file, trypsin_input_file, output_dir, kmer_size,
                              kmer_min_overlap, unite_min_overlap,
                              unite_min_extension, merge_min_quality) == False):
            return None
        # now, let assemble:
        assembler_args = ('all', None if not acid_input_file else acid_input_file.split(FILES_DELIMITER),
                          None if not trypsin_input_file else trypsin_input_file.split(FILES_DELIMITER), output_dir,
                          int(kmer_size), int(kmer_min_overlap), int(unite_min_overlap),
                          int(unite_min_extension), float(merge_min_quality), report_title)
        return assembler_args

    def on_open(self, event, text_field, num_selected_label):
        open_file_dialog = wx.FileDialog(self, "Open csv file", "", "",
                                         "csv files (*.csv)|*.csv", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST | wx.FD_MULTIPLE)
        if open_file_dialog.ShowModal() == wx.ID_CANCEL:
            return
        else:
            old_value = text_field.GetValue()
            old_files = [] if old_value == '' else old_value.split(FILES_DELIMITER)
            files = old_files + open_file_dialog.GetPaths()
            # print files
            text_field.SetValue(FILES_DELIMITER.join(files))
            num_selected_label.SetLabelText('%s files selected' % len(files))

    def clear_text(self, event, text_field, num_selected_label):
        text_field.SetValue('')
        num_selected_label.SetLabelText('0 files selected')


class KmerReporterPanel(ProcessGUIPanel):
    def __init__(self, parent):
        super(KmerReporterPanel, self).__init__(parent, run_reporter_process, 'Create Report')

    def init_ui(self):
        # title:
        title_sizer = wx.BoxSizer(wx.HORIZONTAL)
        font = wx.Font(12, wx.SWISS, wx.NORMAL, wx.BOLD)
        title = wx.StaticText(self, wx.ID_ANY, 'Reporter')
        title.SetFont(font)
        title_sizer.Add(title, 0, wx.ALL, 10)

        # report title:
        report_title_sizer = wx.BoxSizer(wx.HORIZONTAL)
        report_title_label = wx.StaticText(self, wx.ID_ANY, 'Report title:')
        self.report_title = wx.TextCtrl(self, wx.ID_ANY, '', size=(-1, 23))
        report_title_sizer.Add(report_title_label, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        report_title_sizer.Add(self.report_title, 1, wx.BOTTOM|wx.TOP|wx.LEFT, 5)

        # input sequences:
        input_sequences_label = wx.StaticText(self, wx.ID_ANY, 'Input Sequences(fasta format):')
        self.input_sequences = wx.TextCtrl(self, -1, style=wx.TE_MULTILINE | wx.BORDER_SUNKEN |
                                                           wx.TE_RICH2, size=self.logger_size)

        # output dir:
        run_folder_label = wx.StaticText(self, wx.ID_ANY, 'Run Folder:')
        self.run_folder = wx.DirPickerCtrl(self, wx.ID_ANY, message='Please select an run folder', size=(-1, 30), path="")
        self.run_folder.SetBackgroundColour(wx.WHITE)

        top_sizer = wx.BoxSizer(wx.VERTICAL)
        top_sizer.Add(title_sizer, 0, wx.CENTER, 5)
        top_sizer.Add(report_title_sizer, 0, wx.ALL | wx.EXPAND, 5)
        top_sizer.Add(input_sequences_label, 0, wx.ALL | wx.EXPAND, 5)
        top_sizer.Add(self.input_sequences, 0, wx.ALL | wx.EXPAND, 5)
        top_sizer.Add(run_folder_label, 0, wx.ALL | wx.EXPAND, 5)
        top_sizer.Add(self.run_folder, 0, wx.ALL | wx.EXPAND, 5)
        top_sizer.AddSpacer(20)
        top_sizer.Add(self.logger_txt_area, 0, wx.ALL | wx.EXPAND, 5)
        top_sizer.Add(self.run_button, 0, wx.CENTER|wx.BOTTOM|wx.TOP, 15)

        self.SetSizerAndFit(top_sizer)

    def _check_input(self, report_title, run_folder, input_sequences):
        # check empty values:
        if report_title == '' or run_folder == '':
            msgBox = wx.MessageDialog(self, "Some of the fields are empty, please fill them all.", caption="Empty fields Error", style=wx.OK|wx.STAY_ON_TOP|wx.ICON_ERROR)
            msgBox.ShowModal()
            return False
        if len(input_sequences) == 0:
            msgBox = wx.MessageDialog(self, "There is no sequences.",
                                      caption="No Sequences", style=wx.OK | wx.STAY_ON_TOP | wx.ICON_ERROR)
            msgBox.ShowModal()
            return False
        return True

    def get_process_args(self):
        report_title = self.report_title.GetValue()
        input_sequences = self.get_fasta_seqs(self.input_sequences.GetValue().strip())
        run_folder = self.run_folder.GetPath()
        # checking input:
        if not self._check_input(report_title, run_folder, input_sequences):
            return None
        # now, let create a report:
        assembler_args = (report_title, run_folder, input_sequences)
        return assembler_args

    @staticmethod
    def get_fasta_seqs(input_seqs):
        seqs = []
        current_seq = ''
        for line in input_seqs.split('\n'):
            line = line.strip()
            print(line)
            if line.startswith('>'):
                if current_seq:
                    seqs.append(current_seq)
                    current_seq = ''
                continue
            current_seq += line
        if current_seq:
            seqs.append(current_seq)
        return seqs


def main():
    app = wx.App()
    # KmerAssembleGUI(None, 'Protein Assembler')
    MainGUI(None, 'Protein Assembler')
    app.MainLoop()

p_name = multiprocessing.current_process().name
# print p_name
# print __name__

if __name__ == '__main__':
    # On Windows calling this function(freeze_support) is necessary.
    # On Linux/OSX it does nothing.
    multiprocessing.freeze_support()
    main()
