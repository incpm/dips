#!/usr/bin/env python

import argparse
import json

from dips.data import Alignment
from dips.io import ProteinSequenceCsvWriter
from dips.jsonutils import json_load_object_hook


def convert_to_csv(input_filename, output_filename):
    with open(input_filename, 'r') as input_file_handler:
        alignments = json.load(fp=input_file_handler, object_hook=json_load_object_hook)
    assert all([type(alignment) == Alignment for alignment in alignments])
    ProteinSequenceCsvWriter([alignment.sequence for alignment in alignments]).write(output_filename=output_filename)


def load_assembly_results():
    parser = argparse.ArgumentParser(description='Read peptide alignments in JSON format and convert to CSV format')
    parser.add_argument('--input', required=True, type=str,
                        help='JSON file containing the alignments', dest='input_filename')
    parser.add_argument('--output', required=True, type=str, help='Output file', dest='output_filename')
    convert_to_csv(**vars(parser.parse_args()))

if __name__ == '__main__':
    load_assembly_results()
