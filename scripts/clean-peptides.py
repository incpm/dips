#!/usr/bin/env python

import argparse
import csv
import itertools

from dips.cleaner import PeptideCleaner
from dips.io import PeptideReader


def clean_peptides(input_filenames, output_file_handler, **kwargs):
    try:
        peptide_reader = PeptideReader([open(csv_filename, 'r') for csv_filename in input_filenames])
        peptides = peptide_reader.read()
        cleaner = PeptideCleaner(**kwargs)
        clean_peptides = list(itertools.chain(*[cleaner.clean(peptide=peptide) for peptide in peptides]))
        csv_writer = csv.writer(output_file_handler, delimiter=',')
        csv_writer.writerow(['Scan', 'Peptide', 'local confidence (%)'])
        for clean_peptide in clean_peptides:
            csv_writer.writerow([clean_peptide.single_identifier, clean_peptide.sequence, ' '.join([str(v) for v in clean_peptide.confidence])])
    finally:
        output_file_handler.close()

def main():
    parser = argparse.ArgumentParser(description='Read peptides, find clean segments and save them')
    parser.add_argument('--input', required=True, nargs='+', type=str, help='CSV files containing peptides', dest='input_filenames')
    parser.add_argument('--output', required=True, type=argparse.FileType('w'), help='Output file', dest='output_file_handler')
    parser.add_argument('--min-length', required=True, type=int, help='minimal length of clean peptide')
    parser.add_argument('--min-good-confidence', required=True, type=int, help='threshold for what is considered a good confidence score')
    parser.add_argument('--min-edge-confidence', required=True, type=int, help='threshold for what is considered a good confidence score around the edges')
    clean_peptides(**vars(parser.parse_args()))

if __name__ == '__main__':
    main()
