#!/usr/bin/env python

import argparse
import logging.config
import os

from dips.proteins import MYOGLOBIN
from dips.runner import AssemblyRunner
from dips.settings.logging_settings import LOGGING

logger = logging.getLogger('dips.' + __name__)

DEFAULT_SETTINGS_MODULE = 'dips.settings'


def log_config(log_file):
    print 'Initializing logging configuration from dictionary with log file %s' % log_file
    LOGGING['handlers']['file']['filename'] = log_file
    logging.config.dictConfig(LOGGING)


def run_assembly(input_filenames, output_dir, settings_module):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    assert os.path.isdir(output_dir)
    log_config(os.path.join(output_dir, 'dips.log'))
    # TODO: add proteins as a parameter
    runner = AssemblyRunner(proteins=[MYOGLOBIN], input_filenames=input_filenames, output_dir=output_dir,
                            settings_module=settings_module)
    runner.run()
    return runner


def load_assembly_results():
    parser = argparse.ArgumentParser(description='Assemble peptides')
    parser.add_argument('--input', required=True, nargs='+', type=str, help='CSV files containing peptides',
                        dest='input_filenames')
    parser.add_argument('--settings-module', type=str, default=DEFAULT_SETTINGS_MODULE,
                        help='Settings Python module (default is %s)' % DEFAULT_SETTINGS_MODULE, dest='settings_module')
    parser.add_argument('--output', required=True, type=str, help='Output directory - should not exist on file-system',
                        dest='output_dir')
    run_assembly(**vars(parser.parse_args()))

if __name__ == '__main__':
    load_assembly_results()
