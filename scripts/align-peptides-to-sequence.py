#!/usr/bin/env python

import argparse
import json

from dips.align import AlignmentSwapScorer, NaiveAligner
from dips.data import Protein
from dips.jsonutils import json_load_object_hook

from dips.io import KmerWriter


def main(args):
    # get arguments and execute:
    peptides_json_file = args.peptides_json_file
    sequence = args.sequence
    coverage_output_file = args.coverage_output_file
    dataset_name = args.dataset_name

    # load cleaned peptides and then kmers:
    cleaned_peptides = json.load(fp=open(peptides_json_file, 'r'),
                                 object_hook=json_load_object_hook)
    # swap aligner:
    protein = Protein.clean_raw_sequence('sequence', sequence)
    swap_scorer = AlignmentSwapScorer(accuracy_threshold=0.8, min_confidence=0)
    swap_aligner = NaiveAligner(proteins=[protein], scorer=swap_scorer, stop_on_first_alignment=False)

    # get alignments:
    peptides_alignments, _ = swap_aligner.align(cleaned_peptides)

    # calculate coverage:
    coverages = []
    for i in range(len(protein.sequence)):
        filtered_peptides_alignments = [alignment for alignment in peptides_alignments if
                                        alignment.position <= i < len(alignment.sequence) + alignment.position]
        count = 0
        for peptide_alignment in filtered_peptides_alignments:
            # fill variants dict:
            peptide_acid = peptide_alignment.sequence.sequence[i - peptide_alignment.position]
            if peptide_acid == protein.sequence[i]:
                count += 1
        coverages.append([(protein.sequence[i], count)])

    # write coverages:
    KmerWriter.write_coverage_to_csv([coverages], dataset_name, coverage_output_file)




def get_options():
    parser = argparse.ArgumentParser(description='assemble peptides with kmer assembler')
    parser.add_argument('--peptides-json-file', required=True, help='the cleaned peptides json file.')
    parser.add_argument('--coverage-output-file', required=True, help='the cleaned peptides json file.')
    parser.add_argument('--sequence', required=True, help='the sequence that the peptides need to align to.')
    parser.add_argument('--dataset-name', help='name for the dataset, default=dataset',default='dataset')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = get_options()
    main(args)