#!/usr/bin/env python

import argparse
from dips.downsample import downsample
from dips.io import PeptideReader, ProteinSequenceCsvWriter


def main():
    parser = argparse.ArgumentParser(description='Downsample a list of peptides, for testing purposes')
    parser.add_argument('--input', required=True, nargs='+', type=str, help='CSV files containing peptides',
                        dest='input_filenames')
    parser.add_argument('--num-sequences', required=True, type=int, help='Number of sequences to be taken from input')
    parser.add_argument('--output', required=True, type=str, help='Output file', dest='output_filename')
    args = parser.parse_args()
    sequences = PeptideReader([open(fn, 'r') for fn in args.input_filenames], min_clean_length=0).read()
    downsampled_sequences = downsample(sequences, max_num_sequences=args.num_sequences)
    ProteinSequenceCsvWriter(downsampled_sequences).write(args.output_filename)

if __name__ == '__main__':
    main()
