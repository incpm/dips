import json
import logging
import os
import tempfile
import unittest

from nose.plugins.attrib import attr

from dips.assemble import KmersHandler
from dips.data import Peptide
from dips.io import PeptideReader
from dips.jsonutils import json_dump_default, json_load_object_hook
from .utils import TEST_INPUT_DIR

AMINO_ACIDS = list('GAVLIPFYWSTCMNQKRHDE')

logger = logging.getLogger('dips.' + __name__)


@attr(test_type='unit')
class PeptideReaderTest(unittest.TestCase):
    def setUp(self):
        peptides_filename = os.path.join(TEST_INPUT_DIR, 'de_novo_peptides.csv')
        self.peptide_reader = PeptideReader(open(peptides_filename, 'r'))

    def test_read1(self):
        peptides = self.peptide_reader.read()
        self.assertEquals(len(peptides), 1488)
        first_peptide = peptides[0]
        self.assertEquals(first_peptide.identifier, {'9182_a'})
        self.assertEquals(first_peptide.single_identifier, '9182_a')
        self.assertEquals(first_peptide.sequence, 'TPDETYVPKAF')
        self.assertEquals(first_peptide.confidence, [99, 99, 100, 100, 100, 100, 100, 99, 99, 100, 99])
        self.assertEquals(first_peptide.sequence_options,
                          [{'T': 99}, {'P': 99}, {'D': 100}, {'E': 100}, {'T': 100}, {'Y': 100},
                           {'V': 100}, {'P': 99}, {'K': 99}, {'A': 100}, {'F': 99}])

    def test_amino_acids(self):
        peptides = self.peptide_reader.read()
        acid_count = dict([(acid, 0) for acid in AMINO_ACIDS])
        for peptide in peptides:
            for acid in peptide.sequence:
                assert acid in AMINO_ACIDS
                acid_count[acid] += 1
        logger.info('Acid count: %s' % acid_count)

    def test_confidence_spread(self):
        peptides = self.peptide_reader.read()
        confidence_count = list([0] * 11)
        for peptide in peptides:
            for confidence in peptide.confidence:
                assert 0 <= confidence <= 100
                confidence_count[confidence / 10] += 1
        logger.info('Confidence count: %s' % confidence_count)

    def test_dump_and_load_peptides(self):
        peptides = self.peptide_reader.read()
        self.assertEquals(len(peptides), 1488)
        _, temp_output_file = tempfile.mkstemp(suffix='.json', prefix='peptides.')
        json.dump(obj=peptides, fp=open(temp_output_file, 'w'), default=json_dump_default, indent=2)
        loaded_peptides = json.load(fp=open(temp_output_file, 'r'), object_hook=json_load_object_hook)
        self.assertEquals(len(loaded_peptides), len(peptides))
        self.assertEquals(sorted([p.identifier for p in peptides]),
                          sorted([p.identifier for p in loaded_peptides]))
        self.assertEquals(sorted([p.sequence for p in peptides]),
                          sorted([p.sequence for p in loaded_peptides]))


@attr(test_type='unit')
class KmerJsonTest(unittest.TestCase):
    def test_dump_and_load_kmers(self):
        peptides = json.load(fp=open('test-input/modified_peptides.json', 'r'), object_hook=json_load_object_hook)
        num_peptides = 10
        peptides = peptides[:num_peptides]
        from dips.data import peptides_cache
        peptides_cache.clear()
        peptides_cache.update(dict([(peptide.single_identifier, peptide) for peptide in peptides]))
        self.assertEquals(len(peptides), num_peptides)
        kmers = KmersHandler(kmer_size=7).get_kmers(peptides)
        num_kmers = 36
        kmers = kmers[:num_kmers]
        self.assertEquals(len(kmers), num_kmers)
        _, temp_output_file = tempfile.mkstemp(suffix='.json', prefix='kmers.')
        json.dump(obj=kmers, fp=open(temp_output_file, 'w'), default=json_dump_default, indent=2)
        loaded_kmers = json.load(fp=open(temp_output_file, 'r'), object_hook=json_load_object_hook)
        self.assertEquals(len(loaded_kmers), len(kmers))


@attr(test_type='unit')
class KmerContigJsonTest(unittest.TestCase):
    def test_dump_and_load_kmers(self):
        peptides = json.load(fp=open('test-input/modified_peptides.json', 'r'), object_hook=json_load_object_hook)
        from dips.data import peptides_cache, kmers_cache
        peptides_cache.clear()
        kmers_cache.clear()
        peptides_cache.update(dict([(peptide.single_identifier, peptide) for peptide in peptides]))
        kmers = json.load(fp=open('test-input/modified_kmers.json', 'r'), object_hook=json_load_object_hook)
        kmers_cache.update(dict([(kmer.identifier, kmer) for kmer in kmers]))
        _, temp_output_file = tempfile.mkstemp(suffix='.json', prefix='kmer_contigs.')
        kmer_contigs = KmersHandler(kmer_size=7).assemble_kmers(kmers)
        json.dump(obj=kmer_contigs, fp=open(temp_output_file, 'w'), default=json_dump_default, indent=2)
        loaded_kmer_contigs = json.load(fp=open(temp_output_file, 'r'), object_hook=json_load_object_hook)
        self.assertEquals(len(loaded_kmer_contigs), 36)

@attr(test_type='unit')
class PeptidesUtils(unittest.TestCase):
    def test_find_modifications(self):
        sequence1 = 'M(+15.99)LR(+.98)DLAA(-13)KYKEL(+.98)'
        sequence2 = 'MD(+15.99)LDLR(+.98)'
        sequence3 = 'MDLAA(+33)KYKEL(+.98)'
        result1 = Peptide.find_modifications(sequence1)
        result2 = Peptide.find_modifications(sequence2)
        result3 = Peptide.find_modifications(sequence3)
        self.assertEquals(result1, [0, 2, 6, 11])
        self.assertEquals(result2, [1, 5])
        self.assertEquals(result3, [4, 9])

