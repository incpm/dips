from collections import defaultdict
import unittest

from nose.plugins.attrib import attr

from dips.proteins import BSA, FOUR_PROTEINS, MYOGLOBIN, FETUIN


@attr(test_type='unit')
class CleanProteinTest(unittest.TestCase):
    def test_clean_myoglobin(self):
        protein = MYOGLOBIN.clean()
        print(protein.sequence)
        self.assertEquals(protein.sequence,
                          'MGLSDGEWEEVLDVWGKVEADLAGHGEEVLLRLFTGHPETLEKFDKFKHLKTEAEMKASEDLKKHGTVVLTALGGLLKKKGHHEAELKPLAESHATKHKLPLKYLEFLSDALLHVLHSKHPGDFGADAEGAMTKALELFRDDLAAKYKELGFEG')

    def test_clean_fetuin(self):
        protein = FETUIN.clean()
        print(protein.sequence)
        self.assertEquals(protein.sequence,
                          'MKSFVLLFCLAELWGCHSLPLDPVAGYKEPACDDPDTEEAALAAVDYLDKHLPRGYKHTLDELDSVKVWPRRPTGEVYDLELDTLETTCHVLDPTPLADCSVREETEHAVEGDCDLHVLKEDGEFSVLFTKCDSSPDSAEDVRKLCPDCPLLAPLDDSRVVHAVEVALATFDAESDGSYLELVELSRAEFVPLPVSVSVEFAVAATDCLAKEVVDPTKCDLLAEKEYGFCKGSVLEKALGGEDVRVTCTLFETEPVLPEPEPDGAEAEAPSAVPDAAGPTPSAAGPPVASVVVGPSVVAVPLPLHRAHYDLRHTFSGVASVESSSGEAFHVGKTPLVGEPSLPGGPVRLCPGRLRYFKL')


@attr(test_type='unit')
class UniqueKmersTest(unittest.TestCase):
    """
    Given a list of proteins, find all kmers of a given size which appear more than once.
    This should help us determine which kmer size to use during assembly
    """
    def test_unique_kmers_size_4(self):
    #     # this test passes when 'S' is not replaced by 'A'
    #     self._assert_repeating_kmers_given_kmer_size(proteins=[BSA], kmer_size=4, expected_num_recurring_kmers=12,
    #                                                  expected_recurring_kmers={'AEDK': [(BSA, 193), (BSA, 332)], 'CDEF': [(BSA, 146), (BSA, 414)], 'EECC': [(BSA, 188), (BSA, 380)],
    #                                                                            'ETAL': [(BSA, 548), (BSA, 602)], 'KECC': [(BSA, 265), (BSA, 298)], 'LEEC': [(BSA, 54), (BSA, 379)],
    #                                                                            'LPKL': [(BSA, 135), (BSA, 201)], 'LSLL': [(BSA, 6), (BSA, 475)], 'LVDE': [(BSA, 65), (BSA, 402)],
    #                                                                            'PEDL': [(BSA, 321), (BSA, 406)], 'RRHP': [(BSA, 166), (BSA, 358)], 'SLLL': [(BSA, 7), (BSA, 476)]})
    #     # this test passes when 'S' is replaced by 'A'
    #     self._assert_repeating_kmers_given_kmer_size(proteins=[BSA], kmer_size=4, expected_num_recurring_kmers=13)
        self._assert_repeating_kmers_given_kmer_size(proteins=[BSA], kmer_size=4, expected_num_recurring_kmers=4)

    # def test_unique_kmers_size_5(self):
    #     # this test passes when 'S' is not replaced by 'A'
    #     self._assert_repeating_kmers_given_kmer_size(proteins=[BSA], kmer_size=5,
    #                                                  expected_num_recurring_kmers=1, expected_recurring_kmers={'LSLLL': [(BSA, 6), (BSA, 475)]})
    #     # this test passes when 'S' is replaced by 'A'
#         self._assert_repeating_kmers_given_kmer_size(proteins=[BSA], kmer_size=5,
#                                                      expected_num_recurring_kmers=2)

    def test_unique_kmers_size_6(self):
        self._assert_repeating_kmers_given_kmer_size(proteins=[BSA], kmer_size=6, expected_num_recurring_kmers=0)

    # this test passes when 'S' is not replaced by 'A'
    def test_unique_kmers_size_6_multiple_proteins(self):
        self._assert_repeating_kmers_given_kmer_size(proteins=FOUR_PROTEINS, kmer_size=6, expected_num_recurring_kmers=0)

    # # this test passes when 'S' is not replaced by 'A'
    # def test_unique_kmers_size_5_multiple_proteins(self):
    #     self._assert_repeating_kmers_given_kmer_size(proteins=FOUR_PROTEINS, kmer_size=5,
    #                                                  expected_num_recurring_kmers=1, expected_recurring_kmers={'LSLLL': [(ALBU_BOVIN, 6), (ALBU_BOVIN, 475)]})

    # this test passes when 'S' is not replaced by 'A'
    def test_unique_kmers_size_4_multiple_proteins(self):
        # kmers = self._assert_repeating_kmers_given_kmer_size(proteins=FOUR_PROTEINS, kmer_size=4, expected_num_recurring_kmers=19)
        # self.assertEquals(sorted(kmers.keys()), ['AAGP', 'AEDK', 'CDEF', 'EDVR', 'EECC', 'EEVL', 'ETAL', 'HAVE', 'KECC', 'LEEC',
        #                                          'LPKL', 'LSLL', 'LVDE', 'PEDL', 'RRHP', 'SLLL', 'TKCD', 'VASV', 'VPLP'])
        kmers = self._assert_repeating_kmers_given_kmer_size(proteins=FOUR_PROTEINS, kmer_size=4, expected_num_recurring_kmers=9)
        self.assertEquals(sorted(kmers.keys()), ['AAGP', 'AEDK', 'EDVR', 'HAVE', 'KECC',
                                                 'QTAL', 'RRHP', 'VASV', 'VPLP'])

    def _get_protein_kmers(self, protein, kmer_size):
        kmers = defaultdict(list)
        for i in range(len(protein.sequence) - kmer_size + 1):
            kmer = protein.sequence[i:i+kmer_size]
            kmers[kmer].append((protein, i))
        return kmers

    def _assert_repeating_kmers_given_kmer_size(self, proteins, kmer_size, expected_num_recurring_kmers, expected_recurring_kmers={}):
        kmers = dict()
        for protein in proteins:
            kmers.update(self._get_protein_kmers(protein=protein, kmer_size=kmer_size))
        kmers = dict([(k,v) for k,v in kmers.items() if len(v) > 1])
        self.assertEquals(len(kmers), expected_num_recurring_kmers)
        if expected_recurring_kmers:
            self.assertEquals(kmers, expected_recurring_kmers)
        return kmers
