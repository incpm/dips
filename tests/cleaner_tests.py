import unittest
from dips.data import Peptide
from dips.cleaner import PeptideCleaner
from nose.plugins.attrib import attr
from nose.tools.nontrivial import nottest

@attr(test_type='unit')
class CleanerTest(unittest.TestCase):
    def setUp(self):
        self.cleaner = PeptideCleaner(min_length=2,min_good_confidence=50)

    def test_cleaner_cd_pattern(self):
        id = "seq1"
        confidence = [100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100]
        # test middle:
        sequence = "DKFSDDDCDOWOFRKCDDFF"
        peptide = Peptide(id, sequence,confidence)
        split_peptides = self.cleaner.clean(peptide)
        self.assertEquals(len(split_peptides),2)
        self.assertEquals(split_peptides[0].sequence,"DKFS")
        self.assertEquals(split_peptides[1].sequence,"OWOFRKCDDFF")
        # test edge:
        sequence = "DKFSDDDFRGWOFRKCDDCC"
        peptide = Peptide(id, sequence,confidence)
        split_peptides = self.cleaner.clean(peptide)
        self.assertEquals(len(split_peptides),1)
        self.assertEquals(split_peptides[0].sequence,"DKFSDDDFRGWOFRK")

    def test_cleaner_low_confidence(self):
        id = "seq1"
        sequence = "DKFSDDDFRGWOFRKEEFCC"
        # test middle:
        confidence = [100, 100, 100, 49, 40, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 40, 40, 40, 100, 100]
        peptide = Peptide(id, sequence,confidence)
        split_peptides = self.cleaner.clean(peptide)
        self.assertEquals(len(split_peptides),3)
        self.assertEquals(split_peptides[0].sequence,"DKF")
        self.assertEquals(split_peptides[1].sequence,"DDFRGWOFRK")
        self.assertEquals(split_peptides[2].sequence,"CC")
        # test edge:
        confidence = [40, 49, 40, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 49, 10, 10, 10]
        peptide = Peptide(id, sequence,confidence)
        split_peptides = self.cleaner.clean(peptide)
        self.assertEquals(len(split_peptides),1)
        self.assertEquals(split_peptides[0].sequence,"SDDDFRGWOFRKE")

    def test_cleaner_min_length(self):
        id = "seq1"
        confidence = [100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100]
        # test middle:
        sequence = "SDCDDDDCDOCCDDKCDDFF"
        peptide = Peptide(id, sequence,confidence)
        split_peptides = self.cleaner.clean(peptide)
        self.assertEquals(len(split_peptides),1)
        self.assertEquals(split_peptides[0].sequence,"KCDDFF")

    def test_cleaner_amino_acid_switch(self):
        id = "seq1"
        confidence = [100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100]
        # test middle:
        sequence = "NNFSDDDQDOWOFQNCDDFF"
        peptide = Peptide(id, sequence,confidence)
        split_peptides = self.cleaner.clean(peptide)
        self.assertEquals(len(split_peptides),1)
        self.assertEquals(split_peptides[0].sequence,"DDFSDDDEDOWOFEDCDDFF")

    def test_cleaner_complex(self):
        id = "seq1"
        confidence = [100, 100, 100, 100, 49, 50, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100]
        # test middle:
        sequence = "NNFSDDDQDOWOFQNCDDDF"
        peptide = Peptide(id, sequence,confidence)
        split_peptides = self.cleaner.clean(peptide)
        self.assertEquals(len(split_peptides),2)
        self.assertEquals(split_peptides[0].sequence,"DDFS")
        self.assertEquals(split_peptides[1].sequence,"DDEDOWOFED")
