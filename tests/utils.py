import os
from dips.data import ProteinSequence

PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))

TEST_INPUT_DIR = os.path.join(PROJECT_ROOT, 'test-input')


def str_to_sequence(sequence_string, identifier='dummy'):
    return ProteinSequence(identifier=identifier, sequence=sequence_string,
                           confidence=[99] * len(sequence_string))
