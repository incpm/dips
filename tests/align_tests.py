import logging
import unittest

from nose.plugins.attrib import attr

from nose.tools.nontrivial import nottest

from dips.align import NaiveAligner, AlignmentScorer, LevenshteinAlignmentScorer
from dips.data import ProteinSequence
from dips.proteins import BSA, MYG_HORSE, ALBU_BOVIN, FETUIN, MYOGLOBIN
from tests.utils import str_to_sequence

logger = logging.getLogger('dips.' + __name__)


class ContigAlignerTest(unittest.TestCase):
    def setUp(self):
        self.scorer = LevenshteinAlignmentScorer(alignment_per_amino_acid_accuracy=0.98)
        self.aligner = NaiveAligner(proteins=[self.protein], scorer=self.scorer, stop_on_first_alignment=False)

    def _read_contigs(self, input_filename):
        # read and init contigs from a simple list Rotem created
        contig_strings = [contig.strip() for contig in open(input_filename, 'r').readlines()]
        contig_strings.sort(key=len, reverse=True)
        contig_sequences = []
        for (i, contig_string) in enumerate(contig_strings):
            contig_sequences.append(ProteinSequence(sequence=contig_string,
                                                    identifier='contig-%04d' % i,
                                                    confidence=[99] * len(contig_string)))
        return contig_sequences


@attr(test_type='unit')
class UnitedContigsFetuinAlignerTest(ContigAlignerTest):
    def setUp(self):
        self.protein = FETUIN.clean()
        super(UnitedContigsFetuinAlignerTest, self).setUp()

    def test_align1(self):
        contig_sequences = self._read_contigs('test-input/fetuin-contigs-2015-12-30.csv')
        self.assertEquals(len(contig_sequences), 3374)
        self.assertEquals(len(self.protein), 359)
        contig_sequences = contig_sequences[51:55]
        # make sure these contigs are shorter than the protein
        self.assertEquals([len(c) for c in contig_sequences], [357, 357, 357, 356])
        alignments, unaligned_sequences = self.aligner.align(sequences=contig_sequences)
        self.assertEqual(len(alignments), 4)
        self.assertEquals(len(unaligned_sequences), 0)

    def test_align2(self):
        contig_sequences = self._read_contigs('test-input/fetuin-contigs-2015-12-30.csv')
        contig_sequences = [c for c in contig_sequences if 200 <= len(c) <= 250]  # filter contigs by length
        num_contigs = len(contig_sequences)
        self.assertEquals(num_contigs, 120)
        alignments, unaligned_sequences = self.aligner.align(sequences=contig_sequences)
        # print(sorted([a.quality for a in alignments]))
        print("Unaligned:")
        for unaligned_sequence in unaligned_sequences:
            print unaligned_sequence.sequence
        self.assertEqual(len(alignments), 119)  # should ideally be equal to num_contigs


@attr(test_type='unit')
class UnitedContigsMyoglobinAlignerTest(ContigAlignerTest):
    def setUp(self):
        self.protein = MYOGLOBIN.clean()
        super(UnitedContigsMyoglobinAlignerTest, self).setUp()

    def test_align1(self):
        contig_sequences = self._read_contigs('test-input/myo-contigs-2015-12-31.csv')
        self.assertEquals(len(contig_sequences), 3505)
        contig_sequences.sort(key=len, reverse=True)
        contig_sequences = [c for c in contig_sequences if 80 <= len(c) <= 100]  # filter contigs by length
        num_contigs = len(contig_sequences)
        self.assertEquals(num_contigs, 356)
        alignments, unaligned_sequences = self.aligner.align(sequences=contig_sequences)
        print(sorted([a.quality for a in alignments]))
        print("Unaligned:")
        for unaligned_sequence in unaligned_sequences:
            print unaligned_sequence.sequence
        self.assertEqual(len(alignments), 356)  # should ideally be equal to num_contigs

    def test_align2(self):
        contig_sequences = self._read_contigs('test-input/myo-contigs-2016-01-03-kmer-size-12-kmer-min-overlap-10.csv')
        self.assertEquals(len(contig_sequences), 1113)
        num_contigs = 500
        contig_sequences = contig_sequences[:num_contigs]  # take a subset of the contigs
        alignments, unaligned_sequences = self.aligner.align(sequences=contig_sequences)
        self.assertEqual(len(alignments), 494)
        self.assertEquals(sorted([us.sequence for us in unaligned_sequences]),
                          ['ALKAVCVLKGDGPVHGVLHFEEEEEGGPVVLK', 'CGTTAGAHFDPLSKKHGGPKDEERHVGDLG',
                           'VAALAVCVLKGDGPVHGVLHFEEEEEGGPVVLK', 'VALAVCVLKGDGPVHGVLHFEEEEEGGPVVLK',
                           'VVTGHKKLDESAKMEAETKLHKFKDFKELPPD', 'VVTGHKKLDESAKMEAETKLHKFKDFKELTEPYD'])


@attr(test_type='unit')
class LevenshteinAlignmentScorerTest(unittest.TestCase):
    def setUp(self):
        self.scorer = LevenshteinAlignmentScorer(alignment_per_amino_acid_accuracy=0.98)
        self.protein = FETUIN.clean()

    def test_scorer1(self):
        # take a sequence of the protein - position 18, length 34
        sequence = str_to_sequence('LPLDPVAGYKEPACDDPDTEEAALAAVDYLDKHL')
        alignment = self.scorer.score(protein=self.protein, protein_position=18, sequence=sequence)
        self.assertIsNotNone(alignment)
        self.assertEquals(alignment.quality, 1.0)
        alignment = self.scorer.score(protein=self.protein, protein_position=17, sequence=sequence)
        self.assertIsNotNone(alignment)
        self.assertAlmostEqual(alignment.quality, 32./34, places=2)
        alignment = self.scorer.score(protein=self.protein, protein_position=16, sequence=sequence)
        self.assertIsNotNone(alignment)
        self.assertAlmostEqual(alignment.quality, 30./34, places=2)
        alignment = self.scorer.score(protein=self.protein, protein_position=0, sequence=sequence)
        self.assertEquals(alignment, None)


@attr(test_type='unit')
class AlignmentScorerTest(unittest.TestCase):
    def test_align1(self):
        #                                    DKGACLLPKLETMREKVLTSSAR
        sequence = ProteinSequence(sequence='EKGACLLPKLETMRDEDVLLKWE',
                                   identifier={11360, 9827, 9444, 9957, 10022, 10283, 9933, 7790, 9958, 9522,
                                               10166, 10809, 8858, 9822},
                                   confidence=[90, 187, 166, 543, 1050, 1242, 1369, 1176, 1186, 1213, 1244, 414, 340,
                                               248, 81, 108, 78, 90, 173, 88, 79, 68, 80])
        # should be aligned to BSA at location 196 - 'DKGACLLPKLETMREKVLTSSAR'
        scorer = AlignmentScorer(min_confidence=80, alignment_per_amino_acid_accuracy=0.95)
        alignment = scorer.score(protein=BSA, protein_position=195, sequence=sequence)
        self.assertIsNotNone(alignment)
        self.assertEquals(alignment.quality, 0.6)

    def test_align2(self):
        #                                    ADDKEACFAVEGPKLVVSTETALA
        sequence = ProteinSequence(sequence='ADDKEACFAVEGPKLVVSTETALA',
                                   identifier=set([7979, 10248, 12746, 10443, 8184, 9810, 10068, 6552, 7962, 6491,
                                                   6173, 6755, 7719, 7915, 6508, 7147, 6065, 9139, 6900, 6782, 6200,
                                                   6516, 9018, 6909, 6590]),
                                   confidence=[159, 429, 533, 556, 588, 654, 659, 728, 787, 963, 1246, 1142, 1253, 1582,
                                               1619, 1530, 1706, 1582, 1096, 1038, 557, 415, 270, 74])
        # should be aligned to BSA at location 196 - 'DKGACLLPKLETMREKVLTSSAR'
        scorer = AlignmentScorer(accuracy_threshold=0.6, min_confidence=80)
        alignment = scorer.score(protein=BSA, protein_position=583, sequence=sequence)
        self.assertIsNotNone(alignment)
        self.assertAlmostEquals(alignment.quality, 0.95, delta=0.01)

#     def test_align3(self):
#         # interesting case of a very long contig which was not aligned to MYG_HORSE because it contained one extra amino acid at the end:
#         # MYG_HORSE:                        MGLSDGEWEEVLDVWGKVEADLAGHGEEVLLRLFTGHPETLEKFDKFKHLKTEAEMKASEDLKKHGTVVLTALGGLLKKKGHHEAELKPLAESHATKHKLPLKYLEFLSDALLHVLHSKHPGDFGADAEGAMTKALELFRDDLAAKYKELGFEG
#         sequence = ProteinSequence(sequence='TLSDEEWEEVLDVWGKVEADLAGHGEEVLLRLFTGHPETLEKFDKFKHLKTEAEMKASEDLKKHGTVVLTALGGLLKKKGHHEAELKPLAESHATKHKLPLKYLEFLSDALLHVLHSKHPGDFGADAEGAMTKALELFRDDLAAKYKELGFEGG',
# #         sequence = ProteinSequence(sequence='LSDEEWEEVLDVWGKVEADLAGHGEEVLLRLFTGHPETLEKFDKFKHLKTEAEMKASEDLKKHGTVVLTALGGLLKKKGHHEAELKPLAESHATKHKLPLKYLEFLSDALLHVLHSKHPGDFGADAEGAMTKALELFRDDLAAKYKELGFEG',
#                                    identifier=set([16393, 16394, 24587]),  # and a lot more...
#                                    confidence=[40, 48, 89, 116, 264, 316, 434, 745, 936, 1042, 1085, 1132, 1376, 1465, 1395, 2627, 2593, 2799, 2738, 3324, 4875, 5371, 5307, 7787,
#                                                6467, 9244, 10447, 11803, 12499, 11715, 8412, 9081, 5994, 5623, 3918, 6973, 5969, 7001, 7780, 8114, 8665, 8655, 8334, 8546, 9137, 9177,
#                                                10000, 10415, 11250, 10227, 9168, 10428, 8633, 9984, 9529, 7269, 6395, 5303, 6778, 6393, 6766, 6296, 5971, 3800, 1625, 1289, 2322, 3604,
#                                                3775, 3521, 3628, 4300, 2599, 2458, 5580, 7236, 6830, 6527, 4483, 1517, 2645, 5154, 6206, 5549, 6518, 6800, 6890, 5586, 5764, 5405, 5513,
#                                                4074, 3421, 2887, 1723, 1600, 2441, 2907, 4807, 3264, 3857, 4886, 5176, 6061, 7172, 7583, 9082, 8708, 9346, 10596, 12164, 12560, 11834,
#                                                11448, 11433, 8971, 5601, 5462, 6381, 2554, 1826, 3428, 4029, 3834, 5049, 7163, 8789, 11021, 8588, 12597, 19533, 24352, 25258, 26389, 27712,
#                                                27892, 20803, 11843, 7603, 7619, 8156, 14626, 15114, 20611, 14177, 12603, 10048, 11081, 10390, 6004, 5750, 4732, 1812, 262])
#         scorer = AlignmentScorer(sequence=sequence, accuracy_threshold=0.9, min_confidence=80)
#         alignment = scorer.score(protein=MYG_HORSE, protein_position=1)
#         self.assertIsNotNone(alignment)
#         self.assertAlmostEqual(alignment.quality, 0.99, places=2)


@attr(test_type='unit')
class AlignerTest(unittest.TestCase):
    @nottest
    def test_align1(self):
        # interesting case - assembled together 6 sequences but was not aligned to BSA.
        # reason: all sequences individually were not aligned too. perhaps a contamination?
        sequence = ProteinSequence(sequence='SPYSYSTTALVSSPKA',
                                   identifier=set([8706, 7300, 5830, 8493, 7102, 5647]),
                                   confidence=[66, 85, 98, 99, 99, 99, 99, 99, 99, 100, 99, 96, 95, 89, 68, 60])
        scorer = AlignmentScorer(accuracy_threshold=0.8)
        aligner = NaiveAligner(proteins=[BSA], scorer=scorer)
        alignments, unaligned_sequences = aligner.align(sequences=[sequence])
        self.assertEqual(len(alignments), 0)
        self.assertEqual(len(unaligned_sequences), 1)

    def test_align2(self):
        # yet another interesting case - a contig which should be aligned to the end of the protein, but was not due to a small bug 
        sequence = ProteinSequence(sequence='ADDKEACFAVEGPKLVVSTETALA',
                                   identifier={7979, 10248, 12746, 10443, 8184, 9810, 10068, 6552, 7962, 6491,
                                               6173, 6755, 7719, 7915, 6508, 7147, 6065, 9139, 6900, 6782, 6200,
                                               6516, 9018, 6909, 6590},
                                   confidence=[159, 429, 533, 556, 588, 654, 659, 728, 787, 963, 1246, 1142, 1253, 1582,
                                               1619, 1530, 1706, 1582, 1096, 1038, 557, 415, 270, 74])
        # should be aligned to BSA at location 196 - 'DKGACLLPKLETMREKVLTSSAR'
        scorer = AlignmentScorer(accuracy_threshold=0.8, min_confidence=80)
        aligner = NaiveAligner(proteins=[BSA], scorer=scorer)
        alignments, unaligned_sequences = aligner.align(sequences=[sequence])
        self.assertEqual(len(alignments), 1)
        self.assertEqual(len(unaligned_sequences), 0)

    def test_align3(self):
# MYG_HORSE:                                   MGLSDGEWEEVLDVWGKVEADLAGHGEEVLLRLFTGHPETLEKFDKFKHLKTEAEMKASEDLKKHGTVVLTALGGLLKKKGHHEAELKPLAESHATKHKLPLKYLEFLSDALLHVLHSKHPGDFGADAEGAMTKALELFRDDLAAKYKELGFEG
        sequence = ProteinSequence(sequence='FWMTLSDGEWE', identifier={'dummy'}, confidence=[100]*11)
        scorer = AlignmentScorer(alignment_per_amino_acid_accuracy=0.98, min_confidence=80)
        aligner = NaiveAligner(proteins=[MYG_HORSE], scorer=scorer)
        alignments, unaligned_sequences = aligner.align(sequences=[sequence])
        self.assertEqual(len(alignments), 1)
        self.assertEqual(len(unaligned_sequences), 0)
        self.assertEqual(alignments[0].position, -2)

    def test_align4(self):
        sequence = ProteinSequence(sequence='TLSDEEWEEVLDVWGKVEADLAGHGEEVLLRLFTGHPETLEKFDKFKHLKTEAEMKASEDLKKHGTVVLTALGGLLKKKGHHEAELKPLAESHATKHKLPLKYLEFLSDALLHVLHSKHPGDFGADAEGAMTKALELFRDDLAAKYKELGFEGG',
                                   identifier={16393, 16394, 24587},  # and a lot more...
                                   confidence=[40, 48, 89, 116, 264, 316, 434, 745, 936, 1042, 1085, 1132, 1376, 1465, 1395, 2627, 2593, 2799, 2738, 3324, 4875, 5371, 5307, 7787,
                                               6467, 9244, 10447, 11803, 12499, 11715, 8412, 9081, 5994, 5623, 3918, 6973, 5969, 7001, 7780, 8114, 8665, 8655, 8334, 8546, 9137, 9177,
                                               10000, 10415, 11250, 10227, 9168, 10428, 8633, 9984, 9529, 7269, 6395, 5303, 6778, 6393, 6766, 6296, 5971, 3800, 1625, 1289, 2322, 3604,
                                               3775, 3521, 3628, 4300, 2599, 2458, 5580, 7236, 6830, 6527, 4483, 1517, 2645, 5154, 6206, 5549, 6518, 6800, 6890, 5586, 5764, 5405, 5513,
                                               4074, 3421, 2887, 1723, 1600, 2441, 2907, 4807, 3264, 3857, 4886, 5176, 6061, 7172, 7583, 9082, 8708, 9346, 10596, 12164, 12560, 11834,
                                               11448, 11433, 8971, 5601, 5462, 6381, 2554, 1826, 3428, 4029, 3834, 5049, 7163, 8789, 11021, 8588, 12597, 19533, 24352, 25258, 26389, 27712,
                                               27892, 20803, 11843, 7603, 7619, 8156, 14626, 15114, 20611, 14177, 12603, 10048, 11081, 10390, 6004, 5750, 4732, 1812, 262])
        scorer = AlignmentScorer(accuracy_threshold=0.6, min_confidence=80)
        aligner = NaiveAligner(proteins=[MYG_HORSE], scorer=scorer)
        alignments, unaligned_sequences = aligner.align(sequences=[sequence])
        self.assertEqual(len(alignments), 1)
        self.assertEqual(len(unaligned_sequences), 0)
        self.assertEqual(alignments[0].position, 1)

    def test_align5(self):
        pass
        # a contig which should be aligned to ALBU_BOVIN at position 433 according to its aligned peptides
        sequence = ProteinSequence(sequence='YTRKVPEVSTPTLVEVSRSLGKVGRTMEKPES',
                                   identifier={16641, 14476, 18960, 19090},  # and a lot more...
                                   confidence=[79, 151, 105, 266, 402, 489, 678, 936, 1086, 2067, 2452, 2996, 3369, 3187, 3167,
                                               2677, 1977, 1489, 1451, 1372, 900, 992, 817, 504, 35, 61, 27, 88, 89, 86, 97, 90])
        scorer = AlignmentScorer(min_confidence=80, alignment_per_amino_acid_accuracy=0.98)
        aligner = NaiveAligner(proteins=[ALBU_BOVIN], scorer=scorer)
        alignments, unaligned_sequences = aligner.align(sequences=[sequence])
        self.assertEqual(len(alignments), 1)
        self.assertEqual(len(unaligned_sequences), 0)
        self.assertEqual(alignments[0].position, 433)
        self.assertAlmostEqual(alignments[0].quality, 0.78, delta=0.01)

    def test_align6(self):
        pass
        # a contig which should be aligned to FETUA_BOVIN at position 18 according to its aligned peptides 
# Unaligned contig #17: sequence=LPLDPVAGYKEPACDDPDVDP, identifier=[19627, 8199, 9102, 4718, 19734, 19610, 5023, 8739, 10788, 9637, 10666, 14215, 7469, 7470, 5300, 5814, 6844, 5181, 8639, 6592, 20033, 10178, 11981, 17102, 8655, 8790, 8282, 6877, 18527, 8429, 5358, 5693, 5876, 18806, 8184, 5255, 10366], confidence=[564, 576, 743, 992, 2056, 2259, 2462, 2404, 2950, 2993, 3287, 2690, 2813, 2879, 2737, 1957, 283, 190, 89, 90, 78]
# Peptide alignment # 5: Alignment<position= 18,sequence=sequence=LPLDPVAGYKEPACDD, identifier=[19627], confidence=[92, 97, 99, 99, 97, 97, 99, 93, 97, 97, 100, 98, 98, 96, 98, 99],protein=set(['FETUA_BOVIN']),quality=1.000000>
# Peptide alignment #12: Alignment<position= 22,sequence=sequence=PVAGYKEPACD, identifier=[8199], confidence=[93, 96, 98, 94, 97, 97, 99, 93, 97, 99, 98],protein=set(['FETUA_BOVIN']),quality=1.000000>

    # def test_align7(self):
    #     peptide = Peptide(sequence='SEYEELAEERDK',
    #                       identifier=4529, confidence=[62, 65, 98, 95, 85, 94, 91, 95, 70, 43, 75, 83])
    #     aligner = NaiveAligner(proteins=[BSA], accuracy_threshold=0.5, alignment_per_amino_acid_accuracy=0.9,
    #                            stop_on_first_alignment=False, min_confidence=70)
    #     alignments, unaligned_sequences = aligner.align(sequences=[peptide])
    #     self.assertEqual(len(alignments), 1)
    #     self.assertEqual(len(unaligned_sequences), 0)
    #     self.assertAlmostEqual(alignments[0].quality, 0.56, places=2, msg=str(alignments[0]))

    # def test_alignN(self):
    #     sequence = ProteinSequence(sequence='EKKACLLPKLETMRDEDVTLKWE',
    #                                identifier={11360, 9827, 9444, 10022, 9958, 10283, 9933, 7790, 9522, 10166, 10809,
    #                                            8858, 9822, 9957},
    #                                confidence=[90, 95, 91, 92, 98, 99, 100, 93, 96, 98, 99, 98, 97, 89, 81, 73, 78, 90,
    #                                            89, 88, 79, 68, 80])
    #     aligner = NaiveAligner(proteins=[BSA], alignment_per_amino_acid_accuracy=0.98, min_confidence=80)
    #     alignments, unaligned_sequences = aligner.align(sequences=[sequence])  # @UnusedVariable
    #     self.assertEqual(len(alignments), 1)
    #     self.assertEqual(len(unaligned_sequences), 0)
    #     self.assertAlmostEqual(alignments[0].quality, 0.68, places=2)
        # BSA:          DKGACLLPKLETMREKVLTSSA
        #               -+-+++++++++++----+---
        # joined        EKKACLLPKLETMRDEDVTLKWE
#                      AEKKECLLPKLETMRLMGVLLKWE, identifier=[10795, 10053, 9933, 8858, 9950, 11360, 9827, 9444, 9957, 9958, 10678, 10283, 7790, 9522, 10022, 9822, 10166, 10809, 8378, 10943], confidence=[65, 169, 187, 214, 604, 1476, 1745, 1935, 1584, 1614, 1646, 1625, 592, 383, 327, 225, 164, 113, 371, 484, 88, 79, 68, 80]
        #
        #                  CALLPKLETM
        #                  ACLLPKLETMR
        # unaligned     EDKECLLPKLE
        # unaligned        ACLLPKLEPTGFEDALLVRY
        #                  ECLLPKLETMR
        # unaligned        ECLLPKLE
        # unaligned        ECLLPKLEPHADP
        # unaligned           LPKLEVFAWELLTSKWE
        #               DKGACLLPKLE
        #                  ACLLPKLE
        # unaligned        ACLLPKLETSRLMGVL
        #                KGACLLPKLE
        #                  ACLLPKLETMR
        #                  ECLLPKLETTEW
# Alignment #387: Alignment<position=198,sequence=sequence=CALLPKLETM identifier=[11360] confidence=[50, 75, 95, 99, 75, 54, 65, 96, 98, 97],protein=set(['BSA']),quality=1.000000>
# Alignment #388: Alignment<position=198,sequence=sequence=ACLLPKLETMR identifier=[9827] confidence=[87, 89, 97, 99, 89, 88, 90, 92, 85, 95, 89],protein=set(['BSA']),quality=1.000000>
# Unaligned sequence #55: sequence=EDKECLLPKLE identifier=[9444] confidence=[90, 93, 91, 86, 89, 92, 99, 86, 93, 98, 98]
# Unaligned sequence #77: sequence=ACLLPKLEPTGFEDALLVRY identifier=[10022] confidence=[48, 69, 93, 96, 63, 82, 69, 59, 18, 13, 7, 73, 73, 78, 82, 85, 88, 74, 38, 38]
# Alignment #390: Alignment<position=198,sequence=sequence=ECLLPKLETMR identifier=[9958] confidence=[70, 90, 97, 99, 82, 80, 82, 86, 62, 63, 61],protein=set(['BSA']),quality=1.000000>
# Unaligned sequence #50: sequence=ECLLPKLE identifier=[10283] confidence=[82, 98, 99, 100, 93, 89, 96, 99]
# Unaligned sequence #40: sequence=ECLLPKLEPHADP identifier=[9933] confidence=[67, 88, 94, 99, 82, 84, 88, 90, 41, 66, 63, 81, 63]
# Unaligned sequence #48: sequence=LPKLEVFAWELLTSKWE identifier=[7790] confidence=[89, 86, 96, 86, 80, 46, 46, 45, 34, 35, 78, 89, 89, 85, 79, 68, 80]
# Alignment #384: Alignment<position=195,sequence=sequence=DKGACLLPKLE identifier=[9522] confidence=[79, 92, 82, 90, 91, 94, 98, 90, 96, 96, 97],protein=set(['BSA']),quality=1.000000>
# Alignment #386: Alignment<position=198,sequence=sequence=ACLLPKLE identifier=[10166] confidence=[92, 93, 99, 100, 88, 79, 92, 99],protein=set(['BSA']),quality=1.000000>
# Unaligned sequence #156: sequence=ACLLPKLETSRLMGVL identifier=[10809] confidence=[66, 82, 96, 98, 78, 79, 82, 75, 39, 23, 27, 79, 63, 62, 90, 88]
# Alignment #385: Alignment<position=196,sequence=sequence=KGACLLPKLE identifier=[8858] confidence=[95, 84, 88, 90, 94, 98, 93, 96, 95, 95],protein=set(['BSA']),quality=1.000000>
# Alignment #389: Alignment<position=198,sequence=sequence=ACLLPKLETMR identifier=[9822] confidence=[72, 76, 95, 96, 86, 86, 88, 88, 65, 85, 71],protein=set(['BSA']),quality=1.000000>
# Alignment #391: Alignment<position=198,sequence=sequence=ECLLPKLETTEW identifier=[9957] confidence=[59, 95, 97, 99, 85, 84, 86, 90, 65, 51, 74, 27],protein=set(['BSA']),quality=1.000000>
