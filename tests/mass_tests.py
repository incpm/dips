import logging
import unittest

from nose.plugins.attrib import attr
from dips.mass import calc_mass_vector

logger = logging.getLogger('dips.' + __name__)

@attr(test_type='unit')
class MassTest(unittest.TestCase):
    def test_calc_mass_vector1(self):
        self.assertEquals(calc_mass_vector('FSEYL'), [147.17, 87.08, 129.11, 163.17, 113.16])
        self.assertEquals(calc_mass_vector('FSEYL', k=2), [234.25, 216.19, 292.29, 276.33])
        self.assertEquals(calc_mass_vector('FSEYL', k=3), [363.37, 379.36, 405.44])
        self.assertEquals(calc_mass_vector('KTDYL'), [128.17, 101.1, 115.09, 163.17, 113.16])
        self.assertEquals(calc_mass_vector('KTDYL', k=2), [229.28, 216.19, 278.26, 276.33])
        self.assertEquals(calc_mass_vector('KTDYL', k=3), [344.36, 379.36, 391.42])

    def test_calc_mass_vector2(self):
        self.assertEquals(calc_mass_vector('ARERL'), [71.08, 156.19, 129.11, 156.19, 113.16])
        self.assertEquals(calc_mass_vector('ARERL', k=2), [227.26, 285.3, 285.3, 269.34])
        self.assertEquals(calc_mass_vector('ARERL', k=3), [356.38, 441.49, 398.46])
        self.assertEquals(calc_mass_vector('ARERL', k=4), [512.56, 554.64])
        self.assertEquals(calc_mass_vector('WAELL'), [186.21, 71.08, 129.11, 113.16, 113.16])
        self.assertEquals(calc_mass_vector('WAELL', k=2), [257.29, 200.19, 242.27, 226.32])
        self.assertEquals(calc_mass_vector('WAELL', k=4), [499.56, 426.51])

    def test_calc_mass_vector3(self):
        self.assertEquals(calc_mass_vector('AKPEDTMR', k=2), [199.25, 225.29, 226.23, 244.2, 216.19, 232.3, 287.38])
        self.assertEquals(calc_mass_vector('TKPESERM', k=2), [229.28, 225.29, 226.23, 216.19, 216.19, 285.3, 287.38])

    def test_calc_mass_vector4(self):
        self.assertEquals(calc_mass_vector('SLVDRPTEPEP', k=1), [87.08, 113.16, 99.13, 115.09, 156.19, 97.12, 101.1, 129.11, 97.12, 129.11, 97.12])
        self.assertEquals(calc_mass_vector('SLVDRPTEPEP', k=2), [200.23, 212.29, 214.22, 271.27, 253.3, 198.22, 230.22, 226.23, 226.23, 226.23])
        self.assertEquals(calc_mass_vector('SLVDRPTEPEP', k=3), [299.37, 327.38, 370.4, 368.39, 354.4, 327.33, 327.33, 355.34, 323.34])
        self.assertEquals(calc_mass_vector('SLVDRRPCFSA', k=1), [87.08, 113.16, 99.13, 115.09, 156.19, 156.19, 97.12, 103.14, 147.17, 87.08, 71.08])
        self.assertEquals(calc_mass_vector('SLVDRRPCFSA', k=3), [299.37, 327.38, 370.4, 427.46, 409.49, 356.44, 347.43, 337.39, 305.33])

    def test_calc_mass_vector5(self):
        # this is an interesting case - where A is actually S, and the mass of SE is equal to that of TD, but since we replace S with A we do not recognize it
        self.assertEquals(calc_mass_vector('AEYLEE', k=1), [71.08, 129.11, 163.17, 113.16, 129.11, 129.11])
        self.assertEquals(calc_mass_vector('AEYLEE', k=2), [200.19, 292.29, 276.33, 242.27, 258.23])
        self.assertEquals(calc_mass_vector('TDYLEE', k=1), [101.1, 115.09, 163.17, 113.16, 129.11, 129.11])
        self.assertEquals(calc_mass_vector('TDYLEE', k=2), [216.19, 278.26, 276.33, 242.27, 258.23])

    def test_calc_mass_vector6(self):
        self.assertEquals(calc_mass_vector('FAEY', k=2), [218.25, 200.19, 292.29])
        self.assertEquals(calc_mass_vector('DEGF', k=2), [244.2, 186.17, 204.23])
        self.assertEquals(calc_mass_vector('FAEY', k=3), [347.37, 363.37])
        self.assertEquals(calc_mass_vector('DEGF', k=3), [301.25, 333.34])
        self.assertEquals(calc_mass_vector('FAEY', k=4), [510.54])
        self.assertEquals(calc_mass_vector('DEGF', k=4), [448.43])
