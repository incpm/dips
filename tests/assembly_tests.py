import unittest

from nose.plugins.attrib import attr
from nose.tools.nontrivial import nottest

from dips.data import Peptide
from dips.assemble import OnePassAssembler, MultiPassAssembler, NaiveContigAssembler, \
    EdgesContigAssembler, EdgesAssembler, CompositeAssembler, LevenshteinDistance, MassDistance
from dips.io import PeptideReader


@attr(test_type='unit')
class AssemblyTest(unittest.TestCase):
    def test_simple_assembly1(self):
        peptides = {Peptide(1, 'PDTLCDEFK', [96, 99, 100, 100, 99, 99, 100, 100, 99]),
                    Peptide(2, 'PDTLCDEFKAD', [94, 98, 100, 100, 100, 99, 100, 100, 100, 99, 97])}
        assembler = OnePassAssembler(kmer_size=5, min_confidence=80, max_num_iterations=1,
                                     num_kmers_to_use_per_iteration=100)
        assembled_contigs = assembler.assemble(sequences=peptides)
        self.assertEquals(len(assembled_contigs), 1)
        self.assertEquals(set(assembled_contigs.pop().sequences), peptides)

    def test_simple_assembly2(self):
        peptides = {Peptide(1, 'KDLDK', [96, 99, 100, 100, 85]),
                    Peptide(2, 'PDTLCDEFKAD', [94, 98, 100, 100, 100, 99, 100, 100, 100, 99, 97])}
        assembler = MultiPassAssembler(kmer_sizes=(11, 9, 7, 5), min_confidence=80, max_num_iterations=1,
                                       num_kmers_to_use_per_iteration=200)
        assembled_contigs = assembler.assemble(sequences=peptides)
        self.assertEquals(len(assembled_contigs), 2)


@attr(test_type='unit')
class NaiveContigAssemblerTest(unittest.TestCase):
    def test_contig_assembly1(self):
        peptides = {Peptide(1, 'KDCDEFKL', [70, 50, 60, 70, 80, 90, 100, 60]),
                    Peptide(2, 'LACDEFKAD', [60, 95, 100, 90, 80, 70, 60, 50, 40])}
        assembler = NaiveContigAssembler(kmer_size=5, min_confidence=80, num_kmers_to_assemble=200)
        assembly_result = assembler.assemble(sequences=peptides, )
        self.assertEquals(assembly_result, ([], set()))

    def test_contig_assembly2(self):
        peptides = {Peptide(1, 'KDCDEFKL', [70, 50, 60, 70, 80, 90, 100, 60]),
                    Peptide(2, 'LACDEFKAD', [60, 95, 100, 90, 80, 70, 60, 50, 40])}
        assembler = NaiveContigAssembler(kmer_size=5, min_confidence=60, num_kmers_to_assemble=200)
        (contigs, old_sequences) = assembler.assemble(sequences=peptides)
        self.assertEquals(len(contigs), 1)
        contig = contigs[0]
        self.assertEquals(contig.sequence, 'KACDEFKLD')
#         self.assertEquals(contig.confidence, [70,95,100,90,80,90,100,60,40])
        self.assertEquals(contig.confidence, [70, 95, 160, 160, 160, 160, 160, 60, 40])
        self.assertEquals(old_sequences, peptides)

    def test_contig_assembly3(self):
                                                           # C   x   D   T   H   x   S   E   L   A   H   x   x   x   x   x   G    E    E  ...
        peptides = {Peptide(1, 'CDDTHKSELAHRFKDLGEEHFKGL', [39, 80, 96, 99, 99, 99, 98, 99, 99, 99, 99, 95, 95, 96, 99, 99, 98, 100, 100, 99, 99, 99, 96, 98]),
                    Peptide(2, 'CADTHESELAHGVRHP',         [24, 27, 69, 92, 89, 73, 56, 79, 80, 82, 79, 27, 46, 43, 55, 44])}
        assembler = NaiveContigAssembler(kmer_size=5, min_confidence=80, num_kmers_to_assemble=200)  # if min_confidence is 50 this will work wonderfully
        assembly_result = assembler.assemble(sequences=peptides)
        self.assertEquals(assembly_result, ([], set()))
#         self.assertEquals(assembly_result.sequence, 'KACDEFKLD')
#         self.assertEquals(assembly_result.confidence, [70,95,100,90,80,90,100,60,40])

    def test_contig_assembly4(self):
# 41: Alignment<position= 84,sequence=GCEKSLHTLFGDELCK [6, 14, 27, 23, 48, 63, 89, 91, 88, 45, 22, 40, 89, 52, 50, 61],protein=ALBU_BOVIN,quality=1.000000>
# 42: Alignment<position= 84,sequence=ECEKTVHTLFG [34, 48, 73, 59, 41, 40, 72, 68, 89, 82, 52],protein=ALBU_BOVIN,quality=1.000000>
#                                                   x    C   E   K   x   x   H   T   L   F   G   ...
        peptides = {Peptide(1, 'GCEKSLHTLFGDELCK', [6,  14, 27, 23, 48, 63, 89, 91, 88, 45, 22, 40, 89, 52, 50, 61]),
                    Peptide(2, 'ECEKTVHTLFG',      [34, 48, 73, 59, 41, 40, 72, 68, 89, 82, 52])}
        assembler = NaiveContigAssembler(kmer_size=5, min_confidence=80, num_kmers_to_assemble=200)  # if min_confidence is 50 this will work wonderfully
        assembly_result = assembler.assemble(sequences=peptides)
        self.assertEquals(assembly_result, ([], set()))

    def test_contig_assembly5(self):
#52: Alignment<position=102,sequence=SLRETYGDMADCCEKEEPER [22, 29, 13, 30, 33, 79, 51, 74, 92, 66, 62, 40, 41, 66, 41, 74, 78, 24, 55, 21],protein=ALBU_BOVIN,quality=1.000000>
#53: Alignment<position=102,sequence=TVRETYGDMA [57, 57, 52, 82, 60, 75, 59, 96, 79, 71],protein=ALBU_BOVIN,quality=1.000000>
                                                       # x   x   R   E   T   Y   G   D   M   A ...
        peptides = {Peptide(1, 'SLRETYGDMADCCEKEEPER', [22, 29, 13, 30, 33, 79, 51, 74, 92, 66, 62, 40, 41, 66, 41, 74, 78, 24, 55, 21]),
                    Peptide(2, 'TVRETYGDMA',           [57, 57, 52, 82, 60, 75, 59, 96, 79, 71])}
        assembler = NaiveContigAssembler(kmer_size=5, min_confidence=80, num_kmers_to_assemble=200)
        assembly_result = assembler.assemble(sequences=peptides)
        self.assertEquals(assembly_result, ([], set()))


@attr(test_type='unit')
class EdgesContigAssemblerTest(unittest.TestCase):
    def test_levenshtein_distance(self):
        #                                      1100001110111000
        self.assertEquals(LevenshteinDistance('GSFLYEYSRRREPYAV',
                                              'SGFLYEKEERHPEYAV').calculate(), 8)

    def test_mass_distance(self):
        #                               0000001110100000
        self.assertEquals(MassDistance('GSFLYEYSRRREPYAV',
                                       'SGFLYEKEERHPEYAV').calculate(), 4)

    def test_contig_assembly1(self):
        # TADFAEDKDVCKDYEEAKDAFLGSFLYEYSRRREPYAV
        #                       SGFLYEKEERHPEYAVSVLLRLAKEYEATLEECCAKDDPHA
        peptides = {Peptide(1, 'TADFAEDKDVCKDYEEAKDAFLGSFLYEYSRRREPYAV', [6459, 6753, 8921, 11219, 11746, 12839, 11910, 12148, 12610, 13789, 13540, 13203, 12264, 11897, 11832, 11764, 10137, 8077, 6509, 6151, 5854, 5610, 2957, 1666, 1740, 1668, 1303, 1206, 690, 541, 537, 431, 188, 162, 45, 91, 95, 92]),
                    Peptide(2, 'SGFLYEKEERHPEYAVSVLLRLAKEYEATLEECCAKDDPHA', [31, 17, 102, 126, 117, 45, 97, 92, 98, 374, 534, 773, 1149, 2325, 3213, 4551, 5191, 5156, 5324, 4841, 3260, 3959, 3748, 4264, 4371, 3737, 4221, 4277, 4448, 4555, 4782, 4698, 4120, 3889, 3758, 2978, 3274, 3557, 4005, 4394, 4805])}
        assembler = EdgesContigAssembler(overlap_size=10)
        assembly_result = assembler.assemble(sequences=peptides)
        self.assertEquals(assembly_result, ([], set()))
        assembler = EdgesContigAssembler(overlap_size=16)
        (contigs, old_sequences) = assembler.assemble(sequences=peptides)
        self.assertEquals(len(contigs), 1)
        contig = contigs[0]
        # this assertion works when 'S' is not replaced by 'A'
        self.assertEquals(contig.sequence, 'TADFAEDKDVCKDYEEAKDAFLGSFLYEYSRRHPEYAVSVLLRLAKEYEATLEECCAKDDPHA')
        # this assertion works when 'S' is replaced by 'A'
#         self.assertEquals(contig.sequence, 'TADFAEDKDVCKDYEEAKDAFLGAFLYEYARRHPEYAVAVLLRLAKEYEATLEECCAKDDPHA')
        self.assertEquals(old_sequences, peptides)

    @nottest  # TODO: re-enable once we fix this 'S' => 'A' issue
    def test_contig_assembly2(self):
        # DCDTHKSELAHRFKDLGEEHFKGLVLLAFSEYL
        #                             KTDYLEECPFDEHVKLVDELTEFAKTCVADESHAGCEKS
        peptides = {Peptide(1, 'LVLLAFSEYL', [2449, 1358, 1416, 653, 437, 279, 179, 141, 89, 90]),
                    Peptide(2, 'KTDYLEECP', [43, 412, 473, 758, 786, 873, 966, 952, 3530])}
        # test #1
        assembler = EdgesAssembler(min_overlap=10, max_overlap=16, max_num_iterations=3)
        contigs = assembler.assemble(sequences=peptides)
        self.assertEquals(contigs, peptides)
        # test #2
        assembler = EdgesAssembler(min_overlap=5, max_overlap=10, max_num_iterations=3)
        contigs = assembler.assemble(sequences=peptides)
        self.assertEquals(len(contigs), 1)
        contig = contigs.pop()
        self.assertEquals(contig.sequence, 'LVLLAFTDYLEECP')


class WrongAssemblyTester(unittest.TestCase):
    def test_assembly(self):
        """this is a problematic contig - it is assembled from lots of peptides, but the assembly is a false one."""
        #json_contig = {"sequence_options": [{"P": 9, "T": 22}, {"P": 17, "T": 9}, {"P": 28, "K": 43, "L": 64, "T": 50, "V": 31}, {"H": 10, "K": 35, "L": 31, "P": 15, "T": 67, "V": 22}, {"H": 7, "K": 494, "L": 25, "P": 14, "R": 36, "V": 63}, {"A": 8, "E": 8, "D": 8, "K": 21, "R": 21, "V": 693}, {"P": 840, "K": 30, "R": 5, "E": 26, "D": 6}, {"E": 1095, "D": 14, "K": 8, "P": 20, "R": 5, "T": 81, "V": 79}, {"E": 111, "L": 41, "V": 2112}, {"S": 2901}, {"T": 3638}, {"P": 3581}, {"T": 3998}, {"L": 4262, "V": 25}, {"H": 37, "V": 4445}, {"A": 71, "S": 150, "R": 45, "E": 4446}, {"A": 55, "S": 237, "L": 117, "V": 4104}, {"A": 149, "E": 18, "G": 23, "F": 94, "S": 3178, "R": 55, "T": 23, "V": 256}, {"A": 63, "E": 80, "D": 72, "F": 62, "H": 39, "K": 30, "L": 91, "P": 18, "S": 70, "R": 2584, "T": 40, "W": 127, "V": 121}, {"A": 92, "E": 65, "G": 35, "K": 286, "L": 414, "P": 11, "S": 2190, "R": 104, "T": 21, "W": 62, "V": 103}, {"A": 119, "E": 61, "D": 49, "G": 84, "K": 109, "L": 1982, "S": 535, "R": 84, "T": 263, "V": 143}, {"E": 1121, "D": 289, "G": 1322, "K": 59, "L": 105, "P": 20, "T": 79, "W": 54, "V": 153}, {"P": 12, "K": 3723, "L": 55}, {"S": 44, "F": 3643, "M": 22, "L": 14, "V": 580}, {"A": 34, "E": 165, "D": 63, "G": 367, "M": 11, "L": 51, "P": 3195}, {"A": 141, "P": 44, "K": 3399, "E": 33}, {"A": 3960, "P": 50, "K": 82, "E": 137, "S": 25}, {"A": 79, "E": 4863, "G": 14, "F": 30, "K": 138, "P": 85, "Y": 21}, {"P": 16, "K": 29, "V": 49, "D": 40, "F": 6138}, {"V": 7188, "E": 96, "F": 51}, {"E": 7570, "V": 164}, {"E": 99, "T": 92, "V": 8027}, {"K": 95, "T": 8427}, {"K": 8360, "T": 78}, {"K": 67, "L": 8872}, {"L": 85, "V": 8294}, {"S": 92, "T": 7711, "V": 78}, {"E": 91, "M": 15, "D": 7606, "V": 92}, {"S": 11, "E": 93, "L": 7408}, {"C": 19, "T": 6474}, {"S": 28, "K": 6319, "V": 18}, {"V": 5709, "L": 33, "F": 49}, {"C": 46, "H": 5376, "S": 40, "T": 47, "D": 26}, {"C": 28, "E": 80, "D": 21, "F": 44, "H": 101, "K": 3380, "M": 30, "L": 75, "R": 79, "W": 23}, {"C": 155, "E": 2659, "D": 62, "G": 26, "F": 17, "H": 20, "K": 33, "R": 7, "T": 75}, {"A": 9, "C": 1469, "E": 114, "D": 57, "H": 44, "Y": 43}, {"P": 44, "C": 1513, "M": 44, "D": 18, "W": 97}, {"A": 24, "E": 179, "D": 89, "G": 84, "H": 1035, "P": 5, "T": 55, "V": 116}, {"D": 18, "G": 688, "H": 86, "P": 44, "T": 102, "V": 120}, {"V": 33, "L": 118, "T": 43, "D": 877}, {"K": 30, "L": 1371, "D": 102}, {"L": 1637, "V": 63}, {"E": 1728, "L": 72}, {"C": 1484, "E": 94, "D": 39}, {"A": 1535, "W": 33, "E": 113, "D": 54, "G": 18}, {"A": 57, "C": 33, "E": 62, "D": 1776, "G": 62, "T": 51}, {"C": 46, "R": 52, "E": 78, "D": 2326, "G": 59}, {"E": 311, "D": 225, "G": 54, "H": 26, "L": 56, "R": 1514}, {"A": 1985, "E": 239, "G": 189, "F": 22, "L": 93, "R": 48, "W": 48, "V": 135}, {"A": 59, "E": 238, "D": 2620, "G": 106, "R": 193, "W": 184}, {"L": 4717}, {"A": 4838, "K": 71}, {"A": 75, "K": 5254}, {"Y": 5178}, {"L": 5269}, {"C": 4997}, {"D": 5246}, {"D": 5170, "G": 66}, {"C": 55, "E": 4620, "D": 227, "G": 60}, {"P": 126, "E": 200, "D": 7266, "G": 18}, {"D": 276, "G": 15, "H": 56, "L": 83, "P": 51, "T": 7810}, {"H": 5302, "T": 138, "L": 4271}, {"K": 5647, "S": 4425}, {"S": 9711, "E": 225, "K": 89}, {"S": 262, "E": 6198, "K": 3949, "L": 150}, {"K": 114, "L": 10893}, {"A": 6875, "K": 3645, "W": 55}, {"H": 6353, "V": 38, "R": 29, "E": 3617, "F": 71}, {"A": 34, "C": 3216, "F": 318, "H": 70, "K": 57, "P": 41, "R": 5424, "W": 47, "V": 27}, {"A": 39, "C": 2840, "E": 79, "D": 37, "F": 5388, "H": 24, "K": 253, "M": 45, "S": 44, "R": 313, "T": 75}, {"A": 146, "C": 40, "E": 186, "D": 2401, "F": 239, "K": 4994, "L": 122, "R": 183, "T": 131}, {"A": 43, "C": 48, "D": 6193, "G": 102, "F": 34, "K": 1480, "L": 48, "P": 140, "T": 83, "Y": 41}, {"A": 65, "E": 253, "D": 45, "L": 5782, "P": 1002, "V": 88}, {"P": 153, "L": 1120, "G": 3513, "T": 89}, {"E": 3419, "L": 835, "G": 23}, {"K": 81, "E": 3429}, {"H": 2382, "K": 543}, {"F": 1839}, {"K": 1478}, {"G": 895}, {"L": 649}, {"V": 99}, {"L": 99}], "confidence": [22, 17, 64, 67, 494, 693, 840, 1095, 2112, 2901, 3638, 3581, 3998, 4262, 4445, 4446, 4104, 3178, 2584, 2190, 1982, 1322, 3723, 3643, 3195, 3399, 3960, 4863, 6138, 7188, 7570, 8027, 8427, 8360, 8872, 8294, 7711, 7606, 7408, 6474, 6319, 5709, 5376, 3380, 2659, 1469, 1513, 1035, 688, 877, 1371, 1637, 1728, 1484, 1535, 1776, 2326, 1514, 1985, 2620, 4717, 4838, 5254, 5178, 5269, 4997, 5246, 5170, 4620, 7266, 7810, 5302, 5647, 9711, 6198, 10893, 6875, 6353, 5424, 5388, 4994, 6193, 5782, 3513, 3419, 3429, 2382, 1839, 1478, 895, 649, 99, 99], "identifier": ["9076", "23519", "16979", "19497", "14671", "32978", "25462", "27281", "4020", "10104", "4028", "43677", "38111", "11382", "11662", "17840", "37785", "11045", "9938", "7512", "19155", "33428", "7195", "8801", "40086", "23603", "30212", "9372", "24785", "17157", "24783", "35434", "9987", "12827", "39827", "4876", "15058", "4936", "4735", "19193", "6564", "15056", "23848", "12140", "10614", "9574", "12262", "28221", "23461", "10534", "32552", "25884", "8017", "18375", "11205", "13541", "26726", "17701", "10431", "14597", "16311", "20111", "8803", "3471", "7382", "12015", "22855", "15583", "9782", "8930", "12790", "31793", "5682", "15639", "8042", "22789", "20627", "25737", "35055", "31616", "19602", "9004", "10542", "12843", "34113", "21612", "7707", "31483", "11517", "23064", "35123", "26025", "37722", "28089", "28088", "15125", "35278", "6935", "24950", "10387", "28789", "13520", "25787", "37797", "24219", "20137", "8941", "7268", "7090", "3364", "18852", "17961", "27323", "10914", "27326", "10910", "35874", "19590", "31392", "9132", "14967", "21225", "4275", "9134", "4811", "19407", "17298", "26322", "4371", "38027", "25559", "25998", "37201", "24504", "5189", "11064", "4836", "23006", "29745", "14811", "17896", "16746", "31326", "32455", "7141", "22759", "12814", "9183", "7678", "34946", "22058", "19678", "42544", "12935", "27577", "3436", "29817", "19989", "14643", "18896", "14645", "9111", "14902", "5201", "21096", "22519", "11552", "25869", "9041", "8793", "34386", "31631", "11390", "26463", "3548", "11399", "13276", "27837", "24642", "25577", "15601", "23204", "21892", "12077", "11302", "12779", "4707", "5585", "43562", "15364", "23115", "29749", "37619", "9564", "35132", "36778", "18223", "30139", "7698", "9687", "22572", "25292", "20571", "13721", "8331", "18009", "26363", "10110", "24380", "25440", "26566", "24113", "24913", "22277", "18414", "28434", "12786", "34101", "39830", "9266", "6977", "31400", "23777", "6906", "19655", "16942", "16940", "9495", "20359", "9996", "11036", "15646", "28002", "29805", "5844", "39086", "22086", "16925", "9722", "32160", "22227", "35143", "9729", "26546", "10423", "22380", "31459", "20100", "26992", "15336", "7276", "36137", "7279", "28003", "5086", "24769", "35867", "30201", "24938", "26223", "9798", "29849", "19169", "23129", "9535", "26862", "18604", "23406", "12634", "35049", "42525", "22425", "14634", "21522", "24000", "16803", "43529", "8001", "26134", "8616", "16004", "44070", "20664", "24618", "18267", "10156", "10157", "26948", "9332", "20087", "15094", "13313", "5108", "12976", "11234", "22048", "19904", "16934", "6049", "24744", "16933", "12986", "38004", "3373", "21737", "20347", "10923", "7765", "9979", "22337", "10928", "25919", "7282", "19089", "17459", "9100", "7838", "28875", "18138", "22527", "25569", "16980", "13468", "33296", "26592", "26591", "13463", "5194", "27870", "11243", "32502", "8923", "17212", "22402", "11060", "13993", "10072", "30239", "18650", "19551", "30231", "28298", "31356", "7237", "20668", "7104", "23199", "15279", "34661", "21365", "22949", "27944", "11410", "27696", "22597", "15225", "37629", "42572", "38570", "15372", "7061", "6339", "10942", "37604"], "sequence": "TPLTKVPEVSTPTLVEVSRSLGKFPKAEFVEVTKLVTDLTKVHKECCHGDLLECADDRADLAKYLCDDEDTHKSELAHRFKDLGEEHFKGLVL", "cls": "Contig"}
        peptide_reader = PeptideReader([open(csv_filename, 'r') for csv_filename in ['test-input/s3.csv']], min_confidence=80)
        all_peptides = peptide_reader.read()
        peptide_ids = ["9076", "23519", "16979", "19497", "14671", "32978", "25462", "27281", "4020", "10104", "4028", "43677", "38111", "11382", "11662", "17840",
                       "37785", "11045", "9938", "7512", "19155", "33428", "7195", "8801", "40086", "23603", "30212", "9372", "24785", "17157", "24783", "35434",
                       "9987", "12827", "39827", "4876", "15058", "4936", "4735", "19193", "6564", "15056", "23848", "12140", "10614", "9574", "12262", "28221",
                       "23461", "10534", "32552", "25884", "8017", "18375", "11205", "13541", "26726", "17701", "10431", "14597", "16311", "20111", "8803", "3471",
                       "7382", "12015", "22855", "15583", "9782", "8930", "12790", "31793", "5682", "15639", "8042", "22789", "20627", "25737", "35055", "31616",
                       "19602", "9004", "10542", "12843", "34113", "21612", "7707", "31483", "11517", "23064", "35123", "26025", "37722", "28089", "28088", "15125",
                       "35278", "6935", "24950", "10387", "28789", "13520", "25787", "37797", "24219", "20137", "8941", "7268", "7090", "3364", "18852", "17961",
                       "27323", "10914", "27326", "10910", "35874", "19590", "31392", "9132", "14967", "21225", "4275", "9134", "4811", "19407", "17298", "26322",
                       "4371", "38027", "25559", "25998", "37201", "24504", "5189", "11064", "4836", "23006", "29745", "14811", "17896", "16746", "31326", "32455",
                       "7141", "22759", "12814", "9183", "7678", "34946", "22058", "19678", "42544", "12935", "27577", "3436", "29817", "19989", "14643", "18896",
                       "14645", "9111", "14902", "5201", "21096", "22519", "11552", "25869", "9041", "8793", "34386", "31631", "11390", "26463", "3548", "11399",
                       "13276", "27837", "24642", "25577", "15601", "23204", "21892", "12077", "11302", "12779", "4707", "5585", "43562", "15364", "23115", "29749",
                       "37619", "9564", "35132", "36778", "18223", "30139", "7698", "9687", "22572", "25292", "20571", "13721", "8331", "18009", "26363", "10110",
                       "24380", "25440", "26566", "24113", "24913", "22277", "18414", "28434", "12786", "34101", "39830", "9266", "6977", "31400", "23777", "6906",
                       "19655", "16942", "16940", "9495", "20359", "9996", "11036", "15646", "28002", "29805", "5844", "39086", "22086", "16925", "9722", "32160",
                       "22227", "35143", "9729", "26546", "10423", "22380", "31459", "20100", "26992", "15336", "7276", "36137", "7279", "28003", "5086", "24769",
                       "35867", "30201", "24938", "26223", "9798", "29849", "19169", "23129", "9535", "26862", "18604", "23406", "12634", "35049", "42525", "22425",
                       "14634", "21522", "24000", "16803", "43529", "8001", "26134", "8616", "16004", "44070", "20664", "24618", "18267", "10156", "10157", "26948",
                       "9332", "20087", "15094", "13313", "5108", "12976", "11234", "22048", "19904", "16934", "6049", "24744", "16933", "12986", "38004", "3373",
                       "21737", "20347", "10923", "7765", "9979", "22337", "10928", "25919", "7282", "19089", "17459", "9100", "7838", "28875", "18138", "22527",
                       "25569", "16980", "13468", "33296", "26592", "26591", "13463", "5194", "27870", "11243", "32502", "8923", "17212", "22402", "11060", "13993",
                       "10072", "30239", "18650", "19551", "30231", "28298", "31356", "7237", "20668", "7104", "23199", "15279", "34661", "21365", "22949", "27944",
                       "11410", "27696", "22597", "15225", "37629", "42572", "38570", "15372", "7061", "6339", "10942", "37604"]
        peptides = [p for p in all_peptides if p.single_identifier in peptide_ids]
        self.assertEquals(len(peptides), len(peptide_ids))
        assembler = CompositeAssembler()
        assembled_contigs = assembler.assemble(sequences=peptides)
        self.assertEquals(len(assembled_contigs), 1)
        assembled_contig = list(assembled_contigs)[0]
        expected_contig_sequence = 'TPLTKVPEVSTPTLVEVSRSLGKFPKAEFVEVTKLVTDLTKVHKECCHGDLLECADDRADLAKYLCDDEDTHKSELAHRFKDLGEEHFKGLVL'
        self.assertEquals(assembled_contig.sequence, expected_contig_sequence)

#  Protein: set(['ALBU_BOVIN'])
#  Number of aligned sequences: 29

#MKWVTFLSLLLLFSSAYSRGVFRRDTHKSELAHRFKDLGEEHFKGLVLLAFSEYLEECPFDEHVKLVDELTEFAKTCVADESHAGCEKSLHTLFGDELCKVASLRETYGDMADCCEKEEPERDECFLSHKDDSPDLPKLKPDPDTLCDEFKADEKKFWGKYLYELARRHPYFYAPELLYYADKYDGVFEECCEAEDKGACLLPKLETMREKVLTSSARERLRCASLEKFGERALKAWSVARLSEKFPKAEFVEVTKLVTDLTKVHKECCHGDLLECADDRADLAKYLCDDEDTLSSKLKECCDKPLLEKSHCLAEVEKDALPEDLPPLTADFAEDKDVCKDYEEAKDAFLGSFLYEYSRRHPEYAVSVLLRLAKEYEATLEECCAKDDPHACYSTVFDKLKHLVDEPEDLLKEDCDEFEKLGEYGFEDALLVRYTRKVPEVSTPTLVEVSRSLGKVGTRCCTKPESERMPCTEDYLSLLLDRLCVLHEKTPVSEKVTKCCTESLVDRRPCFSALTPDETYVPKAFDEKLFTFHADLCTLPDTEKELKKETALVELLKHKPKATEEELKTVMEDFVAFVDKCCAADDKEACFAVEGPKLVVSTETALA
#                      CDDTHKSELAHRFKDLGEEHFKGL            PFDEHVKLVDELTEFAKTCVADESHAGCEKSLHTLFGDELCKVASLRETYGDMADCCEKEERDEP
#                                                                                                              MADDGCWEEPER                    
#                                                                                                                    KEEPELDECFLSHKDDSPDLPKLKPDPDTLCDEFKADEKKFWWKYLYELARRH
#                                                                                                                     EEPEDRECFL
#  Alignment # 0: Alignment<position= 22,sequence=sequence=CDDTHKSELAHRFKDLGEEHFKGL, identifier=[6144, 4610, 6148, 5132, 7694, 4114, 3868, 3869, 4897, 5410, 5413, 7718, 5415, 5160, 3292, 5163, 3628, 4147, 5429, 5178, 6715, 3906, 2888, 4169, 4170, 3915, 4173, 5967, 3411, 4878, 3931, 6492, 3936, 8033, 7778, 3683, 5991, 5996, 5995, 5484, 5650, 6512, 6258, 6005, 6006, 5240, 5753, 5755, 6013, 5758, 6017, 5251, 5765, 6279, 3720, 5228, 6538, 6798, 6799, 6800, 5777, 6546, 6294, 6551, 6297, 5018, 3996, 3997, 3231, 673, 6032, 3235, 6054, 6288, 4011, 5811, 5044, 5813, 5814, 5559, 5817, 6075, 3772, 5706, 5310, 4802, 5104, 5409, 6858, 5840, 3743, 5074, 4819, 4311, 4312, 5084, 5598, 6880, 5604, 5414, 5097, 5098, 5159, 7148, 4845, 5102, 3568, 3316, 5118, 6135, 5369, 5114, 5115, 3326], confidence=[39, 80, 96, 99, 99, 99, 98, 99, 100, 99, 99, 95, 95, 96, 99, 99, 98, 100, 100, 99, 99, 99, 96, 98],protein=set(['ALBU_BOVIN']),quality=0.956522>
#  Alignment # 1: Alignment<position= 58,sequence=sequence=PFDEHVKLVDELTEFAKTCVADESHAGCEKSLHTLFGDELCKVASLRETYGDMADCCEKEERDEP, identifier=[8707, 4616, 11273, 10763, 7693, 6670, 11791, 7696, 9747, 13657, 5658, 8732, 6174, 12328, 9257, 5675, 11309, 12637, 5681, 8755, 11832, 14393, 7738, 5691, 7946, 4675, 10822, 7751, 7752, 3148, 10320, 6226, 8787, 7595, 4700, 3679, 11792, 9831, 11367, 10353, 4212, 8309, 7799, 8825, 10363, 8835, 4228, 7813, 6281, 8331, 6284, 13453, 8851, 8852, 3736, 9883, 9925, 10403, 4778, 7851, 6830, 3247, 8373, 8886, 7865, 14016, 3778, 10947, 7876, 10437, 7366, 5831, 3789, 7898, 5071, 8412, 4318, 13221, 6608, 4324, 8423, 5352, 4330, 5361, 8948, 7415, 13561, 12538, 5373, 7637, 7425, 5378, 9475, 13784, 10503, 10504, 8970, 6411, 7948, 11533, 5904, 7441, 12199, 8982, 8471, 8619, 13598, 13601, 3879, 7984, 5938, 5171, 14135, 5434, 6973, 9022, 5439, 8001, 14152, 10380, 13130, 4429, 8845, 7736, 5463, 12120, 7513, 14394, 11621, 4966, 7015, 7528, 12062, 6000, 8052, 11435, 12150, 13177, 4927, 5690, 6534, 11073, 14218, 11666, 7062, 4506, 5022, 7583, 14240, 8097, 11683, 7737, 9638, 7921, 11689, 8107, 12136, 12017, 14268, 12224, 13671, 7625, 10703, 13776, 13777, 13731, 12245, 7929, 4056, 9695, 8796, 4579, 5457, 12711, 7817, 6131, 8183, 7166], confidence=[94, 99, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 99, 100, 100, 100, 99, 99, 99, 99, 99, 99, 100, 99, 99, 97, 82, 90, 97, 97, 97, 96, 97, 99, 99, 99, 99, 99, 100, 100, 100, 100, 99, 99, 98, 98, 97, 99, 97, 99, 99, 100, 99, 97, 97, 97, 91, 98, 89, 90, 96, 79, 87, 78, 43],protein=set(['ALBU_BOVIN']),quality=0.983871>
#  Alignment # 2: Alignment<position=110,sequence=sequence=MADDGCWEEPER, identifier=[4216], confidence=[79, 85, 82, 40, 18, 32, 31, 85, 93, 56, 85, 44],protein=set(['ALBU_BOVIN']),quality=1.000000>
#  Alignment # 3: Alignment<position=116,sequence=sequence=KEEPELDECFLSHKDDSPDLPKLKPDPDTLCDEFKADEKKFWWKYLYELARRH, identifier=[6656, 8705, 6659, 7172, 8711, 6665, 7692, 10766, 6674, 5140, 11285, 6680, 7199, 8226, 6188, 7741, 6722, 7235, 6215, 6221, 3664, 8790, 4696, 6745, 10848, 5218, 4710, 10346, 6503, 4717, 8305, 6039, 4734, 4224, 6280, 10889, 5262, 8301, 7830, 7319, 5274, 7324, 8861, 8350, 4257, 8868, 6823, 6428, 7347, 6836, 8377, 7362, 7877, 7368, 9419, 5327, 7377, 7380, 4822, 8923, 7390, 7398, 10990, 10992, 5874, 5363, 5367, 6442, 9984, 10880, 7426, 7527, 6405, 4358, 7440, 6930, 7452, 8478, 7967, 4898, 7459, 6954, 5931, 7470, 10888, 4869, 6966, 5257, 6535, 7484, 7491, 5956, 8005, 4935, 5452, 5456, 5458, 7511, 7522, 6499, 8039, 4972, 10096, 4977, 6012, 7038, 6529, 6635, 4967, 6021, 6023, 9609, 10634, 3979, 6211, 7063, 5533, 7070, 7071, 8098, 7076, 8614, 8617, 8106, 8007, 7087, 5553, 5875, 5556, 8634, 6076, 8637, 8639, 7114, 8269, 4560, 8659, 6109, 7136, 7137, 4066, 10338, 9195, 7661, 6397, 6646, 5745, 7676, 5546, 7679], confidence=[91, 99, 99, 88, 95, 72, 95, 100, 100, 100, 99, 99, 99, 99, 99, 100, 99, 99, 99, 100, 99, 99, 100, 99, 98, 99, 98, 99, 100, 100, 100, 100, 100, 100, 100, 100, 99, 100, 100, 100, 100, 98, 90, 99, 99, 98, 95, 99, 88, 90, 93, 88, 88],protein=set(['ALBU_BOVIN']),quality=0.980769>

#  Alignment # 4: Alignment<position=117,sequence=sequence=EEPEDRECFL, identifier=[8863], confidence=[96, 96, 60, 82, 73, 59, 89, 93, 99, 98],protein=set(['ALBU_BOVIN']),quality=1.000000>
#  Alignment # 5: Alignment<position=173,sequence=sequence=APELLYYADKEDP, identifier=[9092, 10438, 10278, 9767, 9928, 8044, 8462, 6127, 4159, 6687], confidence=[94, 97, 100, 100, 100, 100, 99, 98, 95, 95, 89, 89, 51],protein=set(['ALBU_BOVIN']),quality=0.916667>
#  Alignment # 6: Alignment<position=185,sequence=sequence=VVFEECCEAEDEK, identifier=[8353, 5666, 6084, 6070, 7846, 6250, 7931, 5754], confidence=[59, 91, 98, 100, 100, 95, 86, 95, 93, 98, 92, 87, 72],protein=set(['ALBU_BOVIN']),quality=0.909091>
#  Alignment # 7: Alignment<position=203,sequence=sequence=KLEMVTWVKLTSSA, identifier=[5944], confidence=[92, 96, 96, 52, 52, 22, 20, 46, 59, 85, 92, 95, 82, 68],protein=set(['ALBU_BOVIN']),quality=1.000000>
#  Alignment # 8: Alignment<position=223,sequence=sequence=ASLEKFEERALAAWSVARLSEKFPKAEFVEVTKLVTDLTKVHKECCHH, identifier=[4097, 9366, 9479, 5899, 9997, 13070, 4495, 6164, 9622, 9625, 5401, 9114, 8220, 6298, 13089, 10021, 4777, 8618, 12459, 5804, 3501, 5808, 3505, 4531, 5941, 10682, 5435, 7742, 6080, 5186, 12099, 4548, 11078, 4552, 6985, 7115, 7381, 6232, 7769, 5981, 5855, 4411, 6145, 6629, 11115, 8640, 8762, 5881, 8445, 9982], confidence=[89, 94, 99, 100, 99, 98, 90, 98, 92, 95, 98, 98, 100, 99, 99, 98, 92, 82, 95, 98, 99, 99, 99, 98, 99, 99, 99, 98, 99, 100, 99, 98, 95, 98, 98, 97, 98, 98, 97, 92, 92, 91, 89, 96, 91, 81, 62, 44],protein=set(['ALBU_BOVIN']),quality=0.956522>
#  Alignment # 9: Alignment<position=227,sequence=sequence=KFLERGLKAWSVLAA, identifier=[9602, 9299, 9300], confidence=[91, 69, 66, 82, 23, 41, 93, 96, 98, 97, 93, 83, 73, 63, 49],protein=set(['ALBU_BOVIN']),quality=1.000000>
#  Alignment #10: Alignment<position=228,sequence=sequence=FGERAL, identifier=[6024], confidence=[93, 85, 93, 68, 90, 95],protein=set(['ALBU_BOVIN']),quality=1.000000>
#  Alignment #11: Alignment<position=236,sequence=sequence=WSVKELKEGFPK, identifier=[8335], confidence=[85, 94, 83, 34, 31, 63, 54, 79, 52, 80, 81, 90],protein=set(['ALBU_BOVIN']),quality=1.000000>
#  Alignment #12: Alignment<position=244,sequence=sequence=KFPAKEFVE, identifier=[7574], confidence=[96, 96, 84, 78, 78, 86, 77, 99, 99],protein=set(['ALBU_BOVIN']),quality=1.000000>
#  Alignment #13: Alignment<position=276,sequence=sequence=ADDRADLAKYLCDDE, identifier=[5890, 9060, 9478, 7980, 9385, 8140, 6700, 8942, 9134, 9015, 8860, 7997, 9141], confidence=[76, 80, 77, 84, 87, 93, 99, 99, 99, 98, 99, 99, 100, 95, 87],protein=set(['ALBU_BOVIN']),quality=1.000000>
#  Alignment #14: Alignment<position=290,sequence=sequence=EETLSSKLKECCDKPLLEKSHCLAEVEKKA, identifier=[4994, 5763, 3983, 5781, 5782, 5978, 5150, 7465, 5418, 6187, 6702, 4787, 5437, 6718, 6207, 5440, 6857, 7626, 4557, 4433, 6870, 5975, 6106, 5725, 4958, 4831, 5732, 6246, 6097, 6891, 4649, 6079, 8958], confidence=[66, 97, 98, 100, 100, 99, 99, 99, 99, 99, 99, 99, 99, 99, 100, 100, 100, 100, 98, 99, 99, 100, 100, 100, 100, 99, 100, 98, 93, 82],protein=set(['ALBU_BOVIN']),quality=0.931034>
#  Alignment #15: Alignment<position=318,sequence=sequence=LAPEDALPPLTADFAEDKDVCKDYEEAKDAFLG, identifier=[11013, 6086, 5064, 3980, 8591, 5788, 10358, 7974, 8294, 10343, 10665, 5116, 10609, 4040, 3126, 13623, 8505, 4859, 5372, 5119], confidence=[71, 62, 57, 74, 68, 79, 98, 98, 99, 99, 98, 98, 99, 99, 100, 99, 99, 96, 99, 99, 99, 98, 93, 97, 98, 99, 97, 95, 92, 81, 90, 78, 41],protein=set(['ALBU_BOVIN']),quality=1.000000>
#  Alignment #16: Alignment<position=350,sequence=sequence=GSFLYEYSRR, identifier=[7416, 8584, 7618], confidence=[70, 86, 96, 99, 99, 99, 88, 93, 85, 94],protein=set(['ALBU_BOVIN']),quality=1.000000>
#  Alignment #17: Alignment<position=354,sequence=sequence=YEYDKRHPEY, identifier=[4044], confidence=[90, 96, 82, 68, 62, 38, 82, 92, 98, 95],protein=set(['ALBU_BOVIN']),quality=1.000000>
#  Alignment #18: Alignment<position=364,sequence=sequence=ALSVLLRLAKEYEATLEECCAKDD, identifier=[14756, 14775, 6436, 7514, 8087, 5751], confidence=[15, 22, 38, 38, 54, 63, 32, 57, 43, 43, 77, 72, 96, 98, 98, 99, 100, 99, 97, 92, 92, 91, 83, 79],protein=set(['ALBU_BOVIN']),quality=1.000000>
#  Alignment #19: Alignment<position=386,sequence=sequence=DDPHACYSTVFDKLKHLVDE, identifier=[7682, 12227, 9956, 9254, 12399, 8439, 7706, 11806, 11263], confidence=[91, 95, 97, 98, 97, 86, 89, 98, 99, 98, 98, 97, 96, 94, 97, 95, 98, 99, 98, 98],protein=set(['ALBU_BOVIN']),quality=1.000000>
#  Alignment #20: Alignment<position=405,sequence=sequence=EPEDLLKEDCDEFEKLGEYG, identifier=[6723, 8071, 8585, 6220, 6671, 8465, 8271, 5405, 9379, 7590, 5353, 8881, 5747, 5685, 6313, 7864, 5882], confidence=[99, 97, 99, 96, 98, 100, 98, 99, 94, 98, 99, 100, 99, 99, 96, 95, 83, 99, 96, 28],protein=set(['ALBU_BOVIN']),quality=1.000000>
#  Alignment #21: Alignment<position=422,sequence=sequence=EYGFEDALLVRYTRKVPEVSTPTLVEVSRSLGKVGVT, identifier=[5634, 11012, 8200, 8457, 6412, 9230, 11279, 10771, 10776, 8090, 7067, 9503, 9888, 8667, 10148, 10917, 9511, 11180, 9352, 8372, 6581, 4408, 10810, 10044, 8765, 8384, 8772, 7240, 9068, 9165, 8398, 10705, 6613, 8280, 8356, 11739, 7260, 12392, 10492, 10092, 9966, 10105, 7804], confidence=[89, 85, 96, 98, 100, 99, 99, 100, 99, 97, 93, 95, 89, 92, 82, 95, 95, 98, 93, 97, 99, 99, 100, 100, 100, 100, 99, 97, 97, 98, 97, 93, 97, 96, 87, 45, 32],protein=set(['ALBU_BOVIN']),quality=1.000000>
#  Alignment #22: Alignment<position=429,sequence=sequence=LLFTLWKKVPE, identifier=[6038], confidence=[90, 90, 69, 35, 44, 30, 33, 67, 90, 94, 98],protein=set(['ALBU_BOVIN']),quality=1.000000>
#  Alignment #23: Alignment<position=445,sequence=sequence=LVEVFKAPGKVG, identifier=[7164], confidence=[81, 82, 98, 88, 29, 25, 22, 29, 74, 90, 87, 65],protein=set(['ALBU_BOVIN']),quality=1.000000>
#  Alignment #24: Alignment<position=463,sequence=sequence=PESERMCPTED, identifier=[5087], confidence=[80, 95, 70, 70, 39, 75, 65, 51, 94, 99, 97],protein=set(['ALBU_BOVIN']),quality=1.000000>
#  Alignment #25: Alignment<position=474,sequence=sequence=YLSLLLRLD, identifier=[13081], confidence=[84, 94, 97, 98, 95, 90, 30, 63, 54],protein=set(['ALBU_BOVIN']),quality=1.000000>
#  Alignment #26: Alignment<position=484,sequence=sequence=VLHEKTPVSEKVTKCCTESLVDRTLDTT, identifier=[4736, 4039, 4041, 4362, 5009, 4564, 5014, 7768, 4254, 6688, 5473, 5026, 5222, 7783, 5932, 5357, 5358, 5489, 5362, 7092, 7262, 7604, 4603, 7285], confidence=[89, 95, 95, 95, 98, 99, 99, 100, 99, 99, 98, 98, 97, 99, 99, 99, 99, 100, 98, 96, 85, 84, 79, 69, 75, 70, 70, 82],protein=set(['ALBU_BOVIN']),quality=0.956522>
#  Alignment #27: Alignment<position=511,sequence=sequence=TGLTPDETYVPKAFDEKLFTFHAD, identifier=[7808, 6914, 10539, 9734, 7689, 8458, 8846, 10767, 8978, 9753, 5915, 11420, 6558, 4127, 8319, 9637, 9766, 11975, 8364, 6317, 11183, 5425, 7858, 9182, 11964, 8519, 11595, 8526, 7635, 11733, 10073, 11738, 11102, 11744, 11491, 7014, 7782, 10729, 10091, 11120, 8038, 11387, 11391], confidence=[76, 63, 96, 99, 99, 100, 100, 100, 100, 100, 99, 99, 100, 99, 100, 100, 99, 100, 99, 99, 100, 99, 98, 98],protein=set(['ALBU_BOVIN']),quality=1.000000>
#  Alignment #28: Alignment<position=534,sequence=sequence=ELCTLPDTEKELKKETALVELLKHKEKATEEELKTVMED, identifier=[6149, 5129, 7703, 6162, 5015, 6937, 3482, 7835, 7966, 5023, 4400, 7970, 6563, 8357, 7975, 9130, 4016, 6105, 7348, 4405, 5943, 7096, 5821, 4030, 9653, 7495, 12617, 4151, 8141, 4814, 8275, 8149, 9049, 8154, 7647, 4067, 10597, 8167, 8808, 6695, 7916, 8176, 8168, 7668, 7079, 4348], confidence=[77, 96, 97, 99, 100, 96, 93, 86, 97, 87, 95, 96, 96, 96, 98, 98, 99, 100, 99, 100, 99, 100, 97, 94, 97, 89, 90, 94, 96, 99, 100, 100, 99, 97, 87, 93, 98, 99, 90],protein=set(['ALBU_BOVIN']),quality=0.973684>
