import tempfile
import filecmp
import unittest

import os

from dips.io import PeptideReader
from dips.data import Peptide,KmerContig, Kmer
from dips.assemble import KmersHandler, KmerAssembler
from nose.plugins.attrib import attr
from nose.tools.nontrivial import nottest


@attr(test_type='unit')
class KmerContigTest(unittest.TestCase):
    """
    try building this kmer: DLLAEKE
    """
    def test_kmer_build(self):
        kmer_handler = KmersHandler(7)
        peptides = PeptideReader(open('test-input/kmer_contig_peptides1.csv','r')).read()
        kmers = kmer_handler.get_kmers(peptides)
        kmer = [kmer for kmer in kmers if kmer.kmer=='DLLAEKE'][0]
        self.assertEquals(kmer.num_sequences, 6)
        self.assertEquals(kmer.confidence_score, [100,100,100,100,100,100,90])

    """
    try building this kmer contig: LPLDPVAGYKEPA
    """
    def test_kmer_contig_add_kmer(self):
        kmer_contig = self._create_kmer_contig(1)
        print kmer_contig.confidence
        self.assertEquals(kmer_contig.sequence, "ABCDEFGHIJKLMD")
        self.assertEquals(kmer_contig.confidence, [90, 80, 90, 85, 85, 85, 90, 90, 95, 85, 85, 80, 80, 90])


    """
    """
    def test_kmer_contig_add_contig(self):
        kmer_contig1 = self._create_kmer_contig(1)
        # print '=====contig1====='
        # for overlapping_kmer in kmer_contig1.overlapping_kmers:
        #     print overlapping_kmer.sequence
        #     print overlapping_kmer.before
        #     print overlapping_kmer.after
        kmer_contig2 = self._create_kmer_contig(2)
        # print '=====contig2====='
        # for overlapping_kmer in kmer_contig2.overlapping_kmers:
        #     print overlapping_kmer.sequence
        #     print overlapping_kmer.before
        #     print overlapping_kmer.after
        kmer_contig1.add_contig(kmer_contig2, len(kmer_contig2.sequence)-4)
        kmer_contig1.update_confidence()
        print '========='
        for i in range(len(kmer_contig1.sequence)):
            print chr(kmer_contig1.sequence[i])+": "+str(kmer_contig1.confidence[i])
        print kmer_contig1.sequence
        print kmer_contig1.confidence
        kmers = sorted([overlapping_kmer.sequence.kmer for overlapping_kmer in kmer_contig1.overlapping_kmers])
        print kmers
        self.assertEquals(kmers, ['ABCD', 'BCDE', 'CDEF', 'DEFG', 'ERST', 'FGHI', 'GHIJ', 'HIJK', 'IJKL', 'JKLM', 'KLMD',
                                  'KLMD', 'LMDO', 'MDOP', 'OPER', 'PERS', 'RSTU', 'STUV', 'TUVW', 'UVWX', 'VWXY', 'WXYZ'])
        self.assertEquals(kmer_contig1.sequence, "ABCDEFGHIJKLMDOPERSTUVWXYZ")
        self.assertEquals(kmer_contig1.confidence, [90.0, 80.0, 90.0, 85.0, 85.0, 85.0, 90.0, 90.0, 95.0, 85.0,
                                                         86.66666666666667, 80.0, 80.0, 80.0, 95.0, 80.0, 80.0, 85.0,
                                                         93.33333333333333, 80.0, 76.66666666666667, 85.0, 87.5, 95.0,
                                                         80.0, 50.0])

    """
    """
    def test_kmer_contig_cut_edge(self):
        kmer_contig1 = self._create_kmer_contig(1)
        kmer_contig1.cut_edge(-2)
        print kmer_contig1.sequence
        print kmer_contig1.confidence
        kmers = [overlapping_kmer.sequence.kmer for overlapping_kmer in kmer_contig1.overlapping_kmers]
        for overlapping_kmer in kmer_contig1.overlapping_kmers:
            for overlapping_sequence in overlapping_kmer.sequence.overlapping_sequences:
                for i in range(0,overlapping_kmer.after-overlapping_kmer.before):
                    # print overlapping_sequence.sequence, overlapping_sequence.before, overlapping_kmer.kmer_offset, i
                    assert chr(kmer_contig1.sequence[overlapping_kmer.before+i])!=overlapping_sequence.sequence.sequence
                    [overlapping_sequence.before+overlapping_kmer.kmer_offset+i], "bases does not match"
        print sorted(kmers)
        self.assertEquals(sorted(kmers),sorted(['ABCD', 'BCDE', 'CDEF', 'DEFG', 'FGHI', 'GHIJ', 'HIJK', 'IJKL', 'JKLM', 'KLMD']))
        self.assertEquals(kmer_contig1.sequence, "CDEFGHIJKLMD")
        self.assertEquals(kmer_contig1.confidence, [90.0, 85.0, 85.0, 85.0, 90.0, 90.0, 95.0, 85.0, 85.0, 80.0, 80.0, 90.0])

    """
    """
    def test_kmer_contig_swap_residues(self):
        kmer_contig1 = self._create_kmer_contig(2)
        kmer_contig1.swap_residues(0,1)
        self.assertEquals(kmer_contig1.sequence, "LKMDOPERSTUVWXYZ")
        self.assertEquals(kmer_contig1.confidence, [90.0, 80.0, 80.0, 70.0, 95.0, 80.0, 80.0, 85.0, 93.33333333333333, 80.0,
                                                         76.66666666666667, 85.0, 87.5, 95.0, 80.0, 50.0])

    def test_kmer_contig_absorb(self):
        kmer_contig1 = self._create_kmer_contig(1)
        kmer_contig2 = self._create_kmer_contig(3)
        print kmer_contig2.peptides
        kmer_contig1.absorb_contig(kmer_contig2,2)
        kmer_contig1.update_confidence()
        print kmer_contig1.confidence
        self.assertEquals(kmer_contig1.sequence, "ABCDEFGHIJKLMD")
        self.assertEquals(kmer_contig1.get_variants(3), {'D':7})
        self.assertEquals(kmer_contig1.get_variants(5), {'F':4,'G':3})
        self.assertEquals(kmer_contig1.get_variants(6), {'F':3,'G':4})
        self.assertEquals(kmer_contig1.confidence, [90, 80, 90, 86.66666666666667,86.66666666666667, 90, 90,
                                                    90, 95, 87.5, 85, 75, 75, 90])

    def test_kmer_contig_complex(self):
        # TODO: add this test (add contig, cut, swap all together..)
        pass

    def _create_kmer_contig(self, i):
        kmer_handler = KmersHandler(4)
        kmer_contig = None
        if(i==1):
            peptides = [Peptide(1, 'ABCDE', [90, 80, 80, 70, 90]),Peptide(1, 'ABCDE', [90, 80, 80, 70, 90]),
                        Peptide(2, 'CDEFG', [100, 100, 80, 80, 90]),Peptide(2, 'CDEFG', [100, 100, 80, 80, 90]),
                        Peptide(3, 'FGHIJK', [90, 90, 90, 90, 90, 90]), Peptide(3, 'FGHIJK', [90, 90, 90, 90, 90, 90]),
                        Peptide(4, 'IJKLMD', [100, 80, 80, 80, 80, 90]), Peptide(4, 'IJKLMD', [100, 80, 80, 80, 80, 90])]
            kmers = kmer_handler.get_kmers(peptides)
            print [kmer.kmer for kmer in kmers]
            contigs = kmer_handler.assemble_kmers(kmers, min_overlap=2)
            print len(contigs)
            kmer_contig = contigs[0]
        elif(i==2):
            peptides = [Peptide(5, 'KLMDOP', [90, 80, 80, 70, 90, 100]), Peptide(5, 'KLMDOP', [90, 80, 80, 70, 90, 100]),
                        Peptide(6, 'OPERSTU', [100, 60, 80, 80, 90, 70, 60]),Peptide(6, 'OPERSTU', [100, 60, 80, 80, 90, 70, 60]),
                        Peptide(7, 'RSTUVWXY', [90, 90, 90, 90, 90, 90, 100, 100]),Peptide(7, 'RSTUVWXY', [90, 90, 90, 90, 90, 90, 100, 100]),
                        Peptide(8, 'STUVWXYZ', [100, 80, 80, 80, 85, 90, 60, 50]), Peptide(8, 'STUVWXYZ', [100, 80, 80, 80, 85, 90, 60, 50])]
            kmers = kmer_handler.get_kmers(peptides)
            contigs = kmer_handler.assemble_kmers(kmers, min_overlap=2)
            kmer_contig = contigs[0]
        elif(i==3):
            peptides = [Peptide(9, 'CDEGF', [90, 90, 90, 100, 80]), Peptide(9, 'CDEGF', [90, 90, 90, 100, 80]),
                        Peptide(10, 'GFHIJK', [90, 100, 90, 100, 90, 80]),Peptide(10, 'GFHIJK', [90, 100, 90, 100, 90, 80]),
                        Peptide(11, 'IJKLM', [90, 90, 90, 70, 70]), Peptide(11, 'IJKLM', [90, 90, 90, 70, 70])]
            kmers = kmer_handler.get_kmers(peptides)
            print [kmer.kmer for kmer in kmers]
            contigs = kmer_handler.assemble_kmers(kmers, min_overlap=2)
            print len(contigs)
            kmer_contig = contigs[0]
        return kmer_contig


# FETUIN_ACID_TEST_SET = 'test-input/test_Fetuin_Rep3_Acid.csv'
# FETUIN_TRYPSIN_TEST_SET = 'test-input/test_Fetuin_Trypsin.csv'
# FETUIN_EXPECTED_TEST_OUTPUT_DIR = 'test-input/fetuin_regress1.0'
#
#
# @attr(test_type='unit')
# class KmerAssemblerTest(unittest.TestCase):
#     def test_regress_assembler(self):
#         # run kmer assembler:
#         temp_output_dir = tempfile.mkdtemp(prefix='kmer-assembler-test-output.')
#         print(temp_output_dir)
#         KmerAssembler('all', [FETUIN_ACID_TEST_SET], [FETUIN_TRYPSIN_TEST_SET], temp_output_dir, 7, 5, 5, 7, 0.7, min_peptides_distribution=3).assemble()
#         # check output:
#         self.assertTrue(filecmp.cmp(os.path.join(temp_output_dir, 'cleaned_peptides.csv'), os.path.join(FETUIN_EXPECTED_TEST_OUTPUT_DIR, 'cleaned_peptides.csv'), shallow=False))
#         self.assertTrue(filecmp.cmp(os.path.join(temp_output_dir, 'kmers.csv'), os.path.join(FETUIN_EXPECTED_TEST_OUTPUT_DIR, 'kmers.csv'), shallow=False))
#         self.assertTrue(filecmp.cmp(os.path.join(temp_output_dir, 'contigs.csv'), os.path.join(FETUIN_EXPECTED_TEST_OUTPUT_DIR, 'contigs.csv'), shallow=False))
#         self.assertTrue(filecmp.cmp(os.path.join(temp_output_dir, 'merged_contigs.csv'), os.path.join(FETUIN_EXPECTED_TEST_OUTPUT_DIR, 'merged_contigs.csv'), shallow=False))
#         self.assertTrue(filecmp.cmp(os.path.join(temp_output_dir, 'united_contigs.csv'), os.path.join(FETUIN_EXPECTED_TEST_OUTPUT_DIR, 'united_contigs.csv'), shallow=False))
#         self.assertTrue(filecmp.cmp(os.path.join(temp_output_dir, 'filtered_united_contigs.csv'), os.path.join(FETUIN_EXPECTED_TEST_OUTPUT_DIR, 'filtered_united_contigs.csv'), shallow=False))
#         self.assertTrue(filecmp.cmp(os.path.join(temp_output_dir, 'final_contigs.csv'), os.path.join(FETUIN_EXPECTED_TEST_OUTPUT_DIR, 'final_contigs.csv'), shallow=False))


if __name__ == '__main__':
    pass