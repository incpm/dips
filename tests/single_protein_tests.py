# import abc
# import logging
# import os
# import unittest
# 
# from nose.plugins.attrib import attr
# from nose.tools import nottest
# 
# from dips.align import NaiveAligner
# from dips.assemble import OnePassAssembler, MultiPassAssembler, StubAssembler
# from dips.downsample import DownSampler
# from dips.io import PeptideReader
# from dips import settings
# 
# from .utils import TEST_INPUT_DIR
# from dips.proteins import SINGLE_PROTEIN
# 
# logger = logging.getLogger('dips.' + __name__)
# 
# class AssemblyTest(unittest.TestCase):
#     metaclass = abc.ABCMeta
# 
#     def setUp(self):
#         peptide_reader = PeptideReader([open(csv_filename, 'r') for csv_filename in self.input_filenames])
#         self.peptides = peptide_reader.read()
#         self.peptides = DownSampler().downsample(sequences=self.peptides)
# 
#     def _assembly(self, assembler_class):
#         assembler = assembler_class()
#         assembled_sequences = assembler.assemble(sequences=self.peptides)
#         self._assert_assembled_sequences(assembled_sequences)
#         return assembled_sequences
# 
#     def _classify_assembled_sequences(self, assembled_sequences):
#         aligner = NaiveAligner(proteins=self.proteins, stop_on_first_alignment=False)
#         peptides = [sequence for sequence in assembled_sequences if len(sequence.identifier) == 1]
#         contigs = [sequence for sequence in assembled_sequences if len(sequence.identifier) > 1]
#         contig_alignments, unaligned_contigs = aligner.align(contigs)
#         contig_alignments.sort(key=lambda x: (x.position, -x.quality))
#         peptide_alignments, unaligned_peptides = aligner.align(peptides)
#         peptide_alignments.sort(key=lambda x: (x.position, -x.quality))
#         return (contigs, contig_alignments, unaligned_contigs, peptides, peptide_alignments, unaligned_peptides)
# 
#     def _get_coverage_stats(self, protein, alignments):
#         coverage = [0] * len(protein)
#         for alignment in alignments:
#             for i in range(max(0, alignment.position), min(alignment.position+len(alignment), len(coverage))):
#                 coverage[i] += 1
#         num_covered_acids = sum([1 for c in coverage if c > 0])
#         return (coverage, num_covered_acids)
# 
#     def _assert_assembled_sequences(self, assembled_sequences):
#         (contigs, contig_alignments, unaligned_contigs, peptides, peptide_alignments, unaligned_peptides) = self._classify_assembled_sequences()
#         logger.info('Summary:')
#         logger.info('Proteins: %s' %self.proteins)
#         logger.info('Number of assembled sequences (including peptides which were not assembled): %d' %len(assembled_sequences))
#         logger.info('Number of contigs (aligned/unaligned): %d (%d/%d)' %(len(contigs), len(contig_alignments), len(unaligned_contigs)))
#         logger.info('Number of peptides which were not assembled (aligned/unaligned): %d (%d/%d)' %(len(peptides), len(peptide_alignments), len(unaligned_peptides)))
#         for i,unaligned_contig in enumerate(unaligned_contigs):
#             logger.info('Unaligned contig #%2d: %s' %(i, unaligned_contig))
#         for i,unaligned_peptide in enumerate(unaligned_peptides):
#             logger.info('Unaligned peptide #%2d: %s' %(i, unaligned_peptide))
#         for protein in self.proteins:
#             protein_contig_alignments = [a for a in contig_alignments if a.protein == protein]
#             protein_peptide_alignments = [a for a in peptide_alignments if a.protein == protein]
#             logger.info('~~~~~~~~')
#             logger.info('Protein: %s' %protein)
#             logger.info('Number of aligned contigs: %d' %len(protein_contig_alignments))
#             for i,protein_contig_alignment in enumerate(protein_contig_alignments):
#                 logger.info('Contig alignment #%2d: %s' %(i, protein_contig_alignment))
#             logger.info('Number of aligned peptides: %d' %len(protein_peptide_alignments))
#             for i,protein_peptide_alignment in enumerate(protein_peptide_alignments):
#                 logger.info('Peptide alignment #%2d: %s' %(i, protein_peptide_alignment))
#             coverage, num_covered_acids = self._get_coverage_stats(protein, protein_contig_alignments + protein_peptide_alignments) # take both contig and peptide alignments
#             logger.info('Coverage (assuming all alignments are correct): %d of %d amino acids (%.2f)' %(num_covered_acids, len(protein), float(num_covered_acids) / len(protein)))
#             logger.info("Detailed coverage: %s" %coverage)
# 
#     def _log_configuration(self):
#         logger.info('Configuration:')
#         logger.info('~~~~~~~~~~~~~~')
#         for param_name in sorted(dir(settings)):
#             if param_name.startswith('_'):
#                 continue
#             logger.info('%s=%s' %(param_name, eval('settings.' + param_name)))
#         logger.info('')
# 
# @attr(test_type='unit')
# class SingleContigAssemblyTest(AssemblyTest):
#     '''
#     A sequence of length 24 amino acids is assembled from 114 (!) peptides.
#     '''
#     input_filenames = [os.path.join(TEST_INPUT_DIR, 'de_novo_peptides.csv')]
#     proteins = SINGLE_PROTEIN
#     selected_identifiers = [6144, 4610, 6148, 5132, 7694, 4114, 3868, 3869, 4897, 5410,
#                             5413, 7718, 5415, 5160, 3292, 5163, 3628, 4147, 5429, 5178,
#                             6715, 3906, 2888, 4169, 4170, 3915, 4173, 5967, 3411, 4878,
#                             3931, 6492, 3936, 8033, 7778, 3683, 5991, 5996, 5995, 5484,
#                             5650, 6512, 6258, 6005, 6006, 5240, 5753, 5755, 6013, 5758,
#                             6017, 5251, 5765, 6279, 3720, 5228, 6538, 6798, 6799, 6800,
#                             5777, 6546, 6294, 6551, 6297, 5018, 3996, 3997, 3231, 673,
#                             6032, 3235, 6054, 6288, 4011, 5811, 5044, 5813, 5814, 5559,
#                             5817, 6075, 3772, 5706, 5310, 4802, 5104, 5409, 6858, 5840,
#                             3743, 5074, 4819, 4311, 4312, 5084, 5598, 6880, 5604, 5414,
#                             5097, 5098, 5159, 7148, 4845, 5102, 3568, 3316, 5118, 6135,
#                             5369, 5114, 5115, 3326]
# 
#     def setUp(self):
#         super(SingleContigAssemblyTest, self).setUp()
#         self.peptides = [peptide for peptide in self.peptides if list(peptide.identifier)[0] in self.selected_identifiers]
#         self.assertEquals(len(self.selected_identifiers), len(self.peptides))
# 
#     @nottest
#     def test_single_contig_assembly(self):
#         logger.info('test_single_contig_assembly')
#         assembled_sequences = self._assembly(assembler_class=OnePassAssembler)
#         self.assertTrue(len(assembled_sequences) < 3)
# #         contig = assembled_sequences.pop()
# #         overlapping_sequences = contig.overlapping_sequences
# #         self.assertEquals(overlapping_sequences, None)
# #         self.assertEquals(contig.sequence, 'RRDTHKSELAHRFKDLGEEHFKGL')
# 
# @nottest
# @attr(test_type='slow')
# class SingleProteinAssemblyTest(AssemblyTest):
# #     input_filenames = [os.path.join(TEST_INPUT_DIR, 'de_novo_peptides.csv')]
# #     input_filenames = [os.path.join(TEST_INPUT_DIR, filename) for filename in ('de_novo_peptides.csv', 'de_novo_peptides_BSA_Trypsin_Carb_Removed.csv')]
#     input_filenames = [os.path.join(TEST_INPUT_DIR, filename) for filename in ('All_BSA_de_novo_peptides_TimeLine_Series_301114.csv',)]
#     proteins = SINGLE_PROTEIN
# 
#     @nottest
#     def test_stub_assembly(self):
#         logger.info('test_stub_assembly')
#         self._assembly(assembler_class=StubAssembler)
# 
#     @nottest
#     def test_one_pass_assembly(self):
#         logger.info('test_one_pass_assembly')
#         self._assembly(assembler_class=OnePassAssembler)
# 
#     @nottest
#     def test_multi_pass_assembly(self):
#         logger.info('test_multi_pass_assembly')
#         self._assembly(assembler_class=MultiPassAssembler)
# 
#     def test_one_pass_assembly_versus_stub(self):
#         self._log_configuration()
# #         assembled_sequences = OnePassAssembler(sequences=self.peptides).assemble()
#         assembler = MultiPassAssembler()
#         assembled_sequences = assembler.assemble(sequences=self.peptides)
#         (contigs, contig_alignments, unaligned_contigs, peptides, peptide_alignments, unaligned_peptides) = self._classify_assembled_sequences(assembled_sequences) # @UnusedVariable
#         average_num_peptides_per_aligned_contig, average_aligned_contig_length = self._contig_stats(contig_alignments)
#         min_overlap, max_overlap, avg_overlap = self._get_overlap_stats(contig_alignments)
#         logger.info('Summary - assembly using %s:' %assembler.__class__.__name__)
#         logger.info('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
#         logger.info('Proteins: %s' %[protein.identifier for protein in self.proteins])
#         logger.info('Assembly: #sequences before assembly: %d #sequences after assembly: %d #contigs: %d #peptides: %d'
#                     %(len(self.peptides), len(assembled_sequences), len(contigs), len(peptides))) 
#         logger.info('Contigs: #total: %d #aligned: %d #unaligned: %d'
#                     %(len(contigs), len(contig_alignments), len(unaligned_contigs)))
#         logger.info('Aligned contigs: Average #peptides per contig: %d Average length: %d Min overlap: %d Max overlap: %d Average overlap: %d'
#                     %(average_num_peptides_per_aligned_contig, average_aligned_contig_length, min_overlap, max_overlap, avg_overlap))
#         assert len(self.proteins) == 1 # currently only look at coverage of a single protein
#         protein = self.proteins[0]
#         coverage, num_covered_acids = self._get_coverage_stats(protein, contig_alignments) # @UnusedVariable
#         logger.info('Coverage (assuming all alignments are correct): %d of %d amino acids (%.2f)' %(num_covered_acids, len(protein), float(num_covered_acids) / len(protein)))
#         for i,contig_alignment in enumerate(contig_alignments):
#             logger.debug('Contig alignment #%2d: %s' %(i, contig_alignment))
#         for i,unaligned_contig in enumerate(unaligned_contigs):
#             logger.debug('Unaligned contig #%2d: %s' %(i, unaligned_contig))
#         logger.info('')
#         # without assembly
#         assembled_sequences = StubAssembler(sequences=self.peptides).assemble()
#         (contigs, contig_alignments, unaligned_contigs, peptides, peptide_alignments, unaligned_peptides) = self._classify_assembled_sequences(assembled_sequences)
#         logger.info('Summary - alignment without assembly:')
#         logger.info('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
#         logger.info('Proteins: %s' %[protein.identifier for protein in self.proteins])
#         assert len(contigs) == 0
#         assert len(peptides) == len(assembled_sequences)
#         logger.info('Assembly: #sequences before/after assembly: %d' %(len(self.peptides),)) 
#         logger.info('Peptides: #total: %d #aligned: %d #unaligned: %d'
#                     %(len(peptides), len(peptide_alignments), len(unaligned_peptides)))
#         assert len(self.proteins) == 1 # currently only look at coverage of a single protein
#         protein = self.proteins[0]
#         coverage, num_covered_acids = self._get_coverage_stats(protein, peptide_alignments) # @UnusedVariable
#         logger.info('Coverage (assuming all alignments are correct): %d of %d amino acids (%.2f)' %(num_covered_acids, len(protein), float(num_covered_acids) / len(protein)))
