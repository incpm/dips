from collections import namedtuple
import errno
from jinja2 import Template
import logging.config
import os
import re
from shutil import copytree, rmtree
from dips.data import TRYPSIN_TYPE, ACID_TYPE, AMINO_ACID_REPLACEMENTS, MODIFICATION

logger = logging.getLogger(__name__)

REPROTS_DIR = os.path.join(os.path.dirname(__file__), 'reports')
RESOURCES_DIR = os.path.join(REPROTS_DIR, 'resources')
REPLACEMENT_CLASS = 'replacement'
COLLISION_CLASS = 'collision'
NO_COLOR_YET = 'NO_COLOR'
NUMBER_OF_ACIDS_PER_LINE = 60

Graph = namedtuple('Graph', field_names=('id', 'x_axis', 'y_axis'))


class ContigsReporter(object):
    def __init__(self, kmer_contigs, contigs_replacements, contigs_variants_collisions, output_dir,
                 acid_input_files_paths, trypsin_input_files_paths, kmer_size, kmer_min_overlap, unite_min_overlap,
                 unite_min_extension, merge_min_quality, report_title, show_trypsin=None):
        self.output_dir = output_dir
        self.resources_dir = os.path.join(self.output_dir, "resources")
        self.kmer_contigs = kmer_contigs
        self.contigs_replacements = contigs_replacements
        self.contigs_variants_collisions = contigs_variants_collisions
        self.acid_input_files_paths = acid_input_files_paths if acid_input_files_paths is not None else []
        self.trypsin_input_files_paths = trypsin_input_files_paths if trypsin_input_files_paths is not None else []
        self.kmer_size = kmer_size
        self.kmer_min_overlap = kmer_min_overlap
        self.unite_min_overlap = unite_min_overlap
        self.unite_min_extension = unite_min_extension
        self.merge_min_quality = merge_min_quality
        self.report_title = report_title
        self.show_trypsin = len(self.trypsin_input_files_paths) > 0 if show_trypsin is None else show_trypsin
        self.rep_explain = "[acid|trypsin]" if self.show_trypsin else "[acid]"
        self._init_report()

    @staticmethod
    def chunk_html_string(string, length):
        counter = 0
        chunks = []
        chunk = ""
        in_html = False
        for c in string:
            chunk += c
            if (c == '<'):
                in_html = True
                continue
            if (c == '>'):
                in_html = False
                continue
            if (in_html):
                continue
            counter += 1
            if counter == length:
                chunks.append(chunk)
                counter = 0
                chunk = ""
        if counter != length:
            chunks.append(chunk)
        return chunks

    def _add_sequence_replacements_and_collisions(self, sequence, contig_replacements, contig_variants_collisions):
        new_sequence = ""
        counter = 0
        for (i, c) in enumerate(sequence):
            # handle replacements
            if len(contig_replacements[i]) > 0:
                e_d = contig_replacements[i][0]
                q_n = [acid_rep_key for acid_rep_key in AMINO_ACID_REPLACEMENTS if AMINO_ACID_REPLACEMENTS[acid_rep_key]==e_d][0]
                e_d_acid = contig_replacements[i][1][e_d] if e_d in contig_replacements[i][1] else 0
                q_n_acid = contig_replacements[i][1][q_n] if q_n in contig_replacements[i][1] else 0
                # for E* modification
                mod_acid = ',E*:'+str(contig_replacements[i][1][MODIFICATION]) \
                    if MODIFICATION in contig_replacements[i][1] and i == 0 else ''
                e_d_trypsin = contig_replacements[i][2][e_d] if e_d in contig_replacements[i][2] else 0
                q_n_trypsin = contig_replacements[i][2][q_n] if q_n in contig_replacements[i][2] else 0
                # for E* modification
                mod_trypsin = ',E*:'+str(contig_replacements[i][2][MODIFICATION]) \
                    if MODIFICATION in contig_replacements[i][2] and i == 0 else ''
                if self.show_trypsin:
                    new_sequence += "[(" + str(counter) + "," + REPLACEMENT_CLASS + "," + str(i) + ")" + e_d + ":" + \
                                    str(e_d_acid) + "," + q_n + ":" + str(q_n_acid) + mod_acid + "|" + e_d + ":" + str(e_d_trypsin) + \
                                    "," + q_n + ":" + str(q_n_trypsin) + mod_trypsin +"]"
                else:
                    new_sequence += "[(" + str(counter) + "," + REPLACEMENT_CLASS + "," + str(i) + ")" + e_d + ":" + \
                                    str(e_d_acid) + "," + q_n + ":" + str(q_n_acid) + mod_acid + "]"
                counter += 1
            elif len(contig_variants_collisions[i]) > 1:
                new_sequence += "[(" + str(counter) + "," + COLLISION_CLASS + "," + str(i) + ")" + \
                                contig_variants_collisions[i][0][0] + ":" + \
                                str(contig_variants_collisions[i][0][1]) + "," + contig_variants_collisions[i][1][
                                    0] + ":" + \
                                str(contig_variants_collisions[i][1][1]) + "]"
                counter += 1
            else:
                new_sequence += "{" + str(i) + "," + c + "}"
        return new_sequence

    def _get_adjusted_seq(self, kmer_contig, contig_replacements, contig_variants_collisions):
        sequence_with_replacements_and_collisions = self._add_sequence_replacements_and_collisions(
            str(kmer_contig.sequence),
            contig_replacements, contig_variants_collisions)
        colored_html_sequence = re.sub("\[\(([^\)]*),([^\)]*),([^\)]*)\)([^\]]*)\]", "<font  class='" +
                                       kmer_contig.single_identifier +
                                       "_\\1 \\2' onmouseover='light_acids(true, this);show_index(\\3, \"" + kmer_contig.single_identifier +
                                       "\")' onmouseout='light_acids(false, this);show_index(0, \"" + kmer_contig.single_identifier + "\")'>[\\4]</font>",
                                       sequence_with_replacements_and_collisions)
        colored_html_sequence = re.sub("\{([^\}]*),([^\}]*)\}",
                                       "<font onmouseover='show_index(\\1, \"" + kmer_contig.single_identifier +
                                       "\")' onmouseout='show_index(0, \"" + kmer_contig.single_identifier + "\")'>\\2</font>",
                                       colored_html_sequence)
        chunked_colored_html_sequence = self.chunk_html_string(colored_html_sequence, NUMBER_OF_ACIDS_PER_LINE)
        return chunked_colored_html_sequence

    @staticmethod
    def _decide_sequence_replacements_and_collisions(sequence, contig_replacements, contig_variants_collisions,
                                                     min_q_acid_ratio=0.025, min_n_acid_ratio=0.09,
                                                     min_n_trypsin_ratio=4, min_collision_ratio=0.7):
        new_sequence = ""
        reps_and_collisions_probabilities = []
        counter = 0
        for (i, c) in enumerate(sequence):
            # handle replacements
            if len(contig_replacements[i]) > 0:
                e_d = contig_replacements[i][0]
                q_n = [acid_rep_key for acid_rep_key in AMINO_ACID_REPLACEMENTS if AMINO_ACID_REPLACEMENTS[acid_rep_key]==e_d][0]
                e_d_acid = contig_replacements[i][1][e_d] if e_d in contig_replacements[i][1] else 0
                q_n_acid = contig_replacements[i][1][q_n] if q_n in contig_replacements[i][1] else 0
                e_d_trypsin = contig_replacements[i][2][e_d] if e_d in contig_replacements[i][2] else 0
                q_n_trypsin = contig_replacements[i][2][q_n] if q_n in contig_replacements[i][2] else 0
                acid_ratio = q_n_acid/float(e_d_acid) if e_d_acid != 0 else q_n_acid
                trypsin_ratio = q_n_trypsin/float(e_d_trypsin) if e_d_trypsin != 0 else q_n_trypsin
                # decide acid and calculate prob:
                if q_n == 'Q':
                    if e_d_trypsin == 0 and q_n_trypsin > 0:
                        new_acid = 'Q'
                        adjusted_prob = q_n_trypsin/float(3) if q_n_trypsin < 3 else 1
                    elif e_d_trypsin > 0 and q_n_trypsin == 0:
                        new_acid = 'E'
                        adjusted_prob = e_d_trypsin/float(3) if q_n_trypsin < 3 else 1
                    elif acid_ratio > min_q_acid_ratio:
                        new_acid = 'Q'
                        adjusted_prob = (acid_ratio-min_q_acid_ratio)/float(2*min_q_acid_ratio) \
                            if (acid_ratio-min_q_acid_ratio)/float(2*min_q_acid_ratio) < 1 else 1
                    else:
                        new_acid = 'E'
                        adjusted_prob = (min_q_acid_ratio-acid_ratio)/float(min_q_acid_ratio)
                elif q_n == 'N':
                    if acid_ratio > min_n_acid_ratio:
                        new_acid = 'N'
                        adjusted_prob = (acid_ratio-min_n_acid_ratio)/float(min_n_acid_ratio) \
                            if (acid_ratio-min_n_acid_ratio)/float(min_n_acid_ratio) < 1 else 1
                    elif trypsin_ratio >= min_n_trypsin_ratio and acid_ratio > min_q_acid_ratio:
                        new_acid = 'N'
                        adjusted_prob = (trypsin_ratio-min_n_trypsin_ratio)/float(min_n_trypsin_ratio) \
                            if (trypsin_ratio-min_n_trypsin_ratio)/float(min_n_trypsin_ratio) < 1 else 1
                    else:
                        new_acid = 'D'
                        if trypsin_ratio < min_n_trypsin_ratio:
                            adjusted_prob = (min_n_trypsin_ratio-trypsin_ratio)/float(min_n_trypsin_ratio)
                        else:
                            adjusted_prob = (min_q_acid_ratio-acid_ratio)/float(min_q_acid_ratio)
                else:
                    new_acid = e_d
                    adjusted_prob = 0.5
                # call results
                new_sequence += "[(" + str(counter) + "," + REPLACEMENT_CLASS + "," + str(i) + ")" + new_acid + "]"
                reps_and_collisions_probabilities.append(adjusted_prob)
                counter += 1
            elif len(contig_variants_collisions[i]) > 1:
                # # check left acid and right acid to see if it's a swap and if we can guess the right acid:
                # if ((i-1 >= 0 and sequence[i-1] == contig_variants_collisions[i][0][0] and
                #             len(contig_variants_collisions[i-1]) <= 1 and (i+1 >= len(sequence) or sequence[i+1] != contig_variants_collisions[i][1][0])) or (i+1 < len(sequence) and
                #             sequence[i+1] == contig_variants_collisions[i][0][0] and
                #             len(contig_variants_collisions[i+1]) <= 1 and (i-1 < 0 or sequence[i-1] != contig_variants_collisions[i][1][0]))):
                #     new_acid = contig_variants_collisions[i][1][0]
                # else:
                new_acid = contig_variants_collisions[i][0][0]
                new_sequence += "[(" + str(counter) + "," + REPLACEMENT_CLASS + "," + str(i) + ")" + new_acid + "]"
                reps_and_collisions_probabilities.append(0)
                counter += 1
            else:
                new_sequence += "{" + str(i) + "," + c + "}"
        return new_sequence, reps_and_collisions_probabilities

    @staticmethod
    def _color_by_probs(sequence, reps_probabilities):
        idx = 0
        reps_probabilities = ['color: rgb(' + str(int(round((1 - reps_probability) * 255))) + ',' +
                              str(int(round(reps_probability * 255))) + ',0)' for reps_probability in
                              reps_probabilities]
        while (NO_COLOR_YET in sequence):
            sequence = sequence.replace(NO_COLOR_YET, reps_probabilities[idx], 1)
            idx += 1
        return sequence

    def _get_final_seq(self, kmer_contig, contig_replacements, contig_variants_collisions):
        final_sequence_with_replacements_and_collisions, reps_and_collisions_probabilities = \
            self._decide_sequence_replacements_and_collisions(
                str(kmer_contig.sequence), contig_replacements, contig_variants_collisions)
        final_html_sequence = re.sub("\[\(([^\)]*),([^\)]*),([^\)]*)\)([^\]]*)\]",
                                     "<font class=" + kmer_contig.single_identifier +
                                     "_\\1 style='" + NO_COLOR_YET +
                                     "' onmouseover='light_acids(true, this);show_index(\\3, \"" + kmer_contig.single_identifier +
                                     "\")' onmouseout='light_acids(false, this);show_index(0, \"" + kmer_contig.single_identifier + "\")'>\\4</font>",
                                     final_sequence_with_replacements_and_collisions)
        final_html_sequence = re.sub("\{([^\}]*),([^\}]*)\}",
                                     "<font onmouseover='show_index(\\1, \"" + kmer_contig.single_identifier +
                                     "\")' onmouseout='show_index(0, \"" + kmer_contig.single_identifier + "\")'>\\2</font>",
                                     final_html_sequence)
        final_colored_html_sequence = self._color_by_probs(final_html_sequence, reps_and_collisions_probabilities)
        final_chunked_colored_html_sequence = self.chunk_html_string(final_colored_html_sequence,
                                                                     NUMBER_OF_ACIDS_PER_LINE)
        return final_chunked_colored_html_sequence

    def get_indices(self, final_seq_chunks, kmer_contig):
        num_chunks = len(final_seq_chunks)
        indices_left = [i * NUMBER_OF_ACIDS_PER_LINE for i in range(num_chunks)]
        indices_right = [(i * NUMBER_OF_ACIDS_PER_LINE) - 1 for i in range(1, num_chunks)]
        indices_right.append(len(kmer_contig) - 1)
        indices_left = [str(index+1) for index in indices_left]
        indices_right = [str(index+1) for index in indices_right]
        return ("<br>".join(indices_left), "<br>".join(indices_right))

    def _init_report(self):
        self.coverage_graphs = []
        self.sequences_chunks = []
        self.final_sequences_chunks = []
        self.indices_left = []
        self.indices_right = []
        for (i, kmer_contig) in enumerate(self.kmer_contigs):
            # create adjusted sequence:
            self.sequences_chunks.append(self._get_adjusted_seq(kmer_contig, self.contigs_replacements[i],
                                                                self.contigs_variants_collisions[i]))
            # create final sequence:
            final_seq_chunks = self._get_final_seq(kmer_contig, self.contigs_replacements[i],
                                                   self.contigs_variants_collisions[i])
            self.final_sequences_chunks.append(final_seq_chunks)
            (indices_left, indices_right) = self.get_indices(final_seq_chunks, kmer_contig)
            self.indices_left.append(indices_left)
            self.indices_right.append(indices_right)
            # create coverage graph:
            coverage_id = kmer_contig.single_identifier + "_coverage"
            coverage_y = [contig_variants_collision[0][1] for contig_variants_collision in
                          self.contigs_variants_collisions[i]]
            coverage_x = range(1, len(kmer_contig)+1)
            self.coverage_graphs.append(Graph(coverage_id, coverage_x, coverage_y))
        # copy resources:
        rmtree(self.resources_dir, ignore_errors=True)
        copytree(RESOURCES_DIR, self.resources_dir)

    def create_html_report(self, report_name):
        # create html report
        logger.info("starting to write final report...")
        report_file_path = os.path.join(self.output_dir, report_name + ".html")
        with open(os.path.join(REPROTS_DIR, 'contigs_report.html'), 'r') as template_file:
            template = Template(template_file.read())
        html_text = template.render(kmer_contigs=self.kmer_contigs, coverage_graphs=self.coverage_graphs,
                                    num_contigs=len(self.kmer_contigs), sequences_chunks=self.sequences_chunks,
                                    final_sequences_chunks=self.final_sequences_chunks, indices_left=self.indices_left,
                                    indices_right=self.indices_right, rep_explain=self.rep_explain,
                                    acid_input_files_paths=self.acid_input_files_paths,
                                    trypsin_input_files_paths=self.trypsin_input_files_paths, kmer_size=self.kmer_size,
                                    kmer_min_overlap=self.kmer_min_overlap, unite_min_overlap=self.unite_min_overlap,
                                    unite_min_extension=self.unite_min_extension,
                                    merge_min_quality=self.merge_min_quality, report_title=self.report_title)
        # write to file
        with open(report_file_path, 'w') as html_f:
            html_f.write(html_text)
        logger.info("finished writing final report")
