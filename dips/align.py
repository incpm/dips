import logging
import Levenshtein
import math

# from .assemble import MassDistance
from .data import Alignment
from .settings import MIN_CONFIDENCE, ALIGNMENT_MIN_ALIGNED_ACIDS, ALIGNMENT_MARGINS  # OAOA TODO: use dynamic settings

logger = logging.getLogger(__name__)

ALIGNMENT_SCORER_MIN_ACCURACY = 0.4  # TODO: remove or re-write


class AlignmentScorer(object):
    def __init__(self, accuracy_threshold=None, alignment_per_amino_acid_accuracy=None,
                 min_confidence=MIN_CONFIDENCE):
        """
        There are two ways to calculate accuracy threshold -
        1. explicitly
        2. using a per-amino-acid threshold
        """
        assert accuracy_threshold is not None or alignment_per_amino_acid_accuracy is not None
        self.accuracy_threshold = accuracy_threshold
        self.alignment_per_amino_acid_accuracy = alignment_per_amino_acid_accuracy
        self.min_confidence = min_confidence

    def _calc_max_num_non_aligned_acids(self, sequence):
        if self.accuracy_threshold is None:
            len_dependent_threshold =\
                self.alignment_per_amino_acid_accuracy ** sequence.clean_length(self.min_confidence)
            accuracy_threshold = max(ALIGNMENT_SCORER_MIN_ACCURACY, len_dependent_threshold)
        else:
            accuracy_threshold = self.accuracy_threshold
        return math.floor(sequence.clean_length(self.min_confidence) * (1 - accuracy_threshold))

    def score(self, protein, protein_position, sequence):
        max_num_non_aligned_acids = self._calc_max_num_non_aligned_acids(sequence)
        return self.find_alignment(protein, protein_position, sequence, max_num_non_aligned_acids)

    def find_alignment(self, protein, protein_position, sequence, max_num_non_aligned_acids):
        """
        This method can be overridden to implement a different alignment algorithm

        :param int protein_position: position in the protein to check, can also be negative
        :return an Alignment object if the sequence is aligned to the protein in this position, None otherwise
        """
        num_aligned_acids = 0
        num_non_aligned_acids = 0
        for j in range(len(sequence)):
            if sequence.confidence[j] < self.min_confidence:
                continue
            protein_offset = protein_position + j
            if protein_offset < 0:
                continue
            if protein_offset >= len(protein.sequence):
                break
            if protein.sequence[protein_offset] != sequence.sequence[j]:
                num_non_aligned_acids += 1
                if num_non_aligned_acids > max_num_non_aligned_acids:
                    return None
            else:
                num_aligned_acids += 1
        if num_aligned_acids < ALIGNMENT_MIN_ALIGNED_ACIDS:
            return None
        quality = \
            (sequence.clean_length(self.min_confidence) - num_non_aligned_acids) / \
            float(sequence.clean_length(self.min_confidence))
        return Alignment(sequence=sequence, protein=protein, position=protein_position, quality=quality)


class AlignmentSwapScorer(AlignmentScorer):
    def __init__(self, accuracy_threshold=None, alignment_per_amino_acid_accuracy=None,
                 min_confidence=MIN_CONFIDENCE):
        super(AlignmentSwapScorer, self).__init__(accuracy_threshold=accuracy_threshold, alignment_per_amino_acid_accuracy=alignment_per_amino_acid_accuracy,
                 min_confidence=min_confidence)

    def find_alignment(self, protein, protein_position, sequence, max_num_non_aligned_acids):
        """
        This method can be overridden to implement a different alignment algorithm

        :param int protein_position: position in the protein to check, can also be negative
        :return an Alignment object if the sequence is aligned to the protein in this position, None otherwise
        """
        num_aligned_acids = 0
        num_non_aligned_acids = 0
        last_wrong = False
        for j in range(len(sequence)):
            if sequence.confidence[j] < self.min_confidence:
                continue
            protein_offset = protein_position + j
            if protein_offset < 0:
                continue
            if protein_offset >= len(protein.sequence):
                break
            if protein.sequence[protein_offset] != sequence.sequence[j]:
                # if swap fixes:
                if(last_wrong==True and protein.sequence[protein_offset-1]==sequence.sequence[j] and
                           protein.sequence[protein_offset]==sequence.sequence[j-1]):
                    num_non_aligned_acids -= 1
                    num_aligned_acids += 2
                    last_wrong = False
                else:
                    num_non_aligned_acids += 1
                    last_wrong = True
                if num_non_aligned_acids > max_num_non_aligned_acids+1:
                    return None
            else:
                num_aligned_acids += 1
                last_wrong = False
        if num_non_aligned_acids > max_num_non_aligned_acids:
                    return None
        if num_aligned_acids < ALIGNMENT_MIN_ALIGNED_ACIDS:
            return None
        #print num_non_aligned_acids, max_num_non_aligned_acids
        quality = \
            (sequence.clean_length(self.min_confidence) - num_non_aligned_acids) / \
            float(sequence.clean_length(self.min_confidence))
        return Alignment(sequence=sequence, protein=protein, position=protein_position, quality=quality)


class LevenshteinAlignmentScorer(AlignmentScorer):
    def find_alignment(self, protein, protein_position, sequence, max_num_non_aligned_acids):
        """
        Use Levenshtein distance to check if the sequence matches the protein
        """
        # trim both sequences to the same size
        protein_offset = max(0, protein_position)
        sequence_offset = max(0, -protein_position)
        comparison_len = min(len(protein) - protein_offset, len(sequence) - sequence_offset)
        distance = Levenshtein.distance(str(protein.sequence[protein_offset:protein_offset+comparison_len]),
                                        str(sequence.sequence[sequence_offset:sequence_offset+comparison_len]))
        # for the initial implementation, we can use Levenshtein distance as an equivalent of "num_non_aligned_acids"
        if distance > max_num_non_aligned_acids:
            return None
        num_aligned_acids = comparison_len - distance
        if num_aligned_acids < ALIGNMENT_MIN_ALIGNED_ACIDS:
            return None
        quality = float(num_aligned_acids) / comparison_len
        return Alignment(sequence=sequence, protein=protein, position=protein_position, quality=quality)

# class MassAlignmentScorer(AlignmentScorer):
#     def score(self, protein, protein_position):
#         # TODO: take the confidence into account
#         if protein_position < 0 or protein_position + len(self.sequence) > len(protein):
#             return None
#         num_non_aligned_acids = MassDistance(sequence_a=self.sequence.sequence, sequence_b=protein.sequence[protein_position:protein_position+len(self.sequence)]).calculate()
#         num_aligned_acids = len(self.sequence) - num_non_aligned_acids
#         if num_non_aligned_acids > self.max_num_non_aligned_acids:
#             return None
#         if num_aligned_acids < ALIGNMENT_MIN_ALIGNED_ACIDS:
#             return None
#         sequence_length = self.sequence.clean_length(self.min_confidence) # TODO: use real length instead of clean length?
#         quality = (sequence_length - num_non_aligned_acids) / float(sequence_length)
#         return Alignment(sequence=self.sequence, protein=protein, position=protein_position, quality=quality)


class NaiveAligner(object):
    def __init__(self, proteins, scorer, alignment_margins=ALIGNMENT_MARGINS, stop_on_first_alignment=True):
        self.proteins = proteins if(type(proteins)==list) else [proteins]
        self.scorer = scorer
        self.alignment_margins = alignment_margins
        self.stop_on_first_alignment = stop_on_first_alignment

    def align_to_protein(self, protein, sequence):
        alignments = []
        for i in range(-self.alignment_margins,
                       len(protein.sequence) - len(sequence.sequence) + self.alignment_margins + 1):
            alignment = self.scorer.score(protein=protein, protein_position=i, sequence=sequence)
            if alignment:  # stop on first alignment found (we can also continue and choose the best alignment)
                if self.stop_on_first_alignment:
                    return alignment
                else:
                    alignments.append(alignment)
        if alignments:
            alignments.sort(key=lambda x: x.quality, reverse=True)
            return alignments[0]
        return None

    def align(self, sequences):
        alignments = []
        unaligned_sequences = []
        for (i, sequence) in enumerate(sequences):
            if (i % 100) == 0:
                logger.debug('Tried aligning %d sequences so far...' % i)
            is_aligned = False
            for protein in self.proteins:
                alignment = self.align_to_protein(protein, sequence)
                if alignment:
                    alignments.append(alignment)
                    is_aligned = True
                    break
            if not is_aligned:
                unaligned_sequences.append(sequence)
        logger.debug('Alignment done')
        return alignments, unaligned_sequences
