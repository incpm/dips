import logging
LOGGER_NAME = 'dips'
LOGGING = dict(
    version=1,
    formatters={
        'simpleFormatter': {
            'format': '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        }
    },
    handlers={
        'consoleHandler': {
            'class': 'logging.StreamHandler',
            'formatter': 'simpleFormatter',
            'level': logging.INFO, 'stream': 'ext://sys.stdout'
        }
    },
    loggers={
        'root': {
            'handlers': ['consoleHandler'],
            'level': logging.INFO
        },
        LOGGER_NAME: {
            'handlers': ['consoleHandler'],
            'level': logging.DEBUG,
            'qualname': LOGGER_NAME,
            'propagate': 0
        }
    }
)
