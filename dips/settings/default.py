# general parameters
MIN_CONFIDENCE = 80  # percent
MIN_CLEAN_LENGTH = 1

# alignment parameters
ALIGNMENT_ACCURACY_THRESHOLD = 0.9
ALIGNMENT_PER_AMINO_ACID_ACCURACY = 0.98
ALIGNMENT_MARGINS = 3
ALIGNMENT_MIN_ALIGNED_ACIDS = 5

# downsampling parameters
#DOWNSAMPLE_MAX_NUM_SEQUENCES = 10000 # if non-positive, do not down sample
DOWNSAMPLE_MAX_NUM_SEQUENCES = 0 # if non-positive, do not down sample

# assembly parameters
#ASSEMBLER_CLASS = 'StubAssembler'
ASSEMBLER_CLASS = 'MultiPassAssembler'

ASSEMBLY_MAX_NUM_ITERATIONS = 400
ASSEMBLY_NUM_KMERS_TO_USE_PER_ITERATION = 200
