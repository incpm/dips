from .default import *  # @UnusedWildImport

ASSEMBLER_CLASS = 'MultiPassAssembler'

ASSEMBLY_PARAMS = dict(kmer_sizes=(11, 9, 7, 6),
                       min_confidence=MIN_CONFIDENCE,
                       max_num_iterations=ASSEMBLY_MAX_NUM_ITERATIONS,
                       num_kmers_to_use_per_iteration=ASSEMBLY_NUM_KMERS_TO_USE_PER_ITERATION)
