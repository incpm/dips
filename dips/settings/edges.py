from .default import *  # @UnusedWildImport

ASSEMBLER_CLASS = 'EdgesAssembler'

ASSEMBLY_PARAMS = dict(min_overlap=7, max_overlap=26, max_num_iterations=20)
