import re

from dips.data import Protein

WHITESPACE_PATTERN = re.compile(r'\s+')


def sequence_to_protein(identifier, sequence):
    return Protein(identifier=identifier, sequence=re.sub(WHITESPACE_PATTERN, '', sequence))

MYOGLOBIN = sequence_to_protein('MYOGLOBIN', '''MGLSDGEWQQVLNVWGKVEADIAGHGQEVLIRLFTGHPETLEKFDKFKHL
KTEAEMKASEDLKKHGTVVLTALGGILKKKGHHEAELKPLAQSHATKHKI
PIKYLEFISDAIIHVLHSKHPGDFGADAQGAMTKALELFRNDIAAKYKEL
GFQG''')

FETUIN = sequence_to_protein('FETUIN', '''MKSFVLLFCLAQLWGCHSIPLDPVAGYKEPACDDPDTEQAALAAVDYINK
HLPRGYKHTLNQIDSVKVWPRRPTGEVYDIEIDTLETTCHVLDPTPLANC
SVRQQTQHAVEGDCDIHVLKQDGQFSVLFTKCDSSPDSAEDVRKLCPDCP
LLAPLNDSRVVHAVEVALATFNAESNGSYLQLVEISRAQFVPLPVSVSVE
FAVAATDCIAKEVVDPTKCNLLAEKQYGFCKGSVIQKALGGEDVRVTCTL
FQTQPVIPQPQPDGAEAEAPSAVPDAAGPTPSAAGPPVASVVVGPSVVAV
PLPLHRAHYDLRHTFSGVASVESSSGEAFHVGKTPIVGQPSIPGGPVRLC
PGRIRYFKI''')

ALBU_BOVIN = sequence_to_protein('ALBU_BOVIN', '''MKWVTFISLLLLFSSAYSRGVFRRDTHKSEIAHRFKDLGEEHFKGLVLIA
FSQYLQQCPFDEHVKLVNELTEFAKTCVADESHAGCEKSLHTLFGDELCK
VASLRETYGDMADCCEKQEPERNECFLSHKDDSPDLPKLKPDPNTLCDEF
KADEKKFWGKYLYEIARRHPYFYAPELLYYANKYNGVFQECCQAEDKGAC
LLPKIETMREKVLTSSARQRLRCASIQKFGERALKAWSVARLSQKFPKAE
FVEVTKLVTDLTKVHKECCHGDLLECADDRADLAKYICDNQDTISSKLKE
CCDKPLLEKSHCIAEVEKDAIPENLPPLTADFAEDKDVCKNYQEAKDAFL
GSFLYEYSRRHPEYAVSVLLRLAKEYEATLEECCAKDDPHACYSTVFDKL
KHLVDEPQNLIKQNCDQFEKLGEYGFQNALIVRYTRKVPQVSTPTLVEVS
RSLGKVGTRCCTKPESERMPCTEDYLSLILNRLCVLHEKTPVSEKVTKCC
TESLVNRRPCFSALTPDETYVPKAFDEKLFTFHADICTLPDTEKQIKKQT
ALVELLKHKPKATEEQLKTVMENFVAFVDKCCAADDKEACFAVEGPKLVV
STQTALA''')

MYG_HORSE = sequence_to_protein('MYG_HORSE', '''MGLSDGEWQQVLNVWGKVEADIAGHGQEVLIRLFTGHPETLEKFDKFKHLKTEAEMKASE
DLKKHGTVVLTALGGILKKKGHHEAELKPLAQSHATKHKIPIKYLEFISDAIIHVLHSKH
PGDFGADAQGAMTKALELFRNDIAAKYKELGFQG''')

CASK_BOVIN = sequence_to_protein('CASK_BOVIN', '''MMKSFFLVVTILALTLPFLGAQEQNQEQPIRCEKDERFFSDKIAKYIPIQYVLSRYPSYG
LNYYQQKPVALINNQFLPYPYYAKPAAVRSPAQILQWQVLSNTVPAKSCQAQPTTMARHP
HPHLSFMAIPPKKNQDKTEIPTINTIASGEPTSTPTTEAVESTVATLEDSPEVIESPPEI
NTVQVTSTAV''')

FETUA_BOVIN = sequence_to_protein('FETUA_BOVIN', '''MKSFVLLFCLAQLWGCHSIPLDPVAGYKEPACDDPDTEQAALAAVDYINKHLPRGYKHTL
NQIDSVKVWPRRPTGEVYDIEIDTLETTCHVLDPTPLANCSVRQQTQHAVEGDCDIHVLK
QDGQFSVLFTKCDSSPDSAEDVRKLCPDCPLLAPLNDSRVVHAVEVALATFNAESNGSYL
QLVEISRAQFVPLPVSVSVEFAVAATDCIAKEVVDPTKCNLLAEKQYGFCKGSVIQKALG
GEDVRVTCTLFQTQPVIPQPQPDGAEAEAPSAVPDAAGPTPSAAGPPVASVVVGPSVVAV
PLPLHRAHYDLRHTFSGVASVESSSGEAFHVGKTPIVGQPSIPGGPVRLCPGRIRYFKI''')

FOUR_PROTEINS = [ALBU_BOVIN, MYG_HORSE, CASK_BOVIN, FETUA_BOVIN]

BSA_SEQUENCE = '''MKWVTFISLLLLFSSAYSRGVFRRDTHKSEIAHRFKDLGEEHFKGLVLIA
FSQYLQQCPFDEHVKLVNELTEFAKTCVADESHAGCEKSLHTLFGDELCK
VASLRETYGDMADCCEKQEPERNECFLSHKDDSPDLPKLKPDPNTLCDEF
KADEKKFWGKYLYEIARRHPYFYAPELLYYANKYNGVFQECCQAEDKGAC
LLPKIETMREKVLTSSARQRLRCASIQKFGERALKAWSVARLSQKFPKAE
FVEVTKLVTDLTKVHKECCHGDLLECADDRADLAKYICDNQDTISSKLKE
CCDKPLLEKSHCIAEVEKDAIPENLPPLTADFAEDKDVCKNYQEAKDAFL
GSFLYEYSRRHPEYAVSVLLRLAKEYEATLEECCAKDDPHACYSTVFDKL
KHLVDEPQNLIKQNCDQFEKLGEYGFQNALIVRYTRKVPQVSTPTLVEVS
RSLGKVGTRCCTKPESERMPCTEDYLSLILNRLCVLHEKTPVSEKVTKCC
TESLVNRRPCFSALTPDETYVPKAFDEKLFTFHADICTLPDTEKQIKKQT
ALVELLKHKPKATEEQLKTVMENFVAFVDKCCAADDKEACFAVEGPKLVV
STQTALA'''

#BSA_40_TO_80 = Protein(identifier='BSA_40_TO_80', sequence='ELAHRFKDLGEEHFKGLVLLAFAEYLEECPFDEHVKLVDELTEFAKTCVA')

BSA = Protein(identifier='BSA', sequence=re.sub(re.compile(r'\s+'), '', BSA_SEQUENCE))
SINGLE_PROTEIN = [BSA]
#SINGLE_PROTEIN = [BSA_40_TO_80]

KNOWN_PROTEINS = dict([(protein.single_identifier, protein) for protein in SINGLE_PROTEIN + FOUR_PROTEINS + [FETUIN, MYOGLOBIN]])


SIGMA_LIGHT = sequence_to_protein('SIGMA_LIGHT', '''QSALTQPRSVSGSPGQSVTISCTGTSSDIGGYNFVSWYQQHPGKAPKLMIYDATKRPSGVPD
RFSGSKSGNTASLTISGLQAEDEADYYCCSYAGDYTPGVVFGGGTKLTVLGQPKAAPSVTLFPPSSEELQANKATLVCLISDFYPGAVTVAWKADSSPVKAGVETTTPSKQSNN
KYAASSYLSLTPEQWKSHRSYSCQVTHEGSTVEKTVAPTECS''')

SIGMA_HEAVY = sequence_to_protein('SIGMA_HEAVY', '''EVQLVESGGGLVQPGGSLRLSCVASGFTLNNYDMHWVRQGIGKGLEWVSKIGTAGDRYYAGS
VKGRFTISRENAKDSLYLQMNSLRVGDAAVYYCARGAGRWAPLGAFDIWGQGTMVTVSSASTKGPSVFPLAPSSKSTSGGTAALGCLVKDYFPEPVTVSWNSGALTSGVHTFPA
VLQSSGLYSLSSVVTVPSSSLGTQTYICNVNHKPSNTKVDKKVEPKSCDKTHTCPPCPAPELLGGPSVFLFPPKPKDTLMISRTPEVTCVVVDVSHEDPEVKFNWYVDGVEVHN
AKTKPREEQYNSTYRVVSVLTVLHQDWLNGKEYKCKVSNKALPAPIEKTISKAKGQPREPQVYTLPPSRDELTKNQVSLTCLVKGFYPSDIAVEWESNGQPENNYKTTPPVLDS
DGSFFLYSKLTVDKSRWQQGNVFSCSVMHEALHNHYTQKSLSLSPG''')

AR_HEAVY = sequence_to_protein('AR_HEAVY', '''EVQLVESGGDLVKPGGSLKLSCAASGFTFSNSGMSWFRLTPDKRLEWVATLSSGSTYTFYPDTVKGRF
LLSRDNAKNTLYLQMSSLKSEDTALYYCVRELWPVWGAGTTLTVSSAKTTPSSVYPLAPGSAAQTNSMVTLGCLVKGYFPEPVTVTWNSGSLSSGVHTFPAVLQSDLYTLSSSV
TVPSSTWPSETVTCNVAHPASSTKVDKKIVPRDCGCKPCICTVPEVSSVFIFPPKPKDVLTITLTPKVTCVVVDISKDDPEVQFSWFVDDVEVHTAQTQPREEQFNSTFRSVSE
LPIMHQDWLNGKEFKCRVNSAAFPAPIEKTISKTKGRPKAPQVYTIPPPKEQMAKDKVSLTCMITDFFPEDITVEWQWNGQPAENYKNTQPIMDTDGSYFIYSKLNVQKSNWEA
GNTFTCSVLHEGLHNHHTEKSLSHSPGK''')

AR_LIGHT = sequence_to_protein('AR_LIGHT', '''SPPVTQESALSTSPGETVTLTCRSSTGAVTTSNYANWVQEKPDHLFTGLLGDTDNRPPGVPARFSGSL
LGDKAALTLTGAQTQDEALYFCALWYSNHWVFGGGTKLTVLGQPKSSPSVTLFPPSSEELETNKATLVCTLTDFYPGVVTVDWKVDGTPVTQGMETTQPSKQSNNKYMASSYLT
LTARAWQRHSSYSCQVTHEGHTVEKSLSRADCS''')

AVASTIN_HEAVY = sequence_to_protein('AVASTIN_HEAVY', '''EVQLVESGGGLVQPGGSLRLSCAASGYTFTNYGMNWVRQAPGKGLEWVGWINTYTGEP
TYAADFKRRFTFSLDTSKSTAYLQMNSLRAEDTAVYYCAKYPHYYGSSHWYFDVWGQGTLVTVSSASTKGPSVFPLAPSSKSTSGGTAALGCLVKDYFPEPVTVSWNSGALTSG
VHTFPAVLQSSGLYSLSSVVTVPSSSLGTQTYICNVNHKPSNTKVDKKVEPKSCDKTHTCPPCPAPELLGGPSVFLFPPKPKDTLMISRTPEVTCVVVDVSHEDPEVKFNWYVD
GVEVHNAKTKPREEQYNSTYRVVSVLTVLHQDWLNGKEYKCKVSNKALPAPIEKTISKAKGQPREPQVYTLPPSREEMTKNQVSLTCLVKGFYPSDIAVEWESNGQPENNYKTT
PPVLDSDGSFFLYSKLTVDKSRWQQGNVFSCSVMHEALHNHYTQKSLSLSPGK''')

AVASTIN_LIGHT = sequence_to_protein('AVASTIN_LIGHT', '''DIQMTQSPSSLSASVGDRVTITCSASQDISNYLNWYQQKPGKAPKVLIYFTSSLHSGV
PSRFSGSGSGTDFTLTISSLQPEDFATYYCQQYSTVPWTFGQGTKVEIKRTVAAPSVFIFPPSDEQLKSGTASVVCLLNNFYPREAKVQWKVDNALQSGNSQESVTEQDSKDST
YSLSSTLTLSKADYEKHKVYACEVTHQGLSSPVTKSFNRGEC''')

IFX_HEAVY = sequence_to_protein('IFX_HEAVY', '''EVKLEESGGGLVQPGGSMKLSCVASGFIFSNHWMNWVRQSPEKGLEWVAEIRSKSINSATHY
AESVKGRFTISRDSKSAVYLQMTDLRTEDTGVYYCSRNYYGSTYDYWGQGTTLTVSSASTKGPSVFPLAPSSKSTSGGTAALGCLVKDYFPEPVTVSWNSGALTSGVHTFPAVL
QSSGLYSLSSVVTVPSSSLGTQTYICNVNHKPSNTKVDKKVEPKSCDKTHTCPPCPAPELLGGPSVFLFPPKPKDTLMISRTPEVTCVVVDVSHEDPEVKFNWYVDGVEVHNAK
TKPREEQYNGSTSTYRVVSVLTVLHQDWLNGKEYKCKVSNKALPAPIEKTISKAKGQPREPQVYTLPPSRDELTKNQVSLTCLVKGFYPSDIAVEWESNGQPENNYKTTPPVLD
SDGSFFLYSKLTVDKSRWQQGNVFSCSVMHEALHNHYTQKSLSLSP''')

IFX_LIGHT = sequence_to_protein('IFX_LIGHT', '''DILLTQSPAILSVSPGERVSFSCRASQFVGSSIHWYQQRTNGSPRLLIKYASESMSGIPSRFSGSG
SGTDFTLSINTVESEDIADYYCQQSHSWPFTFGSGTQLEVKRTVAAPSVFIFPPSDEQLKSGTASVVCLLNNFYPREAKVQWKVDNALQSGNSQESVTEQDSKDSTYSLSSTLT
LSKADYEKHKVYACEVTHQGLSSPVTKSFNRGEC''')

SA_HEAVY = sequence_to_protein('SA_HEAVY', '''EVQLLESGGGLVQPGGSLRLSCAASGFTFSGYWMHWVRQAPGKGLEWVSEISGSGDSTHYGDSVKGRF
TISRDNSKNTLYLQMNSLRAEDTAVYYCARGRNGSLDYWGQGTLVTVSSASTKGPSVFPLAPSSKSTSGGTAALGCLVKDYFPEPVTVLWNSGALTSGVHTFPAVLQSSGLYSL
SSVVTVPSSSLGTQTYICNVNHKPSNTEVDKKVEPKSCDKTHTCPPCPAPELLGGPSVFLFPPKPKDTLMISRTPEVTCVVVDVSHEDPEVKFNWYVDGVEVHNAKTKPREEQY
NSTYRVVSVLTVLHQDWLNGKEYKCKVSNKALPAPIEKTISKAKGQPREPQVCTLPPSREEMTKNQVSLSCAVKGFYPSDIAVEWESNGQPENNYKTTPPVLDSDGSFFLVSKL
TVDKSRWQQGNVFSCSVMHEALHNHYTQKSLSLSPGK''')

SA_LIGHT = sequence_to_protein('SA_LIGHT', '''QSVLTQPPSASGTPGQRVTLSCTGSSSNIGSYSVSWYQQLPGTAPKLLIYDNNKRPSGVSDRFSGSKS
GTSASLAISGLRSEDEADYYCQSYDSSLTGSVVFGGGTKLTVLGQPKAAPSVTLFPPSSEELQANKATLVCLISDFYPGAVTVAWKADSSPVKAGVETTTPSKQSNNKYAASSY
LSLTPEQWKSHRSYSCQVTHEGSTVEKTVAPAECS''')
