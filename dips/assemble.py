import abc
from collections import OrderedDict, namedtuple
import logging.config
import Levenshtein
import operator

import itertools

from .data import Contig, Peptide, OverlappingSequence, Kmer, KmerContig, MODIFICATION
from .mass import calc_mass
from dips.cleaner import PeptideCleaner
from dips.io import PeptideReader, ProteinSequenceCsvWriter, KmerWriter

import os
import errno
import json
from dips.proteins import MYOGLOBIN, FETUIN, BSA, SIGMA_HEAVY, SIGMA_LIGHT, AVASTIN_HEAVY, AVASTIN_LIGHT, IFX_HEAVY, \
    IFX_LIGHT, SA_HEAVY, SA_LIGHT, AR_HEAVY, AR_LIGHT
from dips.reporter import ContigsReporter
from dips.jsonutils import json_dump_default, json_load_object_hook
from dips.data import ACID_TYPE, TRYPSIN_TYPE

from dips.data import peptides_cache, kmers_cache

peptides_cache.clear()
kmers_cache.clear()

"""
logging
"""
from dips.settings.logging_settings import LOGGING, LOGGER_NAME

logging.config.dictConfig(LOGGING)
logger = logging.getLogger(LOGGER_NAME)


class Assembler(object):
    metaclass = abc.ABCMeta

    def __init__(self):
        pass

    @abc.abstractmethod
    def assemble(self, sequences):
        assert sequences, "No sequences"
        return set(sequences)

    @abc.abstractmethod
    def assemble_contig(self, sequences, *args, **kwargs):
        return StubContigAssembler().assemble(sequences)


class OnePassAssembler(Assembler):
    def __init__(self, kmer_size, min_confidence, max_num_iterations, num_kmers_to_use_per_iteration):
        super(OnePassAssembler, self).__init__()
        self.kmer_size = kmer_size
        self.min_confidence = min_confidence
        self.max_num_iterations = max_num_iterations
        self.num_kmers_to_use_per_iteration = num_kmers_to_use_per_iteration

    def assemble(self, sequences):
        logger.info("OnePassAssembler started with %d sequences" % (len(sequences)))
        assembled_sequences = set(sequences)
        for num_iterations in range(1, self.max_num_iterations + 1):
            logger.debug("Iteration #%d started - with %d sequences (kmer_size=%d)" % (
                num_iterations, len(assembled_sequences), self.kmer_size))
            new_assembled_sequences, old_sequences = self.assemble_contig(assembled_sequences)
            if not new_assembled_sequences:
                # could not assemble a contig from remaining sequences, no point in proceeding
                logger.debug("Iteration #%d ended with %d sequences" % (num_iterations, len(assembled_sequences)))
                break
            assembled_sequences = assembled_sequences.difference(old_sequences)
            assembled_sequences = assembled_sequences.union(new_assembled_sequences)
            logger.debug("Iteration #%d ended with %d sequences" % (num_iterations, len(assembled_sequences)))
            if len(assembled_sequences) == 1:
                # single sequence left, no point in proceeding
                break
        logger.info(
            "Assembly ended after %d iterations with %d sequences" % (num_iterations + 1, len(assembled_sequences)))
        return assembled_sequences

    def assemble_contig(self, sequences):
        return NaiveContigAssembler(kmer_size=self.kmer_size, min_confidence=self.min_confidence,
                                    num_kmers_to_assemble=self.num_kmers_to_use_per_iteration).assemble(sequences)


class MultiPassAssembler(Assembler):
    def __init__(self, kmer_sizes, min_confidence, max_num_iterations, num_kmers_to_use_per_iteration):
        super(MultiPassAssembler, self).__init__()
        assert kmer_sizes
        self.kmer_sizes = kmer_sizes
        self.min_confidence = min_confidence
        self.max_num_iterations = max_num_iterations
        self.num_kmers_to_use_per_iteration = num_kmers_to_use_per_iteration

    def assemble(self, sequences):
        assembled_sequences = sequences
        for kmer_size in self.kmer_sizes:
            assembled_sequences = OnePassAssembler(kmer_size=kmer_size, min_confidence=self.min_confidence,
                                                   max_num_iterations=self.max_num_iterations,
                                                   num_kmers_to_use_per_iteration=self.num_kmers_to_use_per_iteration).assemble(
                assembled_sequences)
        return assembled_sequences


class StubAssembler(Assembler):
    def assemble(self, sequences):
        return sequences


class ContigAssembler(object):
    metaclass = abc.ABCMeta

    @abc.abstractmethod
    def assemble(self, sequences):
        """
        Assemble a list of sequences, and return a list of the assembled sequences and a set of the sequences which were used in the assembly
        """
        pass


class StubContigAssembler(ContigAssembler):
    def assemble(self, sequences):
        return [], set()


class NaiveContigAssembler(ContigAssembler):
    def __init__(self, kmer_size, min_confidence, num_kmers_to_assemble):
        super(NaiveContigAssembler, self).__init__()
        self.kmer_size = kmer_size
        self.min_confidence = min_confidence
        self.use_min_confidence = (self.min_confidence > 0)
        self.num_kmers_to_assemble = num_kmers_to_assemble

    def _get_kmers(self, sequences):
        fixed_size_sequences = dict()
        kmer_idx = 1
        for sequence in sequences:
            unique_fixed_size_sequences = set()
            for i in range(0, len(sequence) - self.kmer_size + 1):
                fixed_size_sequence = sequence.sequence[i:i + self.kmer_size]
                if self.use_min_confidence and min(sequence.confidence[i:i + self.kmer_size]) < self.min_confidence:
                    continue
                # avoid cases where the same sub-sequence appears more than once in the same sequence, for example: 'LLLLLLL'
                if fixed_size_sequence in unique_fixed_size_sequences:
                    continue
                unique_fixed_size_sequences.add(fixed_size_sequence)
                if not fixed_size_sequences.has_key(fixed_size_sequence):
                    fixed_size_sequences[fixed_size_sequence] = Kmer(identifier=kmer_idx, kmer=fixed_size_sequence)
                    kmer_idx += 1
                fixed_size_sequences[fixed_size_sequence].add(
                    OverlappingSequence(sequence=sequence, before=i, after=len(sequence) - i))
        kmers = sorted(fixed_size_sequences.values(), key=lambda x: x.confidence_score, reverse=True)
        return kmers

    def assemble(self, sequences):
        kmers = self._get_kmers(sequences)
        return self._assemble_kmers(kmers)

    def _assemble_kmers(self, kmers):
        new_sequences = []
        old_sequences = set()
        for kmer in kmers:
            if len(kmer.overlapping_sequences) > 1:
                overlapping_sequences = set([overlapping_sequence.sequence
                                             for overlapping_sequence in kmer.overlapping_sequences])
                previously_assembled = old_sequences.intersection(overlapping_sequences)
                if previously_assembled:
                    logger.debug('Skipping kmer %s since %d of its overlapping sequences were already assembled' %
                                 (kmer, len(previously_assembled)))
                    continue
                new_sequences.append(self.join_sequences(kmer))
                old_sequences = old_sequences.union(overlapping_sequences)
                if len(new_sequences) >= self.num_kmers_to_assemble:
                    break
        return new_sequences, old_sequences

    def join_sequences(self, kmer):
        logger.debug('Starting merge - Kmer %s' % (kmer.kmer,))
        contig = SequenceJoiner().join(list(kmer.overlapping_sequences))
        logger.debug('Merge done for Kmer %s - final sequence: %s' % (kmer.kmer, contig))
        return contig


class SequenceJoiner(object):
    def aggregate_confidence(self, old_confidence, new_confidence):
        return old_confidence + new_confidence  # use simple sum here

    # return max(old_confidence, new_confidence)

    def join(self, overlapping_sequences):  # TODO: simplify this method (maybe in a new class, plus add tests)
        assert overlapping_sequences, 'No sequences'
        max_before = max([overlapping_sequence.before for overlapping_sequence in overlapping_sequences])
        max_after = max([overlapping_sequence.after for overlapping_sequence in overlapping_sequences])
        joined_length = max_before + max_after
        # overlapping_sequences = sorted(list(kmer.overlapping_sequences), key=lambda x: x.start, reverse=True)
        joined_identifier = set()
        joined_confidence = [None] * joined_length
        joined_sequence_options = [{} for _ in range(joined_length)]
        for overlapping_sequence in overlapping_sequences:
            sequence = overlapping_sequence.sequence
            offset = max_before - overlapping_sequence.before
            joined_identifier = joined_identifier.union(sequence.identifier)
            for i in range(len(sequence)):
                single_acid_options = sequence.sequence_options[i]
                for acid, acid_confidence in single_acid_options.items():
                    if acid not in joined_sequence_options[offset + i]:
                        joined_sequence_options[offset + i][acid] = acid_confidence
                    else:
                        joined_sequence_options[offset + i][acid] += acid_confidence  # currently use simple sum here
                        # joined_sequence_options[offset+i][acid] = self.aggregate_confidence(joined_sequence_options[offset+i][acid], acid_confidence) # currently use simple sum here
        joined_sequence = bytearray(joined_length)  # use the mutable bytearray instead of string
        for i, single_acid_options in enumerate(joined_sequence_options):
            assert single_acid_options
            single_acid_options = OrderedDict(sorted(single_acid_options.items(), key=lambda x: x[1], reverse=True))
            best_acid, best_acid_confidence = single_acid_options.items()[0]
            joined_sequence[i] = best_acid
            joined_confidence[i] = best_acid_confidence
        sequences = [op.sequence for op in overlapping_sequences]
        contig = Contig(identifier=joined_identifier, sequence=str(joined_sequence), confidence=joined_confidence,
                        sequence_options=joined_sequence_options, sequences=sequences)
        return contig


class EdgesAssembler(Assembler):
    def __init__(self, min_overlap, max_overlap, max_num_iterations):
        super(EdgesAssembler, self).__init__()
        assert not min_overlap > max_overlap, 'max overlap [%d] should not be less then min overlap' % (
            max_overlap, min_overlap)
        assert min_overlap >= 1, 'min overlap [%d] should be a positive number' % (min_overlap,)
        self.min_overlap = min_overlap
        self.max_overlap = max_overlap
        self.max_num_iterations = max_num_iterations

    def assemble(self, sequences):
        assert sequences, "No sequences"
        logger.info("EdgesAssembler started with %d sequences" % (len(sequences)))
        assembled_sequences = set(sequences)
        for overlap_size in range(self.max_overlap, self.min_overlap - 1, -1):
            for num_iterations in range(1, self.max_num_iterations + 1):
                if len(assembled_sequences) == 1:
                    # single sequence left, no point in proceeding
                    break
                logger.debug("Iteration #%d started - with %d sequences and overlap size %d" % (
                    num_iterations, len(assembled_sequences), overlap_size))
                new_assembled_sequences, old_sequences = self.assemble_contig(assembled_sequences, overlap_size)
                if not new_assembled_sequences:
                    # could not assemble a contig from remaining sequences, no point in proceeding with the same overlap size
                    logger.debug("Iteration #%d ended with %d sequences, trying with a smaller overlap" % (
                        num_iterations, len(assembled_sequences)))
                    break
                assembled_sequences = assembled_sequences.difference(old_sequences)
                assembled_sequences = assembled_sequences.union(new_assembled_sequences)
                logger.debug("Iteration #%d ended with %d sequences" % (num_iterations, len(assembled_sequences)))
        logger.info("Assembly ended after %d iterations with %d sequences" % (num_iterations, len(assembled_sequences)))
        return assembled_sequences

    def assemble_contig(self, sequences, overlap_size):
        # return EdgesContigAssembler(overlap_size=overlap_size).assemble(sequences)
        return EdgesContigAssembler(overlap_size=overlap_size).assemble(sequences)


Edge = namedtuple('Edge', field_names=('sequence', 'side', 'origin'))

EdgesDistanceResult = namedtuple('EdgesDistanceResult', field_names=('left', 'right', 'distance'))


class EdgesContigAssembler(ContigAssembler):
    def __init__(self, overlap_size):
        super(EdgesContigAssembler, self).__init__()
        self.overlap_size = overlap_size
        # self.max_distance = min(int(self.overlap_size * (1 - (0.9 ** self.overlap_size))), int(self.overlap_size * 0.6)) # OAOA TODO refine this threshold
        self.max_distance = int(self.overlap_size * 0.25)
        logger.debug('overlap size [%d] max distance [%d]' % (self.overlap_size, self.max_distance))

    def assemble(self, sequences):
        """
        Take the two edges of each sequences of fixed length, and try to find the nearest edges using Levenshtein distance algorithm and other algorithms such as mass calculation
        """
        assert len(sequences) > 1, 'assembly works with more than one sequence but got [%d]' % (len(sequences),)
        left_edges = []
        right_edges = []
        for sequence in sequences:
            if len(sequence) < self.overlap_size:
                continue
            left_edges.append(Edge(sequence=sequence.sequence[:self.overlap_size], side='left', origin=sequence))
            right_edges.append(Edge(sequence=sequence.sequence[-self.overlap_size:], side='right', origin=sequence))
        return self._assemble_edges(left_edges, right_edges)

    def distance(self, left_edge, right_edge):
        # return LevenshteinDistance(left_edge.sequence, right_edge.sequence).calculate()
        return MassDistance(left_edge.sequence, right_edge.sequence).calculate()

    def _assemble_edges(self, left_edges, right_edges):
        new_sequences = []
        old_sequences = set()
        if len(left_edges) < 2:
            return new_sequences, old_sequences
        distance_results = []
        for (i, left_edge) in enumerate(left_edges):
            for (j, right_edge) in enumerate(right_edges):
                if i == j:
                    continue  # no point in trying to assemble edges of the same contig
                distance_results.append(
                    EdgesDistanceResult(left_edge, right_edge, self.distance(left_edge, right_edge)))
        distance_results.sort(key=lambda x: x.distance, reverse=False)
        best_result = distance_results[0]
        logger.debug('all levenshtein distances: %s' % ([r.distance for r in distance_results]))
        logger.debug('best levenshtein result: %s' % (best_result,))
        if best_result.distance <= self.max_distance:
            new_sequences = [self.join_sequences(best_result.left, best_result.right)]
            old_sequences = {best_result.left.origin, best_result.right.origin}
        return new_sequences, old_sequences

    def join_sequences(self, left_edge, right_edge):
        overlapping_sequences = [OverlappingSequence(sequence=left_edge.origin, before=0, after=len(left_edge.origin)),
                                 OverlappingSequence(sequence=right_edge.origin,
                                                     before=len(right_edge.origin) - self.overlap_size,
                                                     after=self.overlap_size)]
        logger.debug('Starting merge - left %s right %s' % (left_edge, right_edge))
        contig = SequenceJoiner().join(overlapping_sequences)
        return contig


class MassDistance(object):
    def __init__(self, sequence_a, sequence_b):
        self.sequence_a = sequence_a
        self.sequence_b = sequence_b

    def calculate(self):
        i = 0
        assert len(self.sequence_a) == len(self.sequence_b), 'sequence sizes differ: [%s] [%s]' \
                                                             % (self.sequence_a, self.sequence_b)
        distance = sequence_size = len(self.sequence_a)
        while i < sequence_size:
            found_match = False
            for k in range(1, min(3, sequence_size - i + 1)):
                if calc_mass(self.sequence_a[i:i + k]) == calc_mass(self.sequence_b[i:i + k]):
                    found_match = True
                    break
            if found_match:
                distance -= k
                i += k
            else:
                i += 1
        return distance


class LevenshteinDistance(object):
    def __init__(self, sequence_a, sequence_b):
        self.sequence_a = sequence_a
        self.sequence_b = sequence_b

    def calculate(self):
        return Levenshtein.distance(self.sequence_a, self.sequence_b)


class CompositeAssembler(Assembler):
    def __init__(self, min_evidence=1):
        super(CompositeAssembler, self).__init__()
        self.min_evidence = min_evidence

    def assemble(self, sequences):
        # first assembly
        assembled_sequences = MultiPassAssembler(kmer_sizes=(11, 9, 7), min_confidence=80, max_num_iterations=400,
                                                 num_kmers_to_use_per_iteration=200).assemble(sequences)
        # take only the contigs
        assembled_sequences = [sequence for sequence in assembled_sequences if isinstance(sequence, Contig)]
        # assemble on edges
        assembled_sequences = EdgesAssembler(min_overlap=8, max_overlap=20, max_num_iterations=50).assemble(
            assembled_sequences)
        # re-assemble contigs
        assembled_sequences = MultiPassAssembler(kmer_sizes=(6,), min_confidence=-1, max_num_iterations=100,
                                                 num_kmers_to_use_per_iteration=200).assemble(assembled_sequences)
        # re-assemble on edges
        assembled_sequences = EdgesAssembler(min_overlap=6, max_overlap=12, max_num_iterations=50).assemble(
            assembled_sequences)
        # throw away contigs with small evidence
        assembled_sequences = [contig for contig in assembled_sequences if contig.num_peptides > self.min_evidence]
        return assembled_sequences


from dips.align import LevenshteinAlignmentScorer, NaiveAligner, AlignmentScorer, AlignmentSwapScorer
from dips.data import AMINO_ACID_REPLACEMENTS


class KmersHandler(object):
    def __init__(self, kmer_size=7, protein=None):
        self.kmer_size = kmer_size
        self.protein = protein
        # Levenshtein aligner:
        leven_scorer = LevenshteinAlignmentScorer(alignment_per_amino_acid_accuracy=0.98)
        self.leven_aligner = NaiveAligner(proteins=self.protein, scorer=leven_scorer, stop_on_first_alignment=False)
        KmerContig.leven_aligner = self.leven_aligner
        # naive aligner:
        naive_scorer = AlignmentScorer(accuracy_threshold=0.8, min_confidence=0)
        self.naive_aligner = NaiveAligner(proteins=[], scorer=naive_scorer, stop_on_first_alignment=False)
        KmerContig.naive_aligner = self.naive_aligner
        # swap aligner:
        swap_scorer = AlignmentSwapScorer(accuracy_threshold=0.8, min_confidence=0)
        self.swap_aligner = NaiveAligner(proteins=[], scorer=swap_scorer, stop_on_first_alignment=False)
        KmerContig.swap_aligner = self.swap_aligner

        KmerContig.protein = self.protein
        KmerContig.kmer_size = self.kmer_size

    def get_kmers(self, sequences):
        logger.info("starting to build kmers...")
        fixed_size_sequences = dict()
        kmer_idx = 1
        for sequence in sequences:
            unique_fixed_size_sequences = set()
            for i in range(0, len(sequence) - self.kmer_size + 1):
                fixed_size_sequence = sequence.sequence[i:i + self.kmer_size]
                if fixed_size_sequence in unique_fixed_size_sequences:
                    continue
                unique_fixed_size_sequences.add(fixed_size_sequence)
                if not fixed_size_sequences.has_key(fixed_size_sequence):
                    fixed_size_sequences[fixed_size_sequence] = Kmer(identifier=kmer_idx, kmer=fixed_size_sequence)
                    kmer_idx += 1
                fixed_size_sequences[fixed_size_sequence].add(
                    OverlappingSequence(sequence=sequence, before=i, after=i + self.kmer_size))
        kmers = sorted(fixed_size_sequences.values(), key=lambda x: x.num_sequences, reverse=True)
        logger.info("finished building kmers")
        return kmers

    def _find_overlap(self, kmer_contig, temp_original_kmers, current_kmers, min_overlap, go_right=True):
        contig = bytearray(kmer_contig.sequence)
        # extend contig to the right:
        result = None
        found_overlap = False
        for i in range(1, self.kmer_size - min_overlap + 1):
            if go_right:
                seq_to_compare = contig[-(self.kmer_size - i):]
            else:
                seq_to_compare = contig[:(self.kmer_size - i)]
            for temp_kmer_idx in range(len(temp_original_kmers)):
                candidate_kmer = temp_original_kmers[temp_kmer_idx]
                # found an overlap?
                if go_right:
                    if seq_to_compare == candidate_kmer.kmer[:(self.kmer_size - i)]:
                        contig.extend(candidate_kmer.kmer[(self.kmer_size - i):])
                        found_overlap = True
                        del temp_original_kmers[temp_kmer_idx]
                        result = contig
                        offset = i
                        break
                else:
                    if seq_to_compare == candidate_kmer.kmer[-(self.kmer_size - i):]:
                        contig[0:0] = candidate_kmer.kmer[:-(self.kmer_size - i)]
                        found_overlap = True
                        del temp_original_kmers[temp_kmer_idx]
                        result = contig
                        offset = -i
                        break
            if found_overlap:
                logger.debug("found overlap kmer: " + str(candidate_kmer.kmer) + ", contig grew to: " + str(contig))
                # extend kmer contig:
                kmer_contig.add_kmer(candidate_kmer, offset)
                # delete the same kmer from current kmers list too:
                for current_kmer_idx in range(len(current_kmers)):
                    if candidate_kmer.kmer == current_kmers[current_kmer_idx].kmer:
                        del current_kmers[current_kmer_idx]
                        break
                result = (candidate_kmer, i)
                break
        return result

    def assemble_kmers(self, kmers, min_overlap=6):
        logger.info("starting to assemble kmers to contigs...")
        num_kmers = len(kmers)
        original_kmers = kmers
        current_kmers = original_kmers[:]
        contigs = []
        kmer_idx = 0
        while len(current_kmers) > 0:
            # percentage = (kmer_idx/float(num_kmers))*100
            # logger.info("%"+"%.1f" %percentage)
            kmer_idx += 1
            kmer_contig = KmerContig(identifier="contig" + str(kmer_idx), kmer=current_kmers[0])
            logger.debug("kmer " + str(kmer_idx) + ": starting with kmer: " + str(kmer_contig.sequence))
            del current_kmers[0]
            temp_original_kmers = original_kmers[:]

            logger.debug("extending to the right and then left")
            # extend to the right and then to the right:
            for right in [True, False]:
                logger.debug("extending to right=%s" % right)
                one_counter = 0
                ones_addition_size = 0
                two_present = False
                result = self._find_overlap(kmer_contig, temp_original_kmers, current_kmers, min_overlap, go_right=right)
                while result is not None:
                    # remove extensions of unreliable kmers:
                    if result[0].num_sequences == 1 or (not two_present and
                                                        result[0].num_sequences == 2 and
                                                        result[0].average_conf < 60):
                        if result[0].num_sequences == 2:
                            two_present = True
                        one_counter += 1
                        ones_addition_size += result[1]
                        if min_overlap-one_counter <= 2:
                            logger.debug("one/low two counter exceeded his limit removing wrong tail of size: %s"
                                        % ones_addition_size)
                            if not right:
                                ones_addition_size *= -1
                            kmer_contig.cut_edge(ones_addition_size)
                            break
                    else:
                        one_counter = 0
                        ones_addition_size = 0
                        two_present = False
                    result = self._find_overlap(kmer_contig, temp_original_kmers, current_kmers, min_overlap,
                                                go_right=right)

            contigs.append(kmer_contig)
            logger.debug(str(kmer_idx) + ": finished with contig:" + str(kmer_contig.sequence))
        # remove duplicates(just incase):
        # contigs = s = sorted(contigs, key = lambda x: (len(x.peptides), x.avg_conf), reverse=True)
        for contig in contigs:
            contig.update_confidence()
        logger.info("finished assembling kmers to contigs")
        return contigs

    def merge_contigs(self, kmer_contigs, file_path, min_quality=0.7):
        logger.info("starting splitting to families...")
        # try sorting the first 10 contigs by num peptides:
        head_contigs_num = int(len(kmer_contigs) / 200)
        logger.debug("sorting head of list: " + str(head_contigs_num) + " contigs")
        kmer_contigs = sorted(kmer_contigs[:head_contigs_num], key=lambda x: len(x.peptides),
                              reverse=True) + kmer_contigs[head_contigs_num:]
        # kmer_contigs = sorted(kmer_contigs, key=lambda x: len(x.peptides), reverse=True)
        # split to families:
        current_kmer_contigs = kmer_contigs[:]
        kmer_contigs_families = []
        scorer = LevenshteinAlignmentScorer(alignment_per_amino_acid_accuracy=0.98)
        # scorer = AlignmentScorer(accuracy_threshold=0.8, min_confidence=0)
        aligner = NaiveAligner(proteins=[], scorer=scorer, stop_on_first_alignment=False)
        while len(current_kmer_contigs) > 0:
            kmer_contig = current_kmer_contigs[0]
            logger.debug(
                "starting with kmer contig: " + str(kmer_contig.single_identifier) + ": " + str(kmer_contig.sequence))
            del current_kmer_contigs[0]
            idxs = []
            contig_family = [kmer_contig]
            for (i, candidate_kmer_contig) in enumerate(current_kmer_contigs):
                alignment = aligner.align_to_protein(kmer_contig, candidate_kmer_contig)
                if alignment and alignment.quality > min_quality:
                    logger.debug("found similar candidate_kmer_contig: " + str(
                        candidate_kmer_contig.single_identifier) + ": " + str(candidate_kmer_contig.sequence))
                    logger.debug("align quality: " + str(alignment.quality))
                    idxs.append(i)
                    contig_family.append(candidate_kmer_contig)
                    position = alignment.position
                    # TODO: maybe don't cut the edges if reverse contains.
                    # if offset is negative - cut candidate left:
                    if alignment.position < 0:
                        candidate_kmer_contig.cut_edge(alignment.position)
                        position = 0
                    # if candidate is longer than absorbing contig - cut candidate right:
                    if len(candidate_kmer_contig) + position > len(kmer_contig):
                        candidate_kmer_contig.cut_edge(len(candidate_kmer_contig) + position - len(kmer_contig))
                    kmer_contig.absorb_contig(candidate_kmer_contig, position)
            current_kmer_contigs = [new_kmer_contig for (j, new_kmer_contig) in enumerate(current_kmer_contigs) if
                                    j not in idxs]
            kmer_contigs_families.append(contig_family)
            logger.debug(
                "finished with kmer contig: " + str(kmer_contig.single_identifier) + ": " + str(kmer_contig.sequence))
        # write to file:
        with open(file_path, 'w') as file_handle:
            for kmer_contigs_family in kmer_contigs_families:
                for kmer_contig in kmer_contigs_family:
                    file_handle.write(kmer_contig.sequence + "\n")
                file_handle.write("=================================\n")
        merged_contigs = [kmer_contigs_family[0] for kmer_contigs_family in kmer_contigs_families]
        for merged_contig in merged_contigs:
            merged_contig.update_confidence()
        logger.info("number of families: " + str(len(kmer_contigs_families)))
        logger.info("finished splitting to families")
        return merged_contigs

    @staticmethod
    def find_substring_with_mismatches(string, sub_string, allowed_mismatches=0):
        if len(string) < len(sub_string):
            return -1
        for i in range(len(string)-len(sub_string)+1):
            mismatches = 0
            for j in range(len(sub_string)):
                if sub_string[j] != string[i+j]:
                    mismatches += 1
                    if mismatches > allowed_mismatches:
                        break
            if mismatches <= allowed_mismatches:
                return i
        return -1

    def _check_overlap_helper(self, contig, candidate_contig, overlap_size, right, only_edges, num_mismatch=None):
        only_edges_max_dist = 3
        if right:
            # check if it's a match:
            if num_mismatch is None:
                overlap = contig[-overlap_size:]
                if only_edges:
                    start = 0
                    end = overlap_size + only_edges_max_dist
                else:
                    start = end = None
                match_idx = candidate_contig.find(overlap, start, end)
            else:
                overlap = contig[-overlap_size:]
                match_idx = self.find_substring_with_mismatches(candidate_contig, overlap,
                                                                allowed_mismatches=num_mismatch)
            if match_idx != -1:
                # if candidate contains contig:
                if match_idx + overlap_size > len(contig):
                    # contig[:-overlap_size] == candidate_contig[match_idx+overlap_size-len(contig):match_idx]):
                    if num_mismatch is None and contig[:-overlap_size] == candidate_contig[match_idx + overlap_size - len(contig):match_idx]:
                        return {'contig': candidate_contig, 'absorb_offset': candidate_contig.find(contig, start, end)}
                    else:
                        return None
                else:
                    # extend to the right:
                    contig.extend(candidate_contig[match_idx + overlap_size:])
                    return {'contig': contig, 'offset': len(candidate_contig) - (match_idx + overlap_size)}
        else:
            # check if it's a match
            if num_mismatch is None:
                overlap = contig[:overlap_size]
                if only_edges:
                    start = len(candidate_contig) - (overlap_size + only_edges_max_dist)
                    end = len(candidate_contig)
                else:
                    start = end = None
                match_idx = candidate_contig.find(overlap, start, end)
            else:
                overlap = contig[:overlap_size]
                match_idx = self.find_substring_with_mismatches(candidate_contig, overlap,
                                                                allowed_mismatches=num_mismatch)
            if match_idx != -1:
                # if candidate contains contig:
                if len(candidate_contig) - match_idx > len(contig):
                    # contig == candidate_contig[match_idx:match_idx+len(contig)]):
                    if num_mismatch is None and contig == candidate_contig[match_idx:match_idx + len(contig)]:
                        return {'contig': candidate_contig, 'absorb_offset': candidate_contig.find(contig, start, end)}
                    else:
                        return None
                else:
                    # extend to the left:
                    contig[0:0] = candidate_contig[:match_idx]
                    return {'contig': contig, 'offset': -match_idx}
        return None

    def _check_overlap(self, united_kmer_contig, candidate_kmer_contig,
                       overlap_size, right, only_edges, edge_cut_size=None, swap_acids=None, mass_swap=None, num_mismatch=None):
        contig = bytearray(united_kmer_contig.sequence)
        candidate_contig = bytearray(candidate_kmer_contig.sequence)
        result = None
        # overlap size check:
        if overlap_size > len(contig) or overlap_size > len(candidate_contig):
            # logger.info("main or candidate are too small: "+str(candidate_contig))
            return None
        # start checking overlapping:
        if edge_cut_size is not None:
            if edge_cut_size != 0:
                contig = contig[:-edge_cut_size] if right else contig[edge_cut_size:]
            result = self._check_overlap_helper(contig, candidate_contig, overlap_size, right, only_edges, num_mismatch=num_mismatch)
            if result is not None:
                return result
        if swap_acids is True:
            contig_copy = bytearray(contig)
            # swap along the overlap until it found or tried all:
            swap_rang = overlap_size-1 if only_edges else 1
            for i in range(swap_rang):
                contig = bytearray(contig_copy)
                if right:
                    temp_char = contig[len(contig) - 1 - i]
                    contig[len(contig) - 1 - i] = contig[len(contig) - 2 - i]
                    contig[len(contig) - 2 - i] = temp_char
                else:
                    temp_char = contig[i + 0]
                    contig[i + 0] = contig[i + 1]
                    contig[i + 1] = temp_char
                result = self._check_overlap_helper(contig, candidate_contig, overlap_size, right, only_edges, num_mismatch=num_mismatch)
                if result is not None:
                    return result
        if mass_swap is True:
            pass
        return result

    def _unite_contig_helper(self, united_kmer_contig, current_kmer_contigs, overlap_size, min_len_gap,
                             deletion_sizes, only_edges, side, swap_acids=None, num_mismatch=None):
        # check 1 and then 2 edge deletion:
        for deletion_size in deletion_sizes:
            for (i, candidate_kmer_contig) in enumerate(current_kmer_contigs):
                found = self._check_overlap(united_kmer_contig, candidate_kmer_contig,
                                            overlap_size, right=side, only_edges=only_edges,
                                            edge_cut_size=deletion_size, swap_acids=swap_acids,
                                            num_mismatch=num_mismatch)
                if found is not None:
                    new_contig = found['contig']
                    del current_kmer_contigs[i]
                    if len(new_contig) >= len(united_kmer_contig) + min_len_gap:
                        logger.debug("found matching contig " + candidate_kmer_contig.single_identifier + ".")
                        adjusted_deletion_size = deletion_size if side else -deletion_size
                        united_kmer_contig.cut_edge(adjusted_deletion_size)
                        if swap_acids is not None:
                            united_kmer_contig.swap_edge_residues(side)
                        if 'absorb_offset' in found:
                            logger.debug('cut edge:: absorb contig')
                            candidate_kmer_contig.identifier = [united_kmer_contig.single_identifier]
                            candidate_kmer_contig.absorb_contig(united_kmer_contig, found['absorb_offset'])
                            return candidate_kmer_contig
                        else:
                            logger.debug('cut edge:: add contig')
                            # cut left or right of candidate and leave only overlap:
                            if side:  # if right else left
                                if len(candidate_kmer_contig) - found['offset'] > overlap_size:
                                    candidate_kmer_contig.cut_edge(
                                        -(len(candidate_kmer_contig) - found['offset'] - overlap_size))
                            else:
                                if len(candidate_kmer_contig) + found['offset'] > overlap_size:
                                    candidate_kmer_contig.cut_edge(
                                        len(candidate_kmer_contig) + found['offset'] - overlap_size)
                            # add leftover contig to the right/left:
                            united_kmer_contig.add_contig(candidate_kmer_contig, found['offset'])
                            return united_kmer_contig
                        break
        return None

    def _unite_contig(self, united_kmer_contig, current_kmer_contigs, overlap_size, min_len_gap,
                      deletion_sizes, only_edges):
        # do first for right side and then left:
        for side in [True, False]:
            result = self._unite_contig_helper(united_kmer_contig, current_kmer_contigs, overlap_size, min_len_gap,
                                               deletion_sizes, only_edges, side, swap_acids=None, num_mismatch=None)
            if result:
                return result
            result = self._unite_contig_helper(united_kmer_contig, current_kmer_contigs, overlap_size, min_len_gap,
                                                   deletion_sizes, only_edges, side, swap_acids=True, num_mismatch=None)
            if result:
                return result
        # after nothing was found, try mismatch
        # for side in [True, False]:
        #     result = self._unite_contig_helper(united_kmer_contig, current_kmer_contigs, 15, min_len_gap,
        #                                            deletion_sizes, only_edges, side, swap_acids=None, num_mismatch=2)
        #     if result:
        #         return result
        return None

    def unite_contigs(self, kmer_contigs, overlap_size=7, min_len_gap=1, only_edges=False):
        for i in range(overlap_size, overlap_size + 1):
            logger.info("starting unite contigs with overlap size: " + str(i))
            kmer_contigs = self._internal_unite_contigs(kmer_contigs, overlap_size=i, min_len_gap=min_len_gap,
                                                        only_edges=only_edges)
        for contig in kmer_contigs:
            contig.update_confidence()
        logger.info("finished uniting contigs")
        return kmer_contigs

    def _internal_unite_contigs(self, kmer_contigs, overlap_size=7, min_len_gap=1, only_edges=False):
        logger.info("starting to unite contigs...")
        current_kmer_contigs = kmer_contigs[:]
        united_kmer_contigs = []
        while (len(current_kmer_contigs) > 0):
            united_kmer_contig = current_kmer_contigs[0]
            logger.debug("contig " + str(united_kmer_contig.single_identifier) + ": starting with contig: " + str(
                united_kmer_contig.sequence))
            del current_kmer_contigs[0]
            keep_uniting = True
            while (keep_uniting):
                deletion_sizes = [0, 1, 2]
                if overlap_size < 5 and only_edges:
                    deletion_sizes = [0, 1, 2]
                    only_edges = True
                returned_contig = self._unite_contig(united_kmer_contig, current_kmer_contigs, overlap_size,
                                                     min_len_gap, deletion_sizes=deletion_sizes, only_edges=only_edges)
                if (returned_contig == None):
                    keep_uniting = False
                else:
                    united_kmer_contig = returned_contig
                    logger.debug(
                        "contig " + str(united_kmer_contig.single_identifier) + ": contig was united to: " + str(
                            united_kmer_contig.sequence))
            united_kmer_contigs.append(united_kmer_contig)
            logger.debug("contig " + str(united_kmer_contig.single_identifier) + ": finished with contig: " + str(
                united_kmer_contig.sequence))
        return united_kmer_contigs

    def extract_unique_contigs(self, kmer_contigs, intersect_bound=0.785, min_peptides_bound=150,
                               min_peptides_distribution=9):
        logger.info("starting to extract final contigs")

        # head_contigs_num = int(len(kmer_contigs) * 0.01)
        # logger.debug("sorting head of list: " + str(head_contigs_num) + " contigs")
        # sorted_kmer_contigs = sorted(kmer_contigs[:head_contigs_num], key=lambda x: len(x.peptides),
        #                       reverse=True) + kmer_contigs[head_contigs_num:]

        sorted_kmer_contigs = sorted(kmer_contigs, key=lambda x: (len(x.peptides)/float(len(x))), reverse=True)
        intersect_percentages = []
        used_peptides = set()
        for kmer_contig in sorted_kmer_contigs:
            current_peptides = kmer_contig.peptides
            num_intersect_peptides = len(current_peptides.intersection(used_peptides))
            logger.debug("%s, used peptides=%s" % (kmer_contig.single_identifier, num_intersect_peptides / float(len(current_peptides))))
            if (num_intersect_peptides / float(len(current_peptides)) < intersect_bound
                and len(current_peptides) > min_peptides_bound and len(current_peptides) / float(
                    len(kmer_contig)) > min_peptides_distribution):
                used_peptides.update(current_peptides)
                intersect_percentages.append((num_intersect_peptides / float(len(kmer_contig.peptides)), kmer_contig))
                logger.debug("extracted %s" % kmer_contig.single_identifier)
        sorted_intersect_percentages = sorted(intersect_percentages, key=lambda x: x[0])
        final_contigs = [sorted_intersect_percentage[1] for sorted_intersect_percentage in sorted_intersect_percentages]
        logger.info("finished extracting final contigs")
        return final_contigs

    """
    goes over the kmer contigs, aligns their peptides to their sequence and fix them.
    fixes types:
    1. replace acid if it's not max variant to max variant.
    2. cut edges if there isn't enough sites for that acid.
    """

    def fix_contigs(self, kmer_contigs, cleaned_peptides=None, count_bound=10, output_file=None, dry_mode=False,
                    variants_collision_bound=0.7):
        if dry_mode == False:
            logger.info('starting to fix contigs...')
            if cleaned_peptides and count_bound is not None:
                count_bound = len(cleaned_peptides)*0.0004
                logger.debug("count bound: %s" % count_bound)
        if output_file != None:
            logger.info('starting to write contigs report...')
        if output_file != None:
            output_file_handle = open(output_file, 'w')
        contigs_replacements = []
        contigs_variants_collisions = []
        peptides_alignments_list = []
        # contigs_modifications = []
        for kmer_contig in kmer_contigs:
            contig_replacements = []
            contig_variants_collisions = []
            # contig_modifications = []
            peptides_alignments = kmer_contig.get_peptides_alignments() if cleaned_peptides == None else \
                kmer_contig.get_peptides_alignments(my_peptides=cleaned_peptides)
            peptides_alignments_list.append(peptides_alignments)
            max_variants = []
            contig_counts = []
            if (output_file != None):
                output_file_handle.write(kmer_contig.sequence + "\n")
                output_file_handle.write("num peptides:" + str(len(kmer_contig.peptides)) + "\n")
                output_file_handle.write("num peptides alignments:" + str(len(peptides_alignments)) + "\n")
            for i in range(len(kmer_contig)):
                # get relevant peptides alignments and init:
                contig_acid = chr(kmer_contig.sequence[i])
                # print "now i = "+str(i), contig_acid
                filtered_peptides_alignments = [alignment for alignment in peptides_alignments if
                                                alignment.position <= i < len(alignment.sequence) + alignment.position]
                variants_dict = {contig_acid: 0}
                replacements = {contig_acid: {ACID_TYPE: {}, TRYPSIN_TYPE: {}}}
                # modifications = {contig_acid: {'modified_count': 0, 'total_count': 0}}
                for peptide_alignment in filtered_peptides_alignments:
                    # fill variants dict:
                    peptide_acid = peptide_alignment.sequence.sequence[i - peptide_alignment.position]
                    peptide_type = peptide_alignment.sequence.peptide_type
                    if peptide_acid in variants_dict:
                        variants_dict[peptide_acid] += 1
                    else:
                        variants_dict[peptide_acid] = 1

                    # # fill modifications:
                    # if peptide_acid in modifications:
                    #     modifications[peptide_acid]['total_count'] += 1
                    # else:
                    #     modifications[peptide_acid] = {'modified_count': 0, 'total_count': 1}
                    # if i - peptide_alignment.position in peptide_alignment.sequence.modifications:
                    #     modifications[peptide_acid]['modified_count'] += 1

                    # fill replacements dict:
                    orig_peptide_acid = str(
                        peptide_alignment.sequence.original_sequence[i - peptide_alignment.position])
                    if peptide_acid not in replacements:
                        replacements[peptide_acid] = {ACID_TYPE: {}, TRYPSIN_TYPE: {}}

                    if peptide_acid == 'E' and i - peptide_alignment.position in peptide_alignment.sequence.modifications:
                        orig_peptide_acid = MODIFICATION
                    if orig_peptide_acid in replacements[peptide_acid][peptide_type]:
                        replacements[peptide_acid][peptide_type][orig_peptide_acid] += 1
                    else:
                        replacements[peptide_acid][peptide_type][orig_peptide_acid] = 1
                # get max variant:
                sorted_variants = sorted(variants_dict.iteritems(), key=operator.itemgetter(1), reverse=True)
                max_variant = sorted_variants[0]
                second_max_variant = sorted_variants[1] if len(sorted_variants) > 1 else None
                max_variants.append(max_variant)
                contig_counts.append(sum(variants_dict.values()))
                # if the max variant doesn't match the contig_acid - change this acid on contigs sequence:
                if dry_mode is False and max_variant[0] != contig_acid:
                    if not (second_max_variant is not None and second_max_variant[0] == contig_acid and
                            max_variant[1] == second_max_variant[1]):
                        kmer_contig.sequence[i] = max_variant[0]
                        contig_acid = max_variant[0]
                    else:
                        temp = max_variant
                        max_variant = second_max_variant
                        second_max_variant = temp

                # # save modifications data:
                # modification = modifications[contig_acid]
                # contig_modifications.append(modification)

                # save replacement data:
                replacements = replacements[contig_acid]
                contig_replacement = []
                replacement_found = False
                for acid_rep_key in AMINO_ACID_REPLACEMENTS:
                    if acid_rep_key in replacements[ACID_TYPE] or acid_rep_key in replacements[TRYPSIN_TYPE]:
                        replacement_found = True
                        break
                if replacement_found:
                    contig_replacement.append(contig_acid)
                    contig_replacement.append(replacements[ACID_TYPE])
                    contig_replacement.append(replacements[TRYPSIN_TYPE])
                contig_replacements.append(contig_replacement)
                # save variants collisions:
                contig_variants_collision = [max_variant]
                if (second_max_variant is not None and second_max_variant[1] / float(
                        max_variant[1]) > variants_collision_bound):
                    contig_variants_collision.append(second_max_variant)
                contig_variants_collisions.append(contig_variants_collision)
                if (output_file != None):
                    output_file_handle.write(
                        "[" + str(i) + "]" + contig_acid + ", rep: " + str(replacements) + ", sorted variants: " +
                        str(sorted_variants) + "\n")
            # write overrepresented variants as contig and low count variants:
            if output_file != None:
                max_variants_contig = "".join(zip(*max_variants)[0])
                output_file_handle.write("max variants contig: " + str(max_variants_contig) + "\n")
                # print the max variants with count less then cbound:
                if count_bound is not None:
                    low_count_max_variants = [(idx, max_variant) for (idx, max_variant) in enumerate(max_variants) if
                                              max_variant[1] <= count_bound]
                    output_file_handle.write(
                        "max variants less then " + str(count_bound) + ": " + str(low_count_max_variants) + "\n")
            # cut edges if needed:
            if dry_mode == False and count_bound is not None:
                # from left:
                stop_idx = 0
                for (i, contig_count) in enumerate(contig_counts):
                    logger.debug("(i:%s, count:%s)" % (i, contig_count))
                    if (contig_count > count_bound):
                        stop_idx = i
                        break
                kmer_contig.cut_edge(-stop_idx)
                contig_replacements = contig_replacements[stop_idx:]
                contig_variants_collisions = contig_variants_collisions[stop_idx:]
                # from right:
                contig_counts.reverse()
                stop_idx = 0
                for (i, contig_count) in enumerate(contig_counts):
                    logger.debug("(i:%s, count:%s)" % (i, contig_count))
                    if (contig_count > count_bound):
                        stop_idx = i
                        break
                kmer_contig.cut_edge(stop_idx)
                contig_replacements = contig_replacements[:-stop_idx] if stop_idx != 0 else contig_replacements
                contig_variants_collisions = contig_variants_collisions[
                                             :-stop_idx] if stop_idx != 0 else contig_variants_collisions
            contigs_replacements.append(contig_replacements)
            contigs_variants_collisions.append(contig_variants_collisions)
            # contigs_modifications.append(contig_modifications)
        if (output_file != None):
            output_file_handle.close()
        if (dry_mode == False):
            logger.info('finished fixing contigs')
        if (output_file != None):
            logger.info('finished writing contigs report')
        return contigs_replacements, contigs_variants_collisions, peptides_alignments_list

    def remove_redundant_contigs(self, final_contigs, contigs_replacements, contigs_variants_collisions, peptides_alignments_list, min_quality=0.9):
        logger.info('start to remove redundant contigs')
        new_contigs = final_contigs[:]
        for i in reversed(range(len(final_contigs))):
            candidate_contig = final_contigs[i]
            for contig in new_contigs:
                if candidate_contig == contig:
                    continue
                alignment = self.leven_aligner.align_to_protein(contig, candidate_contig)
                if alignment:
                    logger.debug(candidate_contig.single_identifier+"is similar to "+contig.single_identifier+" with quality: "+str(alignment.quality))
                    if alignment.quality > min_quality:
                        logger.debug("dropping "+candidate_contig.single_identifier+"because he's too similar("+str(alignment.quality)+") to "+contig.single_identifier)
                        new_contigs.remove(candidate_contig)
                        del contigs_replacements[i]
                        del contigs_variants_collisions[i]
                        del peptides_alignments_list[i]
                        break
        logger.info('finished removing redundant contigs')
        return new_contigs

    # currently not in use(for testing)!
    def extract_final_contigs(self, kmer_contigs):
        logger.info("starting to extract final contigs")
        sorted_kmer_contigs = sorted(kmer_contigs, key=lambda x: (len(x.peptides)), reverse=True)
        final_contigs = []
        for kmer_contig in sorted_kmer_contigs:
            alignment = kmer_contig.align_to_protein()
            alignment_score = alignment.quality if alignment else None
            if (alignment_score != None and alignment_score > 0.9):
                final_contigs.append(kmer_contig)
        logger.info("finished extracting final contigs")
        return final_contigs


PROTEINS_DICT = {'MYOGLOBIN': MYOGLOBIN, 'FETUIN': FETUIN, 'BSA': BSA,
                 'SIGMA': [SIGMA_HEAVY, SIGMA_LIGHT], 'AR': [AR_HEAVY, AR_LIGHT], 'AVASTIN': [AVASTIN_HEAVY, AVASTIN_LIGHT],
                 'IFX': [IFX_HEAVY, IFX_LIGHT], 'SA': [SA_HEAVY, SA_LIGHT]}
PROTEINS_OFFSETS = {'MYOGLOBIN': 1, 'FETUIN': 18, 'BSA': 24,
                    'SIGMA': 0}
STEP_ALL = 'all'
STEP_ASSEMBLE_CONTIGS = 'assemble_contigs'
STEP_MERGE_CONTIGS = 'merge_contigs'
STEP_UNITE_CONTIGS = 'unite_contigs'
STEP_FILTERED_CONTIGS = 'unite_filtered_contigs'
ASSEMBLE_STEPS = [STEP_ALL, STEP_ASSEMBLE_CONTIGS, STEP_MERGE_CONTIGS, STEP_UNITE_CONTIGS, STEP_FILTERED_CONTIGS]


class KmerAssembler(Assembler):
    def __init__(self, starting_step, acid_input_files_paths, trypsin_input_files_paths, output_dir, kmer_size,
                 kmer_min_overlap, unite_min_overlap,
                 unite_min_extension, merge_min_quality, report_title='Final Contigs Report', protein_name=None,
                 contigs_ids_file=None, min_peptides_distribution=9):
        self.starting_step = starting_step
        self.acid_input_files_paths = acid_input_files_paths if (acid_input_files_paths != None) else []
        self.trypsin_input_files_paths = trypsin_input_files_paths if (trypsin_input_files_paths != None) else []
        self.output_dir = output_dir
        self.kmer_size = kmer_size
        self.kmer_min_overlap = kmer_min_overlap
        self.unite_min_overlap = unite_min_overlap
        self.unite_min_extension = unite_min_extension
        self.protein_name = protein_name
        self.contigs_ids_file = contigs_ids_file
        self.merge_min_quality = merge_min_quality
        self.report_title = report_title
        self.min_peptides_distribution = min_peptides_distribution
        # get protein:
        self.protein = PROTEINS_DICT[protein_name] if protein_name in PROTEINS_DICT else None
        self.cleaned_protein = None
        if (self.protein != None):
            self.cleaned_protein = self.protein.clean() if (type(self.protein) != list) else [protein.clean() for
                                                                                              protein in self.protein]
        # create kmer handler:
        self.kmers_handler = KmersHandler(self.kmer_size, self.cleaned_protein)
        # paths declarations:
        self._create_paths()
        # prepare output dir and logger:
        self._prepare_output_folder()
        # write script parameters:
        self._write_scripts_params()

    def _prepare_output_folder(self):
        # create output dir:
        try:
            os.makedirs(self.output_dir)
        except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(self.output_dir):
                pass
            else:
                raise
        # add log file:
        log_file_path = os.path.join(self.output_dir, 'stdout.log')
        # try:
        # os.remove(log_file_path)
        # except OSError:
        # pass
        hdlr = logging.FileHandler(os.path.join(self.output_dir, 'stdout.log'))
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        hdlr.setLevel(logging.DEBUG)
        logger.addHandler(hdlr)
        logger.setLevel(logging.DEBUG)

    def _create_paths(self):
        # paths declarations:
        self.cleaned_peptides_path = os.path.join(self.output_dir, 'cleaned_peptides.csv')
        self.cleaned_peptides_json_path = os.path.join(self.output_dir, 'cleaned_peptides.json')
        self.kmers_path = os.path.join(self.output_dir, 'kmers.csv')
        self.kmers_json_path = os.path.join(self.output_dir, 'kmers.json')
        self.contigs_path = os.path.join(self.output_dir, 'contigs.csv')
        self.contigs_json_path = os.path.join(self.output_dir, 'contigs.json')
        self.united_contigs_path = os.path.join(self.output_dir, 'united_contigs.csv')
        self.merged_contigs_path = os.path.join(self.output_dir, 'merged_contigs.csv')
        self.merged_contigs_json_path = os.path.join(self.output_dir, 'merged_contigs.json')
        self.families_path = os.path.join(self.output_dir, 'families.txt')
        self.filtered_united_contigs_path = os.path.join(self.output_dir, 'filtered_united_contigs.csv')
        self.filtered_united_contigs_json_path = os.path.join(self.output_dir, 'filtered_united_contigs.json')
        self.final_contigs_path = os.path.join(self.output_dir, 'final_contigs.csv')
        self.filtered_united_contigs_report_path = os.path.join(self.output_dir, 'filtered_united_contigs_report.txt')
        self.final_contigs_report_path = os.path.join(self.output_dir, 'final_contigs_report.txt')
        self.final_reps_path = os.path.join(self.output_dir, 'final_reps.csv')
        self.final_coverage_path = os.path.join(self.output_dir, 'final_coverages.csv')
        self.final_alignments_path = os.path.join(self.output_dir, 'final_alignments.txt')

    def _write_scripts_params(self):
        logger.info("script parameters:")
        logger.info("==================")
        logger.info("starting_step: " + str(self.starting_step))
        logger.info("acid_input_file_path: " + str(self.acid_input_files_paths))
        logger.info("trypsin_input_file_path: " + str(self.trypsin_input_files_paths))
        logger.info("output_dir: " + str(self.output_dir))
        logger.info("kmer_size: " + str(self.kmer_size))
        logger.info("kmer_min_overlap: " + str(self.kmer_min_overlap))
        logger.info("unite_min_overlap: " + str(self.unite_min_overlap))
        logger.info("unite_min_extension: " + str(self.unite_min_extension))
        logger.info("protein_name: " + str(self.protein_name))
        logger.info("contigs_ids_file: " + str(self.contigs_ids_file))
        logger.info("merge_min_quality: " + str(self.merge_min_quality))
        logger.info("==================")

    def assemble(self):
        # start working:
        if self.starting_step == STEP_ALL:
            acid_peptide_reader = PeptideReader(
                [open(csv_filename, 'r') for csv_filename in self.acid_input_files_paths], peptide_type=ACID_TYPE)
            acid_peptides = acid_peptide_reader.read()
            trypsin_peptide_reader = PeptideReader(
                [open(csv_filename, 'r') for csv_filename in self.trypsin_input_files_paths], peptide_type=TRYPSIN_TYPE)
            trypsin_peptides = trypsin_peptide_reader.read()
            peptides = acid_peptides + trypsin_peptides
            cleaner = PeptideCleaner(min_length=2, min_good_confidence=10)
            cleaned_peptides = cleaner.clean(peptides)
            ProteinSequenceCsvWriter(cleaned_peptides).write(self.cleaned_peptides_path)
            json.dump(obj=cleaned_peptides, fp=open(self.cleaned_peptides_json_path, 'w'), default=json_dump_default,
                      indent=2)
            kmers = self.kmers_handler.get_kmers(cleaned_peptides)
            kmers = [kmer for kmer in kmers if (kmer.num_sequences >= 2 or (
                kmer.num_sequences == 1 and kmer.average_conf >= 90))]
            KmerWriter.write_kmers(kmers, self.kmers_path)
            json.dump(obj=kmers, fp=open(self.kmers_json_path, 'w'), default=json_dump_default, indent=2)
        else:
            # load cleaned peptides and then kmers:
            cleaned_peptides = json.load(fp=open(self.cleaned_peptides_json_path, 'r'),
                                         object_hook=json_load_object_hook)
            logger.info("finished loading peptides from json")
            peptides_cache.update(dict([(peptide.single_identifier, peptide) for peptide in cleaned_peptides]))
            kmers = json.load(fp=open(self.kmers_json_path, 'r'), object_hook=json_load_object_hook)
            logger.info("finished loading kmers from json")
            kmers_cache.update(dict([(kmer.identifier, kmer) for kmer in kmers]))

        if self.starting_step in [STEP_ALL, STEP_ASSEMBLE_CONTIGS]:
            contigs = self.kmers_handler.assemble_kmers(kmers, min_overlap=self.kmer_min_overlap)
            KmerWriter.write_contigs_to_csv(contigs, self.contigs_path)
            json.dump(obj=contigs, fp=open(self.contigs_json_path, 'w'), default=json_dump_default, indent=2)
        else:
            # load contigs:
            contigs = json.load(fp=open(self.contigs_json_path, 'r'), object_hook=json_load_object_hook)
            logger.info("finished loading kmer contigs from json")

        # filter contigs if needed:
        if self.contigs_ids_file != None:
            contigs_ids = open(self.contigs_ids_file, 'r').read().splitlines()
            logger.info("selected contigs: " + str(contigs_ids))
            contigs = [contig for contig in contigs if contig.single_identifier in contigs_ids]

        if self.starting_step in [STEP_ALL, STEP_ASSEMBLE_CONTIGS, STEP_MERGE_CONTIGS]:
            # create families(merge contigs)
            merged_contigs = self.kmers_handler.merge_contigs(contigs, self.families_path, self.merge_min_quality)
            KmerWriter.write_contigs_to_csv(merged_contigs, self.merged_contigs_path)
            json.dump(obj=merged_contigs, fp=open(self.merged_contigs_json_path, 'w'), default=json_dump_default,
                      indent=2)
        else:
            # load merged_contigs:
            merged_contigs = json.load(fp=open(self.merged_contigs_json_path, 'r'), object_hook=json_load_object_hook)
            logger.info("finished loading merged kmer contigs from json")

        if self.starting_step in [STEP_ALL, STEP_ASSEMBLE_CONTIGS, STEP_MERGE_CONTIGS, STEP_UNITE_CONTIGS]:
            # unite contigs:
            united_contigs = self.kmers_handler.unite_contigs(merged_contigs, overlap_size=self.unite_min_overlap,
                                                              min_len_gap=self.unite_min_extension)
            KmerWriter.write_contigs_to_csv(united_contigs, self.united_contigs_path)
            # extract final contigs and fix them (dry):
            filtered_united_contigs = self.kmers_handler.extract_unique_contigs(united_contigs, min_peptides_distribution=self.min_peptides_distribution)
            KmerWriter.write_contigs_to_csv(filtered_united_contigs, self.filtered_united_contigs_path)
            json.dump(obj=filtered_united_contigs, fp=open(self.filtered_united_contigs_json_path, 'w'), default=json_dump_default,
                      indent=2)
            # self.kmers_handler.fix_contigs(filtered_united_contigs, cleaned_peptides=cleaned_peptides,
            #                                output_file=self.filtered_united_contigs_report_path, dry_mode=True)
        else:
            # load filtered_united_contigs:
            filtered_united_contigs = json.load(fp=open(self.filtered_united_contigs_json_path, 'r'), object_hook=json_load_object_hook)
            logger.info("finished loading filtered united contigs from json")
        # second unite + extract and fix:
        final_contigs = self.kmers_handler.unite_contigs(filtered_united_contigs, overlap_size=3,
                                                         min_len_gap=self.unite_min_extension, only_edges=True)
        final_contigs = self.kmers_handler.extract_unique_contigs(final_contigs, min_peptides_distribution=self.min_peptides_distribution)

        self.kmers_handler.fix_contigs(final_contigs, cleaned_peptides=cleaned_peptides,
                                       output_file=self.filtered_united_contigs_report_path, count_bound=None)
        contigs_replacements, contigs_variants_collisions, peptides_alignments_list = self.kmers_handler.fix_contigs(final_contigs,
                                                                                           cleaned_peptides=cleaned_peptides,
                                                                                           output_file=self.final_contigs_report_path)
        # remove redundant final contigs
        final_contigs = self.kmers_handler.remove_redundant_contigs(final_contigs, contigs_replacements, contigs_variants_collisions, peptides_alignments_list)

        # write peptides alignments:
        KmerWriter.write_peptides_alignments(final_contigs, peptides_alignments_list, self.final_alignments_path)
        # write final reps and coverages info to files - for debugging info:
        # KmerWriter.write_reps_to_csv(contigs_replacements, self.acid_input_files_paths, self.protein,
        #                              self.final_reps_path, offset=PROTEINS_OFFSETS[self.protein_name])
        KmerWriter.write_coverage_to_csv(contigs_variants_collisions, self.acid_input_files_paths,
                                         self.final_coverage_path)
        # write final contigs and their report:
        KmerWriter.write_contigs_to_csv(final_contigs, self.final_contigs_path)
        ContigsReporter(final_contigs, contigs_replacements, contigs_variants_collisions,
                        self.output_dir, self.acid_input_files_paths, self.trypsin_input_files_paths,
                        self.kmer_size, self.kmer_min_overlap, self.unite_min_overlap, self.unite_min_extension,
                        self.merge_min_quality, self.report_title).create_html_report("contigs_report")
        logger.info('-=FIN=-')


class KmerReporter(object):
    def __init__(self, report_title, run_folder, input_sequences):
        self.report_title = report_title
        self.run_folder = run_folder
        self.input_sequences = input_sequences

        # check if run folder exist:
        if not os.path.isdir(self.run_folder):
            raise Exception('run folder does not exist!\n%s' % self.run_folder)

        # check that there is no report with that name already:
        self.report_path = os.path.join(self.run_folder, self.report_title + ".html")
        self.alignments_path = os.path.join(self.run_folder, '%s_alignments.txt' % self.report_title)
        if os.path.isfile(self.report_path):
            raise Exception('a report with that name already exist in the run folder!\n%s' % self.report_path)
        if os.path.isfile(self.alignments_path):
            raise Exception('a alignment file with that name already exist in the run folder!\n%s' % self.alignments_path)

        self.cleaned_peptides_json_path = os.path.join(self.run_folder, 'cleaned_peptides.json')

        # create kmer handler:
        self.kmers_handler = KmersHandler()
        # write script parameters:
        self._write_scripts_params()

    def _write_scripts_params(self):
        logger.info("script parameters:")
        logger.info("==================")
        logger.info("report title: " + self.report_title)
        logger.info("run folder: " + self.run_folder)
        logger.info("input sequences: " + str(self.input_sequences))
        logger.info("==================")

    def create_report(self):
        logger.info('loading cleaned peptides...')
        # load cleaned peptides and then kmers:
        cleaned_peptides = json.load(fp=open(self.cleaned_peptides_json_path, 'r'),
                                     object_hook=json_load_object_hook)
        show_trypsin = any([peptide.peptide_type == TRYPSIN_TYPE for peptide in cleaned_peptides])
        # creating final contigs:
        final_contigs = [KmerContig(identifier='contig_%s' % (i+1,), sequence=seq, confidence=[100]*len(seq), overlapping_kmers=[]) for (i, seq)
                         in enumerate(self.input_sequences)]
        # fixing contigs:
        contigs_replacements, contigs_variants_collisions, peptides_alignments_list = self.kmers_handler.fix_contigs(final_contigs,
                                                                                           cleaned_peptides=cleaned_peptides, dry_mode=True)
        # write peptide alignments:
        KmerWriter.write_peptides_alignments(final_contigs, peptides_alignments_list, self.alignments_path)
        # write report:
        ContigsReporter(final_contigs, contigs_replacements, contigs_variants_collisions, self.run_folder, None, None,
                        None, None, None, None, None, self.report_title, show_trypsin=show_trypsin).create_html_report(self.report_title)
        logger.info('-=FIN=-')