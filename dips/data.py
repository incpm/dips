import logging
import re

from dips.settings import MIN_CONFIDENCE

logger = logging.getLogger(__name__)

AMINO_ACID_REPLACEMENTS = {  # TODO: think of a better solution for this. hierarchy should be as follows:
                             # Q/E: Q is Q, E is E or Q
                             # S/A: S is S, A is A or S (in terms of mass: 'A' + oxidation = 'S')
                             # N/D: N is N, D is D or N
                             # I/L: can't know (same molecular weight)
    'N': 'D',
    'Q': 'E',
    'I': 'L'
    # 'S': 'A'
}

PROTEIN_AMINO_ACID_REPLACEMENTS = AMINO_ACID_REPLACEMENTS.copy()
PROTEIN_AMINO_ACID_REPLACEMENTS.update({
    'I': 'L',
})

ACID_TYPE = "ACID"
TRYPSIN_TYPE = "TRYPSIN"
MODIFICATION = "*"

peptides_cache = {}
kmers_cache = {}


class ProteinSequence(object):
    def __init__(self, identifier, sequence, confidence, sequence_options=None, original_sequence=None):
        self.identifier = identifier if type(identifier) is set \
            else (set(identifier) if type(identifier) is list else set([identifier]))
        assert len(sequence) == len(confidence),\
            'Sequence %s [%s] - sequence length [%d] does not match confidence array length [%d]' %\
            (sequence, self.identifier, len(sequence), len(confidence))
        self.sequence = str(sequence)
        self.original_sequence = original_sequence if original_sequence else str(sequence)
        # for k,v in AMINO_ACID_REPLACEMENTS.items():
        #     self.sequence = self.sequence.replace(k, v)
        self.confidence = confidence
        self._length = len(self.sequence)
        self.sequence_options = sequence_options if sequence_options else self._init_sequence_options()

    def clean_length(self, min_confidence):
        return sum([1 for c in self.confidence if c >= min_confidence])

    @property
    def clean_sequence(self):
        clean_sequence = bytearray(self.sequence)
        for i in range(len(self.sequence)):
            if self.confidence[i] < MIN_CONFIDENCE:
                clean_sequence[i] = '.'
        return clean_sequence

    def _init_sequence_options(self):
        sequence_options = []
        for i, acid in enumerate(self.sequence):
            sequence_options.append({acid: self.confidence[i]})
        return sequence_options

    def __len__(self):
        return self._length

    def __str__(self):
        return 'sequence=%s, identifier=%s, confidence=%s' %\
               (self.clean_sequence, list(self.identifier), self.confidence)

    def __repr__(self):
        return '%s<%s,%s>' % (self.__class__.__name__, self.sequence, self.confidence)


    @property
    def single_identifier(self):
        identifiers = list(self.identifier)
        return '_'.join(identifiers)

    def to_json(self):
        json_repr = dict(cls=self.__class__.__name__)
        json_repr.update(dict([(k, v) for (k, v) in self.__dict__.items()
                               if k in ['identifier', 'sequence', 'confidence', 'sequence_options', 'original_sequence']]))
        return json_repr

    @classmethod
    def from_json(cls, dct):
        instance_params = dict([(k, v) for (k, v) in dct.items()
                                if k in ['identifier', 'sequence', 'confidence', 'sequence_options', 'original_sequence']])
        try:
            return eval(dct['cls'])(**instance_params)
        except Exception:
            logger.error('failed during from_json() with class %s' % (dct['cls'],))
            raise


class Alignment(object):
    def __init__(self, sequence, protein, position, quality):
        self.sequence = sequence
        self.protein = protein
        self.position = position
        self.quality = quality

    def __repr__(self):
        return 'Alignment<position=%3d,%s,protein=%s,quality=%f>' \
            % (self.position, self.sequence, self.protein.identifier, self.quality)

    def __len__(self):
        return len(self.sequence)

    def to_json(self):
        json_repr = dict(cls=self.__class__.__name__)
        json_repr.update(dict([(k, v) for (k, v) in self.__dict__.items()
                               if k in ['sequence', 'protein', 'position', 'quality']]))
        return json_repr

    @classmethod
    def from_json(cls, dct):
        instance_params = dict([(k, v) for (k, v) in dct.items()
                                if k in ['sequence', 'protein', 'position', 'quality']])
        return eval(dct['cls'])(**instance_params)


class Protein(ProteinSequence):
    def __init__(self, identifier, sequence):
        super(Protein, self).__init__(identifier=identifier, sequence=sequence, confidence=[100] * len(sequence))

    def __repr__(self):
        return str(self.single_identifier)

    def to_json(self):
        # special case - save only the protein name
        json_repr = dict(cls=self.__class__.__name__, identifier=self.single_identifier)
        return json_repr

    def clean(self):
        return self.clean_raw_sequence(self.single_identifier, self.sequence[:])

    @staticmethod
    def clean_raw_sequence(id, raw_sequence):
        clean_sequence = raw_sequence
        for k, v in PROTEIN_AMINO_ACID_REPLACEMENTS.items():
            clean_sequence = clean_sequence.replace(k, v)
        return Protein(identifier='clean ' + id, sequence=clean_sequence)

    @classmethod
    def from_json(cls, dct):
        from dips.proteins import KNOWN_PROTEINS
        protein_identifier = dct['identifier']
        assert protein_identifier in KNOWN_PROTEINS
        return KNOWN_PROTEINS.get(protein_identifier)


class Peptide(ProteinSequence):
    @classmethod
    def from_csv(cls, peptide, peptide_type=None):
        identifier = peptide['Scan'] if peptide_type is None else peptide_type+'_'+peptide['Scan']
        sequence = peptide['Peptide']
        # remove brackets from sequence, for example:
        # 'ADAEGAMTKALELM(+15.99)LR(+.98)DLAAKYKEL' should be 'ADAEGAMTKALELMLRDLAAKYKEL'
        modifications = cls.find_modifications(sequence)
        sequence = re.sub('\(.*?\)', '', sequence)
        confidence = [int(s.strip()) for s in peptide['local confidence (%)'].split(' ')]
        return cls(identifier=identifier, sequence=sequence, confidence=confidence, peptide_type=peptide_type,
                   modifications=modifications)

    @staticmethod
    def find_modifications(sequence):
        matches = re.finditer('\(.*?\)', sequence)
        modifications_length = 0
        modifications = []
        for m in matches:
            modifications.append(m.start() - modifications_length - 1)
            modifications_length += len(m.group())
        return modifications

    def __init__(self, identifier, sequence, confidence, sequence_options=None, original_sequence=None,
                 peptide_type=None, modifications=[]):
        super(Peptide, self).__init__(identifier=identifier, sequence=sequence, confidence=confidence,
                                      sequence_options=sequence_options, original_sequence=original_sequence)
        self.peptide_type = peptide_type
        self.modifications = modifications

    def to_json(self):
        json_repr = super(Peptide, self).to_json()
        json_repr.update(dict([(k, v) for (k, v) in self.__dict__.items()
                               if k in ['peptide_type', 'modifications']]))
        return json_repr

    @classmethod
    def from_json(cls, dct):
        obj = super(Peptide, Peptide).from_json(dct)
        obj.peptide_type = dct['peptide_type']
        obj.modifications = dct['modifications']
        return obj


class Contig(ProteinSequence):
    def __init__(self, identifier, sequence, confidence, sequence_options, sequences=list()):
        super(Contig, self).__init__(identifier=identifier, sequence=sequence,
                                     confidence=confidence, sequence_options=sequence_options)
        self.sequences = sequences

    @property
    def num_peptides(self):
        return len(self.identifier)

    @property
    def peptides(self):
        peptides = []
        for sequence in self.sequences:
            if isinstance(sequence, Contig):
                return peptides + sequence.peptides
            elif isinstance(sequence, Peptide):
                peptides.append(sequence)
            else:
                raise IOError('Unexpected sequence type: %s' % type(sequence))
        return peptides


class OverlappingSequence(object):
    def __init__(self, sequence, before, after=None):
        self.sequence = sequence
        self.before = before
        self.after = after

    def to_json(self):
        json_repr = dict(cls=self.__class__.__name__)
        json_repr.update(dict([(k, v) for (k, v) in self.__dict__.items()
                               if k in ['before', 'after']]))
        json_repr['sequence_identifier'] = self.sequence.single_identifier
        return json_repr

    @classmethod
    def from_json(cls, dct):
        instance_params = dict([(k, v) for (k, v) in dct.items()
                                if k in ['before', 'after']])
        sequence_identifier = dct['sequence_identifier']
        global peptides_cache
        sequence = peptides_cache[sequence_identifier]
        try:
            return eval(dct['cls'])(sequence=sequence, **instance_params)
        except Exception:
            logger.error('failed during from_json() with class %s' % (dct['cls'],))
            raise


class OverlappingKmer(OverlappingSequence):
    def __init__(self, sequence, before, after=None, kmer_offset = 0):
        super(OverlappingKmer, self).__init__(sequence, before, after=after)
        self.kmer_offset = kmer_offset

    def to_json(self):
        json_repr = dict(cls=self.__class__.__name__)
        json_repr.update(dict([(k, v) for (k, v) in self.__dict__.items()
                               if k in ['before', 'after', 'kmer_offset']]))
        json_repr['sequence_identifier'] = self.sequence.identifier
        return json_repr

    @classmethod
    def from_json(cls, dct):
        instance_params = dict([(k, v) for (k, v) in dct.items()
                                if k in ['before', 'after', 'kmer_offset']])
        sequence_identifier = dct['sequence_identifier']
        global kmers_cache
        sequence = kmers_cache[sequence_identifier]
        try:
            return eval(dct['cls'])(sequence=sequence, **instance_params)
        except Exception:
            logger.error('failed during from_json() with class %s' % (dct['cls'],))
            raise


class Kmer(object):
    def __init__(self, identifier, kmer, overlapping_sequences=None, confidence_score=None):
        self.identifier = identifier
        self.kmer = kmer
        self.overlapping_sequences = overlapping_sequences if overlapping_sequences else set()
        self.confidence_score = [0] * len(self.kmer) if confidence_score is None else confidence_score

    def to_json(self):
        json_repr = dict(cls=self.__class__.__name__)
        json_repr.update(dict([(k, v) for (k, v) in self.__dict__.items()
                               if k in ['identifier', 'kmer', 'overlapping_sequences', 'confidence_score']]))
        return json_repr

    @classmethod
    def from_json(cls, dct):
        instance_params = dict([(k, v) for (k, v) in dct.items()
                                if k in ['identifier', 'kmer', 'confidence_score']])
        try:
            return eval(dct['cls'])(overlapping_sequences=set(dct['overlapping_sequences']), **instance_params)
        except Exception:
            logger.error('failed during from_json() with class %s' % (dct['cls'],))
            raise

    @classmethod
    def from_csv(cls, kmer_dict):
        kmer_sequence = kmer_dict['kmer']
        occurrences = int(kmer_dict['occurrences'].strip())
        confidence_score = [float(confidence.strip()) for confidence in kmer_dict['confidence'].split("|")]
        kmer = cls(kmer_sequence)
        kmer.overlapping_sequences = [0]*occurrences
        kmer.confidence_score = confidence_score
        return kmer

    def add(self, overlapping_sequence):
        self.overlapping_sequences.add(overlapping_sequence)
        for i in range(len(self.kmer)):
            new_base_confidence = overlapping_sequence.sequence.confidence[overlapping_sequence.before+i]
            self.confidence_score[i] = (self.confidence_score[i]*(len(self.overlapping_sequences)-1) +
                                        new_base_confidence)/float(len(self.overlapping_sequences))

    def swap_residues(self, i, j):
        pass

    @property
    def num_sequences(self):
        return len(self.overlapping_sequences)

    @property
    def average_conf(self):
        return sum(self.confidence_score) / float(len(self.confidence_score))

    def __str__(self):
        return self.kmer


class KmerContig(ProteinSequence):
    protein = None
    leven_aligner = None
    naive_aligner = None
    swap_aligner = None
    kmer_size = None

    def __init__(self, identifier, sequence=None, confidence=None, overlapping_kmers=None, kmer=None):
        if kmer is None:
            super(KmerContig, self).__init__(identifier=identifier, sequence=sequence, confidence=confidence)
            self.overlapping_kmers = overlapping_kmers
        else:
            super(KmerContig, self).__init__(identifier=identifier, sequence=kmer.kmer, confidence=list(kmer.confidence_score))
            self.overlapping_kmers = set([OverlappingKmer(kmer, 0, len(kmer.kmer))])
        self.sequence = bytearray(self.sequence)

    def to_json(self):
        json_repr = dict(cls=self.__class__.__name__)
        json_repr.update(dict([(k, v) for (k, v) in self.__dict__.items()
                               if k in ['identifier', 'confidence', 'overlapping_kmers']]))
        json_repr.update({'sequence': str(self.sequence)})
        return json_repr

    @classmethod
    def from_json(cls, dct):
        instance_params = dict([(k, v) for (k, v) in dct.items()
                                if k in ['identifier', 'sequence', 'confidence']])
        try:
            return eval(dct['cls'])(overlapping_kmers=set(dct['overlapping_kmers']), **instance_params)
        except Exception:
            logger.error('failed during from_json() with class %s' % (dct['cls'],))
            raise

    def update_confidence(self):
        # calculate new confidence:
        self.confidence = []
        for i in range(len(self.sequence)):
            relevant_overlapping_kmers = [overlapping_kmer for overlapping_kmer in self.overlapping_kmers
                                          if(overlapping_kmer.before<=i and overlapping_kmer.after>i)]
            count = 0
            sum = 0
            used_peptides = set()
            for relevant_overlapping_kmer in relevant_overlapping_kmers:
                peptides_dict = {overlapping_peptide.sequence:overlapping_peptide for overlapping_peptide in relevant_overlapping_kmer.sequence.overlapping_sequences}
                relevant_peptides = set(peptides_dict.keys())
                relevant_peptides_minus_used = relevant_peptides.difference(used_peptides)
                used_peptides.update(relevant_peptides_minus_used)
                for relevant_peptide in relevant_peptides_minus_used:
                    overlapping_peptide = peptides_dict[relevant_peptide]
                    sum += relevant_peptide.confidence[overlapping_peptide.before+relevant_overlapping_kmer.kmer_offset+(i-relevant_overlapping_kmer.before)]
                    count += 1
            self.confidence.append(sum/float(count) if(count!=0) else 0)


    """
    adding a kmer to the contig with offset(how much it will add to the right or left).
    if offset is positive will add to right, if negative will add left.
    """
    def add_kmer(self, kmer, offset):
        new_kmer_sequence = bytearray(kmer.kmer)
        #print self.sequence+" -> "+new_kmer_sequence+", offset: "+str(offset)
        # extend sequence and fix kmers if needed:
        if offset >= 0:
            before = len(self.sequence)+offset-len(new_kmer_sequence)
            self.sequence.extend(new_kmer_sequence[-offset:]) #[-(len(new_kmer_sequence)-offset):] = new_kmer_sequence
        else:
            self.sequence[0:0] = new_kmer_sequence[:-offset]
            before = 0
            # fix before in all old overlapping kmers:
            for current_overlapping_kmer in self.overlapping_kmers:
                current_overlapping_kmer.before += -offset
                current_overlapping_kmer.after += -offset
        overlapping_kmer_orig = OverlappingKmer(sequence=kmer, before=before, after=before+len(kmer.kmer))
        # add the new overlapping kmer:
        my_kmers = {overlapping_kmer.sequence:overlapping_kmer for overlapping_kmer in self.overlapping_kmers}
        if(overlapping_kmer_orig.sequence in my_kmers):
            relevant_kmer = my_kmers[overlapping_kmer_orig.sequence]
            relevant_kmer.before = overlapping_kmer_orig.before
            relevant_kmer.after = overlapping_kmer_orig.after
            relevant_kmer.kmer_offset = overlapping_kmer_orig.kmer_offset
        else:
            self.overlapping_kmers.add(overlapping_kmer_orig)

    """
    adding a contig to the existing contig with offset(how much it will add to the right or left).
    if offset is positive will add to right, if negative will add left.
    """
    def add_contig(self, contig, offset):
        new_contig_sequence = bytearray(contig.sequence)
        #print self.sequence+" -> "+new_contig_sequence+", offset: "+str(offset)
        # extend sequence and fix kmers:
        if offset >= 0:
            before = len(self.sequence)+offset-len(new_contig_sequence)
            self.sequence.extend(new_contig_sequence[-offset:])
            # self.confidence.extend(contig.confidence[-offset:])
        else:
            self.sequence[0:0] = new_contig_sequence[:-offset]
            # self.confidence[0:0] = contig.confidence[:-offset]
            before = 0
            # fix before in all old overlapping kmers:
            for current_overlapping_kmer in self.overlapping_kmers:
                current_overlapping_kmer.before += -offset
                current_overlapping_kmer.after += -offset
        # add the new overlapping kmers:
        my_kmers = {overlapping_kmer.sequence:overlapping_kmer for overlapping_kmer in self.overlapping_kmers}
        for contig_overlapping_kmer in contig.overlapping_kmers:
            if(offset>=0):
                new_offset = len(self.sequence)-len(contig.sequence) # self.sequence is after the extension
            else:
                new_offset = 0
            if(contig_overlapping_kmer.sequence in my_kmers):
                relevant_overlapping_kmer = my_kmers[contig_overlapping_kmer.sequence]
                if(contig_overlapping_kmer.before+new_offset==relevant_overlapping_kmer.before):
                    relevant_overlapping_kmer.before = min(contig_overlapping_kmer.before+new_offset,relevant_overlapping_kmer.before)
                    relevant_overlapping_kmer.after = max(contig_overlapping_kmer.after+new_offset,relevant_overlapping_kmer.after)
                    relevant_overlapping_kmer.kmer_offset = min(contig_overlapping_kmer.kmer_offset,relevant_overlapping_kmer.kmer_offset)
                    assert(relevant_overlapping_kmer.after-relevant_overlapping_kmer.before<=KmerContig.kmer_size),"same kmer in two different places on contig"
            else:
                new_overlapping_kmer = OverlappingKmer(contig_overlapping_kmer.sequence, contig_overlapping_kmer.before+new_offset,
                                                       contig_overlapping_kmer.after+new_offset, kmer_offset=contig_overlapping_kmer.kmer_offset)
                assert(new_overlapping_kmer.after-new_overlapping_kmer.before<=KmerContig.kmer_size),"kmer after-before is bigger than kmer size"
                self.overlapping_kmers.add(new_overlapping_kmer)

    """
    if edge_size is positive will cut the obsolete from right, if negative will cut from left.
    """
    def cut_edge(self, edge_size):
        assert (abs(edge_size)<len(self.sequence)), "trying to cut "+edge_size+" edge from a kmer with length: "+len(self.sequence)
        if(edge_size==0):
            return
        if(edge_size>=0):
            # remove redundant kmers:
            self.overlapping_kmers = set([overlapping_kmer for overlapping_kmer in self.overlapping_kmers if(not overlapping_kmer.before>=(len(self.sequence)-edge_size))])
            # fix old:
            for current_overlapping_kmer in self.overlapping_kmers:
                if(current_overlapping_kmer.after>(len(self.sequence)-edge_size)):
                    current_overlapping_kmer.after = len(self.sequence)-edge_size
            self.sequence = self.sequence[:-edge_size]
            self.confidence = self.confidence[:-edge_size]
        elif(edge_size<0):
            # remove redundant kmers:
            self.overlapping_kmers = set([overlapping_kmer for overlapping_kmer in self.overlapping_kmers if(not overlapping_kmer.after<=-edge_size)])
            # fix old:
            for current_overlapping_kmer in self.overlapping_kmers:
                current_overlapping_kmer.before += edge_size
                current_overlapping_kmer.after += edge_size
                if(current_overlapping_kmer.before<0):
                    current_overlapping_kmer.kmer_offset += -current_overlapping_kmer.before
                    current_overlapping_kmer.before = 0
            # cut sequence and confidence:
            self.sequence = self.sequence[-edge_size:]
            self.confidence = self.confidence[-edge_size:]

    """
    will swap the positions of the residue i with j.
    """
    def swap_residues(self, i, j):
        temp = self.sequence[i]
        self.sequence[i] = self.sequence[j]
        self.sequence[j] = temp

    def swap_edge_residues(self, right):
        if(right):
            self.swap_residues(len(self)-1, len(self)-2)
        else:
            self.swap_residues(0,1)

    """
    will absorb given kmer contig to current kmer contig with given offset(the index which the given kmer contig starts
    in our kmer contig sequence).
    """
    def absorb_contig(self, child_kmer_contig, offset):
        #print len(self.sequence), len(child_kmer_contig.sequence), offset
        assert len(self.sequence)>=len(child_kmer_contig.sequence)+offset, "child contig + offset is longer then father contig"
        # add new kmers:
        kmers_dict = {overlapping_kmer.sequence:overlapping_kmer for overlapping_kmer in self.overlapping_kmers}
        for contig_overlapping_kmer in child_kmer_contig.overlapping_kmers:
            if(contig_overlapping_kmer.sequence in kmers_dict):
                relevant_overlapping_kmer = kmers_dict[contig_overlapping_kmer.sequence]
                # if this kmer is found in different places on the contig:
                if(relevant_overlapping_kmer.before==contig_overlapping_kmer.before+offset):
                    relevant_overlapping_kmer.before = min(contig_overlapping_kmer.before+offset,relevant_overlapping_kmer.before)
                    relevant_overlapping_kmer.after = max(contig_overlapping_kmer.after+offset,relevant_overlapping_kmer.after)
                    relevant_overlapping_kmer.kmer_offset = min(contig_overlapping_kmer.kmer_offset,relevant_overlapping_kmer.kmer_offset)
                    assert(relevant_overlapping_kmer.after-relevant_overlapping_kmer.before<=KmerContig.kmer_size),"same kmer in two different places on contig"
                else:
                    pass

            else:
                new_overlapping_kmer = OverlappingKmer(contig_overlapping_kmer.sequence, contig_overlapping_kmer.before+offset,
                                                       contig_overlapping_kmer.after+offset, kmer_offset=contig_overlapping_kmer.kmer_offset)
                assert(new_overlapping_kmer.after-new_overlapping_kmer.before<=KmerContig.kmer_size),"kmer after-before is bigger than kmer size"
                self.overlapping_kmers.add(new_overlapping_kmer)

    @property
    def peptides(self):
        peptides = set()
        for overlapping_kmer in self.overlapping_kmers:
            peptides.update([overlapping_sequence.sequence for overlapping_sequence in overlapping_kmer.sequence.overlapping_sequences])
        return peptides

    @property
    def coverage(self):
        coverage = []
        for i in range(len(self)):
            relevant_peptides = set([overlapping_peptide.sequence for overlapping_kmer in self.overlapping_kmers
                                     if(overlapping_kmer.before<=i and overlapping_kmer.after>i) for overlapping_peptide
                                     in overlapping_kmer.sequence.overlapping_sequences])
            coverage.append(len(relevant_peptides))
        return coverage

    @property
    def avg_conf(self):
        avg_conf = sum(self.confidence)/float(len(self.confidence))
        return avg_conf

    def align_to_protein(self):
        if KmerContig.protein==None or KmerContig.leven_aligner==None:
            return None
        self.sequence = str(self.sequence)
        alignments, unaligned_sequences = KmerContig.leven_aligner.align([self])
        self.sequence = bytearray(self.sequence)
        return None if(not alignments) else alignments[0]

    # not really in use!
    def get_variants(self, i):
        relevant_overlapping_kmers = [overlapping_kmer for overlapping_kmer in self.overlapping_kmers
                                          if(overlapping_kmer.before<=i and overlapping_kmer.after>i)]
        options_dict = {}
        options_dict[chr(self.sequence[i])] = 1
        for relevant_overlapping_kmer in relevant_overlapping_kmers:
            base = relevant_overlapping_kmer.sequence.kmer[i-relevant_overlapping_kmer.before+relevant_overlapping_kmer.kmer_offset]
            if base in options_dict:
                options_dict[base] += 1
            else:
                options_dict[base] = 1
        return options_dict

    # not really in use!
    def get_swaps_and_replacements(self, i):
        relevant_overlapping_kmers = [overlapping_kmer for overlapping_kmer in self.overlapping_kmers
                                          if(overlapping_kmer.before<=i and overlapping_kmer.after>i)]
        peptides_bases = set()
        for relevant_overlapping_kmer in relevant_overlapping_kmers:
            kmer_str = relevant_overlapping_kmer.sequence.kmer[relevant_overlapping_kmer.kmer_offset:relevant_overlapping_kmer.kmer_offset+
                                                                                                     (relevant_overlapping_kmer.after-relevant_overlapping_kmer.before)]
            if(self.sequence[relevant_overlapping_kmer.before:relevant_overlapping_kmer.after]==kmer_str):
                for overlapping_peptide in relevant_overlapping_kmer.sequence.overlapping_sequences:
                    peptides_bases.add(overlapping_peptide.sequence.original_sequence[overlapping_peptide.before+
                                                                            (i-relevant_overlapping_kmer.before+
                                                                             relevant_overlapping_kmer.kmer_offset)])
        return peptides_bases

    def get_peptides_alignments(self, my_peptides=None):
        self.sequence = str(self.sequence)
        peptides = self.peptides if my_peptides==None else my_peptides
        alignments = []
        for peptide in peptides:
            alignment = KmerContig.swap_aligner.align_to_protein(self, peptide)
            if(alignment):
                alignments.append(alignment)
        self.sequence = bytearray(self.sequence)
        return alignments

    def __len__(self):
        return len(self.sequence)
