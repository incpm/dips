from dips.data import ProteinSequence, Alignment, Protein, Kmer, OverlappingSequence, OverlappingKmer, KmerContig, \
    Peptide


# JSON save/load helper functions
def json_dump_default(obj):
    if isinstance(obj, set):
        return list(obj)
    elif isinstance(obj, ProteinSequence) or isinstance(obj, Alignment)\
            or isinstance(obj, Kmer) or isinstance(obj, OverlappingSequence):
        json_repr = obj.to_json()
        return json_repr
    else:
        return obj


def json_load_object_hook(dct):
    if 'cls' in dct:
        cls = dct['cls']
        if cls == 'Protein':
            return Protein.from_json(dct)
        elif cls == 'Peptide':
            return Peptide.from_json(dct)
        elif cls in ('Contig'):
            return ProteinSequence.from_json(dct)
        elif cls == 'Alignment':
            return Alignment.from_json(dct)
        elif cls == 'Kmer':
            return Kmer.from_json(dct)
        elif cls == 'OverlappingSequence':
            return OverlappingSequence.from_json(dct)
        elif cls == 'OverlappingKmer':
            return OverlappingKmer.from_json(dct)
        elif cls == 'KmerContig':
            return KmerContig.from_json(dct)
        else:
            raise IOError('Unhandled class [%s]' % cls)
    return dct


def json_dump_kmer_contig(obj):
    if isinstance(obj, set):
        return list(obj)
    elif isinstance(obj, ProteinSequence) or isinstance(obj, Alignment):
        json_repr = obj.to_json()
        return json_repr
    else:
        return obj
