import re
import logging

from .data import Peptide, AMINO_ACID_REPLACEMENTS

CD_REGEX_PATTERN = "[CD]{4,}"
INCORRECT_BASE = '-'
logger = logging.getLogger(__name__)


class PeptideCleaner(object):
    def __init__(self, min_length=5, min_good_confidence=6):
        assert min_length > 0, 'min_length should be positive but is %s' % (min_length,)
        assert min_length > 0, 'min_good_confidence should be positive but is %s' % (min_good_confidence,)
        self.min_length = min_length
        self.min_good_confidence = min_good_confidence

    def clean(self, peptides):
        # convert to list if needed
        if not isinstance(peptides, list):
            peptides = [peptides]
        all_split_peptides = []
        for peptide in peptides:
            # split peptides around around [CD](4,} and around residues with local confidence<10
            # make a mutable copy and replace all problematic residues with '-':
            split_peptides = []
            sequence = bytearray(peptide.sequence)
            # find consecutive C/D acids
            for match in re.finditer(CD_REGEX_PATTERN, sequence):
                for i in range(len(match.group())):
                    sequence[match.start()+i] = INCORRECT_BASE
            # find low confidence
            for i in range(len(peptide.confidence)):
                if peptide.confidence[i] < self.min_good_confidence:
                    sequence[i] = INCORRECT_BASE
            # split peptides
            start = 0
            split_count = 0
            #logger.debug(str(sequence))
            for i in range(len(sequence)):
                if chr(sequence[i]) == INCORRECT_BASE:
                    end = i
                    if start != end and (end-start) >= self.min_length:
                        split_count += 1
                        split_peptides.append(Peptide(next(iter(peptide.identifier)) + "_" + str(split_count),
                                                      sequence[start:end], peptide.confidence[start:end],
                                                      peptide_type=peptide.peptide_type,
                                                      modifications=[modification-start for modification in
                                                      peptide.modifications if start <= modification < end]))
                    start = i + 1
            # final check(cause there isn't always '-' at the end of the sequence)
            if start != len(sequence) and (len(sequence)-start) >= self.min_length:
                split_count += 1
                split_peptides.append(Peptide(next(iter(peptide.identifier)) + "_" + str(split_count),
                                              sequence[start:], peptide.confidence[start:],
                                              peptide_type=peptide.peptide_type,
                                              modifications=[modification-start for modification in
                                              peptide.modifications if start <= modification]))

            # replace N->D, Q->E:
            for split_peptide in split_peptides:
                for k, v in AMINO_ACID_REPLACEMENTS.items():
                    split_peptide.sequence = split_peptide.sequence.replace(k, v)
            #logger.debug(split_peptides)
            all_split_peptides += split_peptides
        return all_split_peptides


def main():
    from dips.io import PeptideReader
    input_filenames = ['test-input/s3.csv']
    peptide_reader = PeptideReader([open(csv_filename, 'r') for csv_filename in input_filenames])
    peptides = peptide_reader.read()[:5]
    cleaner = PeptideCleaner(min_length=5, min_good_confidence=6)
    clean_peptides = cleaner.clean(peptides)
    print('Clean peptides: %s' % (clean_peptides,))

if __name__ == '__main__':
    main()
