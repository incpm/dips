import importlib
import json
import logging
import os

from dips.cleaner import PeptideCleaner
from dips.align import NaiveAligner
from dips.downsample import DownSampler
from dips.io import PeptideReader
from dips.assemble import StubAssembler, OnePassAssembler, MultiPassAssembler, EdgesAssembler, CompositeAssembler, KmerAssembler
from dips.jsonutils import json_load_object_hook, json_dump_default

logger = logging.getLogger(__name__)

settings = None

class BaseAssemblyRunner(object):
    def _get_coverage_stats(self, protein, alignments):
        alignment_start_positions = [0] * len(protein)
        alignment_end_positions = [0] * len(protein)
        coverage = [0] * len(protein)
        for alignment in alignments:
            alignment_start_positions[alignment.position] += 1
            alignment_end_positions[min(alignment.position+len(alignment), len(coverage))-1] += 1
            for i in range(max(0, alignment.position), min(alignment.position+len(alignment), len(coverage))):
                coverage[i] += 1
        num_covered_acids = sum([1 for c in coverage if c > 0])
        return dict(coverage=coverage, num_covered_acids=num_covered_acids,
                    alignment_start_positions=alignment_start_positions, alignment_end_positions=alignment_end_positions)

    def print_summary(self):
        if self.assembler_class == 'StubAssembler':
            summary = self._stub_assembly_summary()
        else:
            summary = self._real_assembly_summary()
        return summary

    def _stub_assembly_summary(self):
        assert len(self.contigs) == 0
        assert len(self.peptides) == len(self.assembled_sequences)
        logger.info('Assembly: #sequences before/after assembly: %d' %(len(self.peptides),)) 
        logger.info('Peptides: #total: %d #aligned: %d #unaligned: %d'
                    %(len(self.peptides), len(self.peptide_alignments), len(self.unaligned_peptides)))
        assert len(self.proteins) == 1 # currently only look at coverage of a single protein
        protein = self.proteins[0]
        coverage_stats_dict = self._get_coverage_stats(protein, self.peptide_alignments)
        logger.info('Coverage (assuming all alignments are correct): %d of %d amino acids (%.2f)' %(coverage_stats_dict['num_covered_acids'], len(protein), float(coverage_stats_dict['num_covered_acids']) / len(protein)))
        logger.info("Detailed coverage: %s" %coverage_stats_dict['coverage'])
        return coverage_stats_dict

    def _real_assembly_summary(self):
        average_num_peptides_per_aligned_contig, average_aligned_contig_length = self._contig_stats(self.contig_alignments)
        min_overlap, max_overlap, avg_overlap = self._get_overlap_stats(self.contig_alignments)
        logger.info('Summary - assembly using %s:' %self.assembler_class)
        logger.info('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
        logger.info('Proteins: %s' %[protein.identifier for protein in self.proteins])
        logger.info('Assembly: #sequences before assembly: %d #sequences after assembly: %d #contigs: %d #peptides: %d'
                    %(len(self.peptides), len(self.assembled_sequences), len(self.contigs), len(self.remaining_peptides))) 
        logger.info('Contigs: #total: %d #aligned: %d #unaligned: %d'
                    %(len(self.contigs), len(self.contig_alignments), len(self.unaligned_contigs)))
        logger.info('Aligned contigs: Average #peptides per contig: %s Average length: %s Min overlap: %s Max overlap: %s Average overlap: %s'
                    %(average_num_peptides_per_aligned_contig, average_aligned_contig_length, min_overlap, max_overlap, avg_overlap))
        logger.info('Peptides: #total: %d #aligned: %d #unaligned: %d'
                    %(len(self.peptides), len(self.peptide_alignments), len(self.unaligned_peptides)))
        assert len(self.proteins) == 1 # currently only look at coverage of a single protein
        protein = self.proteins[0]
        coverage_stats_dict = self._get_coverage_stats(protein, self.contig_alignments)
        logger.info('Coverage (assuming all alignments are correct): %d of %d amino acids (%.2f)' %(coverage_stats_dict['num_covered_acids'], len(protein), float(coverage_stats_dict['num_covered_acids']) / len(protein)))
        logger.info("Detailed coverage: %s" %coverage_stats_dict['coverage'])
#         logger.info('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
#         logger.info('Unaligned contigs:')
#         logger.info('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
#         for unaligned_contig in self.unaligned_contigs:
#             logger.info('%s' %(unaligned_contig,))
        return coverage_stats_dict

    def _contig_stats(self, contig_alignments):
        if not contig_alignments:
            return ('N/A', 'N/A')
        total_num_peptides_in_contigs = sum([len(alignment.sequence.identifier) for alignment in contig_alignments])
        average_num_peptides_per_contig = total_num_peptides_in_contigs / float(len(contig_alignments))
        total_contig_length = sum([len(alignment) for alignment in contig_alignments])
        average_contig_length = total_contig_length / float(len(contig_alignments))
        return (average_num_peptides_per_contig, average_contig_length)

    def _get_overlap_stats(self, contig_alignments):
        if len(contig_alignments) <= 1:
            return ('N/A', 'N/A', 'N/A')
        overlaps = []
        for first_alignment,second_alignment in zip(contig_alignments[:-1], contig_alignments[1:]):
            if first_alignment.position + len(first_alignment) >= second_alignment.position + len(second_alignment):
                pass
            overlap = (first_alignment.position + len(first_alignment)) - second_alignment.position
            overlaps.append(max(0, overlap))
        return (min(overlaps), max(overlaps), sum(overlaps) / float(len(overlaps)))

    def save_sequences_to_file(self, sequences, filename):
        logger.info('Saving %s sequences to file %s' %(len(sequences), filename))
        self._save_sequences(sequences, open(os.path.join(self.output_dir, filename), 'w'))

    def _save_sequences(self, sequences, fp):
        json.dump(obj=sequences, fp=fp, default=json_dump_default)

class AssemblyResultLoader(BaseAssemblyRunner):
    def __init__(self, output_dir):
        self.output_dir = output_dir
        assert os.path.isdir(self.output_dir), 'Output directory %s does not exist' %(self.output_dir,)
        self.config = self._load_configuration_from_file()
        self.assembler_class = self.config['ASSEMBLER_CLASS']
        self.proteins = self.config['proteins']
        self._read_assembly_results()

    def _load_configuration_from_file(self):
        return json.load(fp=open(os.path.join(self.output_dir, 'config.json'), 'r'), object_hook=json_load_object_hook)

    def _read_assembly_results(self):
        self.peptides = self._load_sequences_from_file('peptides.json')
        self.assembled_sequences = self._load_sequences_from_file('assembled_sequences.json')
        self.contigs = self._load_sequences_from_file('contigs.json')
        self.contig_alignments = self._load_sequences_from_file('aligned_contigs.json')
        self.unaligned_contigs = self._load_sequences_from_file('unaligned_contigs.json')
        self.remaining_peptides = self._load_sequences_from_file('remaining_peptides.json')
        self.peptide_alignments = self._load_sequences_from_file('aligned_peptides.json')
        self.unaligned_peptides = self._load_sequences_from_file('unaligned_peptides.json')

    def _load_sequences_from_file(self, filename):
        return self._load_sequences(open(os.path.join(self.output_dir, filename), 'r'))

    def _load_sequences(self, fp):
        return json.load(fp=fp, object_hook=json_load_object_hook)

class AssemblyRunner(BaseAssemblyRunner):
    def __init__(self, proteins, input_filenames, output_dir, settings_module):
        global settings
        settings = importlib.import_module(settings_module)
        self.output_dir = output_dir
        self.input_filenames = input_filenames
        self.proteins = proteins
        self.config = dict(proteins=self.proteins, input_filenames=self.input_filenames, output_dir=self.output_dir)
        self.config.update(self._load_settings())
        self._save_configuration_to_file()
        self.assembler_class = self.config['ASSEMBLER_CLASS']
        self.assembler = self._init_assembler()
        peptide_reader = PeptideReader([open(csv_filename, 'r') for csv_filename in self.input_filenames])
        self.peptides = peptide_reader.read()
        # added cleaning to peptides(pre-processing):
        cleaner = PeptideCleaner(min_length=2,min_good_confidence=10)
        self.peptides = cleaner.clean(self.peptides)
        #self.peptides = DownSampler().downsample(sequences=self.peptides, max_num_sequences=settings.DOWNSAMPLE_MAX_NUM_SEQUENCES)
        self.save_sequences_to_file(self.peptides, 'peptides.json')
        self._log_configuration()

    def _init_assembler(self):
        if self.assembler_class == 'StubAssembler':
            return StubAssembler()
        elif self.assembler_class == 'OnePassAssembler':
            return OnePassAssembler(**settings.ASSEMBLY_PARAMS)
        elif self.assembler_class == 'MultiPassAssembler':
            return MultiPassAssembler(**settings.ASSEMBLY_PARAMS)
        elif self.assembler_class == 'EdgesAssembler':
            return EdgesAssembler(**settings.ASSEMBLY_PARAMS)
        elif self.assembler_class == 'CompositeAssembler':
            return CompositeAssembler(min_evidence=10)
        elif self.assembler_class == 'KmerAssembler':
            return KmerAssembler()
        raise IOError('Unknown assembler class [%s]' %self.assembler_class)

    def _save_summary_to_file(self, summary):
        json.dump(obj=summary, fp=open(os.path.join(self.output_dir, 'summary.json'), 'w'))

    def _save_configuration_to_file(self):
        json.dump(obj=self.config, fp=open(os.path.join(self.output_dir, 'config.json'), 'w'), default=json_dump_default)

    def _log_configuration(self):
        logger.info('Configuration:')
        logger.info('~~~~~~~~~~~~~~')
        for param_name,param_value in self.config.items():
            param_output = '%s=%s' %(param_name, param_value)
            logger.info(param_output)
        logger.info('')

    def _load_settings(self):
        settings_dict = {}
        for param_name in sorted(dir(settings)):
            if param_name.startswith('_'):
                continue
            settings_dict[param_name] = eval('settings.' + param_name)
        logger.debug('loaded settings %s' %(settings_dict,))
        return settings_dict

    def _classify_assembled_sequences(self, assembled_sequences):
        aligner = NaiveAligner(proteins=self.proteins, stop_on_first_alignment=False)
        peptides = [sequence for sequence in assembled_sequences if len(sequence.identifier) == 1]
        contigs = [sequence for sequence in assembled_sequences if len(sequence.identifier) > 1]
        contig_alignments, unaligned_contigs = aligner.align(contigs)
        contig_alignments.sort(key=lambda x: (x.position, -x.quality))
        peptide_alignments, unaligned_peptides = aligner.align(peptides)
        peptide_alignments.sort(key=lambda x: (x.position, -x.quality))
        return (contigs, contig_alignments, unaligned_contigs, peptides, peptide_alignments, unaligned_peptides)

    def run(self):
        self.assembled_sequences = self.assembler.assemble(self.peptides)
        # self.save_sequences_to_file(self.assembled_sequences, 'assembled_sequences.json')
        # (self.contigs, self.contig_alignments, self.unaligned_contigs, self.remaining_peptides, self.peptide_alignments, self.unaligned_peptides) = self._classify_assembled_sequences(self.assembled_sequences)
        # self.save_sequences_to_file(self.contigs, 'contigs.json')
        # self.save_sequences_to_file(self.contig_alignments, 'aligned_contigs.json')
        # self.save_sequences_to_file(self.unaligned_contigs, 'unaligned_contigs.json')
        # self.save_sequences_to_file(self.remaining_peptides, 'remaining_peptides.json')
        # self.save_sequences_to_file(self.peptide_alignments, 'aligned_peptides.json')
        # self.save_sequences_to_file(self.unaligned_peptides, 'unaligned_peptides.json')
        # summary = self.print_summary()
        # self._save_summary_to_file(summary)
