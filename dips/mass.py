OXIDATION_MASS = 15.99491

ACID_MASS = {'A': 71.0779,
             'R': 156.1857,
             'N': 114.1026,
             'D': 115.0874,
             'C': 103.1429,
             'E': 129.114,
             'Q': 128.1292,
             'G': 57.0513,
             'H': 137.1393,
             'I': 113.1576,
             'L': 113.1576,
             'K': 128.1723,
             'M': 131.1961,
             'F': 147.1739,
             'P': 97.1152,
             'S': 87.0773,
             'T': 101.1039,
             'U': 150.0379,
             'W': 186.2099,
             'Y': 163.1733,
             'V': 99.1311}


def calc_mass(amino_acids_sequence):
    return float("{0:.2f}".format(sum([ACID_MASS[acid] for acid in amino_acids_sequence])))


def calc_mass_vector(amino_acids_sequence, k=1):
    return [calc_mass(amino_acids_sequence[i:i+k]) for i in range(0, len(amino_acids_sequence)-k+1)]
