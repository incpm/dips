import logging
import random

logger = logging.getLogger(__name__)


def downsample(sequences, max_num_sequences):
    logger.info('Started from %d sequences, max_num_sequences is %d' % (len(sequences), max_num_sequences))
    downsampled_sequences = sequences[:]
    if 0 < max_num_sequences < len(sequences):
        random.shuffle(downsampled_sequences)
        downsampled_sequences = downsampled_sequences[:max_num_sequences]
    logger.info('Done - returning %d sequences' % (len(downsampled_sequences),))
    return downsampled_sequences
