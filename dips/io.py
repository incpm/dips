import csv
import operator
import logging
import logging.config
from dips.data import Peptide, TRYPSIN_TYPE, ACID_TYPE, AMINO_ACID_REPLACEMENTS

"""logging:"""
from dips.settings.logging_settings import LOGGING, LOGGER_NAME
logging.config.dictConfig(LOGGING)
logger = logging.getLogger(LOGGER_NAME)


class PeptideReader(object):
    def __init__(self, peptides_csv_files, peptide_type=None):
        self._csv_files = peptides_csv_files if type(peptides_csv_files) is list else [peptides_csv_files]
        self.peptide_type = peptide_type
        self.used_ids = {}

    def read(self):
        peptides = []
        for csv_file in self._csv_files:
            peptides += self._read_single_csv_file(csv_file, self.peptide_type)
        logger.info('Done. Read %d peptides' % len(peptides))
        return peptides

    def _read_single_csv_file(self, peptides_csv_file, peptide_type=None):
        peptides = []
        logger.info('Reading peptides from CSV file %s ...' % peptides_csv_file)
        csv_reader = csv.DictReader(peptides_csv_file, delimiter=',')
        while True:
            try:
                row = next(csv_reader)
                # handle duplicate ids:
                if row['Scan'] in self.used_ids:
                    self.used_ids[row['Scan']] += 1
                else:
                    self.used_ids[row['Scan']] = 0
                row['Scan'] += "_"+chr(ord('a')+self.used_ids[row['Scan']])
                peptide = Peptide.from_csv(row, peptide_type)
                peptides.append(peptide)
            except StopIteration:
                break
        logger.debug('Done. Read %d peptides' % len(peptides))
        return peptides


class ProteinSequenceCsvWriter(object):
    def __init__(self, sequences):
        self.sequences = sequences

    def write(self, output_filename):
        logger.info('starting to write peptides...')
        with open(output_filename, 'w') as output_file_handler:
            csv_writer = csv.writer(output_file_handler, delimiter=',', lineterminator='\n')
            csv_writer.writerow(['Scan', 'Peptide', 'local confidence (%)', "peptide type"])
            for sequence in self.sequences:
                csv_writer.writerow([sequence.single_identifier, sequence.sequence,
                                     ' '.join([str(v) for v in sequence.confidence]), sequence.peptide_type])
        logger.info('finished writing peptides')

class KmerWriter(object):
    @staticmethod
    def write_kmers(kmers, kemrs_output_file):
        logger.info('starting to write kmers...')
        header = ["kmer","occurrences","confidence"]
        with open(kemrs_output_file, 'w') as output_csv_handle:
            csv_dict_writer = csv.DictWriter(output_csv_handle, delimiter=',', fieldnames=header, lineterminator='\n')
            csv_dict_writer.writerow(dict((fn,fn) for fn in header))
            for kmer in kmers:
                confidence_str = [str(confidence) for confidence in kmer.confidence_score]
                row = {"kmer":kmer.kmer, "occurrences":kmer.num_sequences, "confidence":"|".join(confidence_str)}
                csv_dict_writer.writerow(row)
        logger.info('finished writing kmers')

    @staticmethod
    def write_contigs_to_csv(contigs, contigs_output_file):
        logger.info('starting to write contigs...')
        header = ["id", "contig", "length", "num_peptides", "num_intersect_peptides", "alignment_position", "alignment_score","avg_conf", "confidence", "coverage"]
        used_peptides = set()
        with open(contigs_output_file, 'w') as output_csv_handle:
            csv_dict_writer = csv.DictWriter(output_csv_handle, delimiter=',', fieldnames=header, lineterminator='\n')
            csv_dict_writer.writerow(dict((fn,fn) for fn in header))
            for contig in contigs:
                current_peptides = contig.peptides
                num_intersect_peptides = len(current_peptides.intersection(used_peptides))
                used_peptides.update(current_peptides)
                confidence = " | ".join([str(conf) for conf in contig.confidence])
                coverage = " | ".join([str(cover) for cover in contig.coverage])
                alignment = contig.align_to_protein()
                alignment_score = alignment.quality if alignment else None
                alignment_position = alignment.position if alignment else None
                row = {"id": contig.single_identifier ,"contig": contig.sequence, "length": len(contig), "num_peptides": len(current_peptides), "num_intersect_peptides": num_intersect_peptides, "alignment_position": alignment_position, "alignment_score": alignment_score, "avg_conf": contig.avg_conf, "confidence": confidence, "coverage": coverage}
                csv_dict_writer.writerow(row)
        logger.info('finished writing contigs')

    # for decisions debugging:
    @staticmethod
    def write_reps_to_csv(contigs_reps, dataset, protein ,output_file, offset=0):
        logger.info('starting to write reps...')
        protein = [protein] if type(protein)!=list else protein
        header = ["dataset", "index", "#E|#D (acid)", "#Q|#N (acid)", "#E|#D (trypsin)", "#Q|#N (trypsin)", "decision","real"]
        with open(output_file, 'w') as output_csv_handle:
            csv_dict_writer = csv.DictWriter(output_csv_handle, delimiter=',', fieldnames=header, lineterminator='\n')
            csv_dict_writer.writerow(dict((fn,fn) for fn in header))
            for (contig_idx, contig_reps) in enumerate(contigs_reps):
                for (i,contig_rep) in enumerate(contig_reps):
                    if len(contig_rep) > 0:
                        index = i
                        e_d = contig_rep[0]
                        q_n = [acid_rep_key for acid_rep_key in AMINO_ACID_REPLACEMENTS if AMINO_ACID_REPLACEMENTS[acid_rep_key]==e_d][0]
                        e_d_acid = contig_rep[1][e_d] if e_d in contig_rep[1] else 0
                        q_n_acid = contig_rep[1][q_n] if q_n in contig_rep[1] else 0
                        e_d_trypsin = contig_rep[2][e_d] if e_d in contig_rep[2] else 0
                        q_n_trypsin = contig_rep[2][q_n] if q_n in contig_rep[2] else 0

                        row = {"dataset": dataset ,"index": index, "#E|#D (acid)": e_d_acid, "#Q|#N (acid)": q_n_acid,
                               "#E|#D (trypsin)": e_d_trypsin,"#Q|#N (trypsin)":q_n_trypsin, "decision":None,"real":protein[contig_idx].sequence[offset+i]}
                        csv_dict_writer.writerow(row)
        logger.info('finished writing reps')

    # for coverage debugging:
    @staticmethod
    def write_coverage_to_csv(contigs_collisions, dataset, output_file):
        logger.info('starting to write final coverages...')
        header = ["dataset", "index", "coverage", "acid"]
        with open(output_file, 'w') as output_csv_handle:
            csv_dict_writer = csv.DictWriter(output_csv_handle, delimiter=',', fieldnames=header, lineterminator='\n')
            csv_dict_writer.writerow(dict((fn,fn) for fn in header))
            for (contig_idx, contig_collisions) in enumerate(contigs_collisions):
                for (i,contig_collision) in enumerate(contig_collisions):
                    row = {"dataset": dataset ,"index": i, "coverage": contig_collision[0][1], "acid": contig_collision[0][0]}
                    csv_dict_writer.writerow(row)
        logger.info('finished writing final coverages')

    CONTIG_WRITE_INTERVAL = 15

    @staticmethod
    def _output_alignments(kmer_contig, alignments, f_out):
        alignments = sorted(alignments, key=lambda x: x.position)
        line_length = len(kmer_contig.sequence)
        f_out.write('%s\n' % kmer_contig.single_identifier)
        f_out.write('number of peptides aligned: %s \n' % len(alignments))
        f_out.write('%s\n' % kmer_contig.sequence)
        for (i, alignment) in enumerate(alignments):
            if i != 0 and i % KmerWriter.CONTIG_WRITE_INTERVAL == 0:
                f_out.write('%s\n' % kmer_contig.sequence)
            current_line = '.' * alignment.position + alignment.sequence.sequence
            f_out.write(current_line + '.' * (line_length - len(current_line)) + alignment.sequence.single_identifier + '\n')
        f_out.write('------------------------------------------\n')

    @staticmethod
    def write_peptides_alignments(kmer_contigs, peptides_alignments_list, output_file):
        logger.info('starting to write peptides alignments to final contigs...')
        with open(output_file, 'w') as f_out:
            for (i, alignments) in enumerate(peptides_alignments_list):
                KmerWriter._output_alignments(kmer_contigs[i], alignments, f_out)
        logger.info('finished writing peptides alignments to final contigs')



