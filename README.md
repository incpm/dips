# pTA - Peptide Tag Assembler

## Installation

This package requires python 2.7. To install, download the source code and run:

    python setup.py install

## Usage

    kmer-assemble.py -h

    usage: kmer-assemble.py [-h] --acid-input-file ACID_INPUT_FILE
                            [ACID_INPUT_FILE ...]
                            [--trypsin-input-file TRYPSIN_INPUT_FILE [TRYPSIN_INPUT_FILE ...]]
                            --output-dir OUTPUT_DIR
                            [--starting-step STARTING_STEP]
                            [--kmer-size KMER_SIZE]
                            [--kmer-min-overlap KMER_MIN_OVERLAP]
                            [--unite-min-overlap UNITE_MIN_OVERLAP]
                            [--unite-min-extension UNITE_MIN_EXTENSION]
                            [--protein-name PROTEIN_NAME]
                            [--contigs-ids-file CONTIGS_IDS_FILE]
                            [--merge-min-quality MERGE_MIN_QUALITY]
                            [--report-title REPORT_TITLE]

    assemble peptides with kmer assembler

    optional arguments:
      -h, --help            show this help message and exit
      --acid-input-file ACID_INPUT_FILE [ACID_INPUT_FILE ...]
                            Peptides input file/s (acid). Required
      --trypsin-input-file TRYPSIN_INPUT_FILE [TRYPSIN_INPUT_FILE ...]
                            Peptides input file/s (Trypsin). Optional
      --output-dir OUTPUT_DIR
                            Output directory. Required
      --starting-step STARTING_STEP
                            Starting step: all, assemble_contigs, merge_contigs,
                            unite_contigs, unite_filtered_contigs, default=all
      --kmer-size KMER_SIZE
                            K-mer size, default=7
      --kmer-min-overlap KMER_MIN_OVERLAP
                            Minimal overlap for assembling k-mers (not uniting),
                            default=5
      --unite-min-overlap UNITE_MIN_OVERLAP
                            Minimal overlap for uniting contigs, default=5
      --unite-min-extension UNITE_MIN_EXTENSION
                            Minimal extension for uniting two contigs, default=7
      --protein-name PROTEIN_NAME
                            DEBUG PARAMETER (optional): The protein you are trying
                            to assemble from this set: [MYOGLOBIN, FETUIN, BSA],
                            default=None
      --contigs-ids-file CONTIGS_IDS_FILE
                            DEBUG PARAMETER (optional): A file containing the
                            contig ID's per line. The filtration occurs before the
                            merge contigs step
      --merge-min-quality MERGE_MIN_QUALITY
                            Minimal quality for similarity between two k-mer
                            contigs, value between 0 to 1, default=0.7
      --report-title REPORT_TITLE
                            Report title name, default='Final Contigs Report'

## Output

*kmer-assemble.py* generates several output files inside the output directory.

A final web report called *contigs_report.html*

Other files:

stdout.log - detailed log file.

cleaned_peptides.csv - CSV file containing the peptides after pre-processing.

cleaned_peptides.json - JSON file containing the peptides after pre-processing (for internal use).

kmers.csv - CSV file containing the kmers after pre-processing.

kmers.json - JSON file containing the kmers after pre-processing (for internal use).

contigs.csv - CSV file containing the contigs that were assembled from the kmers.

contigs.json - JSON file containing the contigs that were assembled from the kmers (for internal use).

merged_contigs.csv - CSV file containing the contigs after the merge step.

merged_contigs.json - JSON file containing the contigs after the merge step (for internal use).

families.txt - TXT file that lists all the contigs families from the merge step (each family is composed from similar contigs).

united_contigs.csv - CSV file containing the contigs after unification of the merged contigs.

filtered_united_contigs.csv - CSV file containing the filtered contigs after filtration of the united contigs.

filtered_united_contigs_report.txt - detailed txt report of the filtered contigs.

final_contigs.csv - CSV file containing the final contigs after another unification of the filtered contigs.

final_contigs_report.txt - detailed txt report of the final contigs.

resources - resources folder containing resources for the final web report

## Tests

To run unit tests, use:

    nosetests

## Datasets

You can find the example datasets in the Datasets directory.
Use one Acid file and one Trypsin file for the Input (except MYOGLOBIN that only one Acid file is needed).

Command line example:

kmer-assemble.py --acid-input-file datasets/FETUIN/Fetuin_Rep1_Acid.csv --trypsin-input-file datasets/FETUIN/Fetuin_Trypsin.csv --output-dir runs/fetuin_example_run/

